#!/usr/bin/env bash

export C_FORCE_ROOT=True

DJANGO_SETTINGS_MODULE='hipcloud_block_storage.settings.prod' /usr/local/bin/celery worker -A hipcloud_block_storage -Q block.sync --concurrency=1 -l info -Ofair