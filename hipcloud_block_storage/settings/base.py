import os
from os.path import abspath, basename, dirname, join, normpath
from kombu import Exchange, Queue
import sys
from datetime import timedelta
reload(sys)
sys.setdefaultencoding('utf8')


ALLOWED_HOSTS = ["*"]
# Application definition

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework_swagger',
    'rest_framework',
    'block'
]

########## PATH CONFIGURATION
# Absolute filesystem path to this Django project directory.
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Site name.
SITE_NAME = basename(DJANGO_ROOT)

# Absolute filesystem path to the top-level project folder.
SITE_ROOT = dirname(DJANGO_ROOT)


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f2+o)+28b8!%a(ak(zw6rjfy%6lww@m$bup7vk4bax!k%o$e0d'

########## DEBUG CONFIGURATION
# Disable debugging by default.
DEBUG = False

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

HASH_PATH_SALT = "ww@m$bup7vk4b"

FILE_UPLOAD_MAX_MEMORY_SIZE = 8988608

UPLOAD_CHUNK_SIZE = 8388608

CAMERA_UPLOAD_ROOT = "/Camera Uploads"

CAMERA_UPLOAD_FOLDER = "Camera Uploads"

CLOUD_CAMERA_ROOT = "/Cloud Camera"

CLOUD_CAMERA_FOLDER = "Cloud Camera"

PHOTOS_ROOT = "/Photos"

PHOTOS_FOLDER = "Photos"

LIST_UNCHANGABLE_PATHS = ["/Camera Uploads","/Cloud Camera","/Photos"]

REST_FRAMEWORK = {
    'EXCEPTION_HANDLER': 'block.utils.custom_exception_handler'
}

JWT_PRIVATE_SIGNATURE = "Y2auJS_2X,Btg6o7J+CVS5c7lHOr;a"


AUDIO_EXTENSION = ["3gp", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb", "dct", "dss", "dvf", "flac",
                   "gsm", "iklax", "ivs", "m4a", "m4b", "m4p", "mmf", "mp3", "mpc", "msv", "ogg", "oga", "opus", "ra ",
                   "rm", "raw", "sln", "tta", "vox", "wav", "wma", "wv", "webm"]

VIDEO_EXTENSION = ["mkv","mp4","avi","mpeg","mpg","wmv","m1v","m2v","mov","rmvb"]

IMAGE_EXTENSION = ["png","jpeg","gif","jpg"]

IMAGE_PREFIX = ["IMG", "CIMG", "DSC", "DSCF", "DSCN", "DUW", "JD", "MGP", "S700", "PICT", "vlcsnap", "KIF", "IMAG"]

THUMBNAIL_SIZES = [(120,120),(350,350),(1100,1100)]
# FIXTURE_DIRS = ['fixtures/']

# CONFIG CURRENT STORAGE TYPE
ALLOWS_STORAGE = { 'swift': 0, 'swift_radosgw': 1, 's3': 2 }
CURRENT_STORAGE = ALLOWS_STORAGE['s3']

DOWNLOAD_URL_DURATION = 60 * 60 * 36

# Default max quota
DEFAULT_QUOTA_CONTAINER = 21474836480

# Max amount of data (in bytes) that can be uploaded. `None` means no limit
MAX_BYTES = None

ACTION_FILE_EXISTS = ['skip', 'override', 'rename', 'cancel']