#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import os
from django.conf import settings
from celery import Celery

# set the default Django settings module for the 'celery' program.
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hipcloud_block_storage.settings')
app = Celery('hipcloud_block_storage')

# export CELERYD_CHDIR=/Users/thaing/Working/SourceCode/hipcloud_block_storage
# export DJANGO_SETTINGS_MODULE=hipcloud_block_storage.settings.local

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')
# For autodiscover_tasks to work, you must define your tasks in a file called 'tasks.py'.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
