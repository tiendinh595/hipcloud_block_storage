#!/usr/bin/env python
# -*- coding: utf-8 -*-
from block.services.swift.files import SwiftStorage
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage, PageNotAnInteger
from block.models.swift import SwiftContainer, FileSystem, SwiftAccount
from block.serializers.api_document import AlbumSerializer
from block.serializers.swift import FileSystemSerializer
from rest_framework.exceptions import PermissionDenied, ParseError
from django.conf import settings
from django.db.models import Q, F
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.utils import timezone
import re
import hashlib
import logging
import os
import copy

from ...models.swift import Album, FileAlbum

logger = logging.getLogger(__name__)


class AlbumUtils(object):

    def __init__(self, user_id=None, container_id=None, album_id=None):
        self.user_id = user_id
        self.container_id = container_id
        if container_id is not None:
            if isinstance(container_id, str) or isinstance(container_id, unicode):
                self._container = SwiftContainer.objects.get(id=container_id)
            elif isinstance(container_id, SwiftContainer):
                self._container = container_id
        else:
            self._container = SwiftContainer.objects.get(account__user_id=user_id)
        if album_id:
            self._album = Album.objects.get(id=album_id, status=Album.ALBUM_ACTIVE)

    @property
    def container(self):
        return self._container

    @container.setter
    def container(self, value):
        self.user_id = value
        self._container = SwiftContainer.objects.get(account__user_id=self.user_id)

    @property
    def album(self):
        return self._album

    def valid_file_name(self, album_name):
        try:
            if str(album_name).lower() == 'photo' or str(album_name).lower() == 'video' or \
                    not re.match(r'^[^\\/?%*:|"<>\.]+$', album_name):
                return False
            else:
                return True
        except:
            return True

    def check_permissions(self, permission):
        """
        Check permission
        :return:
        """
        if not self._album:
            return None
        return (self._album.permissions & permission) == permission

    def get_permissions(self):
        """
        Get permission of folder / file
        :return:
        """
        return self._album.permissions

    def is_creatable(self):
        """
        Check whether new files or folders can be created inside this folder
        :return:
        """
        if not self._album:
            return None
        return (self._album.permissions & Album.PERMISSION_CREATE) == Album.PERMISSION_CREATE

    def is_readable(self):
        """
        Check if the file or folder is readable
        :return:
        """
        if not self._album:
            return None
        return (self._album.permissions & Album.PERMISSION_READ) == Album.PERMISSION_READ

    def is_updateable(self):
        """
        Check if a file is writable
        :return:
        """
        if not self._album:
            return None
        return (self._album.permissions & Album.PERMISSION_UPDATE) == Album.PERMISSION_UPDATE

    def is_deletable(self):
        """
        Check if a file or folder can be deleted
        :return:
        """
        if not self._album:
            return None
        return (self._album.permissions & Album.PERMISSION_DELETE) == Album.PERMISSION_DELETE

    def is_shareable(self):
        """
        Check if a file or folder can be shared
        :return:
        """
        if not self._album:
            return None
        return (self._album.permissions & FileSystem.PERMISSION_SHARE) == FileSystem.PERMISSION_SHARE

    def create(self, album_name, album_type=Album.T_USER_DEFINED_MIXED):
        if not self.valid_file_name(album_name):
            raise ParseError('Album đặt tên không hơp lệ')
        condition = Q(container=self.container) & Q(name=album_name)
        try:
            album = Album()
            album.container = self.container
            album.name = album_name
            album.type = album_type
            album.status = Album.ALBUM_ACTIVE
            album.permissions = Album.PERMISSION_ALL
            album.save()
            self._album = album
            return self._album
        except IntegrityError:
            raise ParseError('Tên album bị trùng')

    def delete(self):
        if not self.is_deletable():
            raise ParseError('Album này không được phép xoá')
        self._album.delete()
        return True

    def rename(self, new_album_name, album_id=None, force=False):
        # check valid filename
        if not self.valid_file_name(new_album_name):
            raise ParseError('Album đặt tên không hợp lệ')
        if album_id:
            condition = Q(container=self.container) & Q(status=Album.ALBUM_ACTIVE) & Q(id=album_id)
            self._album = Album.objects.get(condition)
        # check if the same name
        if self._album.name == new_album_name:
            return self._album.name
        # check permission
        if not self.is_updateable():
            raise ParseError('Album này không được phép đổi tên')
        try:
            condition = Q(container=self.container) & Q(status=Album.ALBUM_ACTIVE) & Q(name=new_album_name)
            other_album = Album.objects.get(condition)
            if not force:
                # in case client not force / confirm to merge album
                raise ParseError("Album bị trùng tên, vui lòng xác nhận trộn album hiện tại với album mới")
            # perform merge albums
            FileAlbum.objects.filter(album=self._album).update(album=other_album)
            self._album.delete()
            self._album = other_album
        except Album.DoesNotExist:
            # perform rename
            self._album.name = new_album_name
            self._album.save()
        return self._album

    def list(self, album_type=None, ordering=('-created_at', )):
        """
        List all albums
        :return:
        """
        conditions = Q(container=self.container) & Q(status=Album.ALBUM_ACTIVE)
        if album_type:
            conditions &= Q(type=album_type)
        albums = Album.objects.filter(conditions).order_by(*ordering)
        return albums

    def add_item_to_album(self, filesystem, check_permission=True):
        if check_permission and not self.is_creatable():
            raise ParseError('Bạn không có quyền thêm nội dung vào album này')

        file_album = FileAlbum()
        file_album.album = self._album
        if isinstance(filesystem, str) or isinstance(filesystem, unicode):
            file_album.filesystem_id = filesystem
            filesystem = FileSystem.objects.get(id=filesystem, status=FileSystem.FILE_ACTIVE)
        else:
            file_album.filesystem = filesystem

        # Check file type and album type
        if self._album.type == Album.T_AUTO_PHOTO:
            if filesystem.kind != FileSystem.IMAGE_KIND:
                raise ParseError('Album được thêm vào không phải là album hình ảnh')
        elif self._album.type == Album.T_AUTO_VIDEO:
            if filesystem.kind != FileSystem.VIDEO_KIND:
                raise ParseError('Album được thêm vào không phải là album video')

        if filesystem.kind not in (FileSystem.IMAGE_KIND, FileSystem.VIDEO_KIND):
            raise ParseError('Nội dung được thêm vào không phải là nội dung video hay hình ảnh')

        # Update album cover if this is the auto alband items is image
        if self._album.type == Album.T_AUTO_PHOTO and filesystem.kind == FileSystem.IMAGE_KIND:
            self._album.last_added_item = filesystem
            self._album.cover_thumbnail_url_tmpl = filesystem.large_thumbnail_url_tmpl

        # Update album cover if this is user defiend album + item is image + have not have cover yet
        if self._album.type == Album.T_USER_DEFINED_MIXED and not self._album.last_added_item \
                and filesystem.kind == FileSystem.IMAGE_KIND:
            self._album.last_added_item = filesystem
            self._album.cover_thumbnail_url_tmpl = filesystem.large_thumbnail_url_tmpl

        self._album.file_count = F('file_count') + 1
        file_album.save()
        self._album.save()
        return self._album

    def remove_item_from_album(self, filesystem, check_permission=True):
        if check_permission and not self.is_deletable():
            raise ParseError('Bạn không có xoá nội dung  album này')

        # Query file input is the id
        if isinstance(filesystem, str) or isinstance(filesystem, unicode):
            filesystem = FileSystem.objects.get(pk=filesystem)

        # Check the mapper
        try:
            current_file_album = FileAlbum.objects.get(album=self._album, filesystem=filesystem)
        except FileAlbum.DoesNotExist:
            raise ParseError('Nội dung cần xoá không thuộc về album đang chọn')

        if self._album.cover_thumbnail_url_tmpl == filesystem.large_thumbnail_url_tmpl:
            self._album.cover_thumbnail_url_tmpl = None

        # If the removed item is the cover
        if self._album.last_added_item_id == filesystem:
            condition = Q(album=self._album) & Q(filesystem__kind=FileSystem.IMAGE_KIND) & ~Q(filesystem=filesystem)
            try:
                query = FileAlbum.objects.filter(condition).select_related('filesystem').first()
                if self._album.type == Album.T_USER_DEFINED_MIXED:
                    specified_file_album = query.first()
                else:
                    specified_file_album = query.last()
                self._album.last_added_item = specified_file_album
                self._album.cover_thumbnail_url_tmpl = specified_file_album.filesystem.large_thumbnail_url_tmpl
            except FileAlbum.DoesNotExist:
                self._album.last_added_item = None
                self._album.cover_thumbnail_url_tmpl = None
        # Reduce count
        self._album.file_count = F('file_count') - 1
        self._album.save()
        current_file_album.delete()
        return self._album

    def list_items_in_album(self):
        if not self.is_readable():
            raise ParseError('Bạn không có quyền xem album này')

        file_ids = FileAlbum.objects.filter(album=self._album).order_by('-created_at').values_list('filesystem', flat=True)
        condition = Q(container=self.container)
        if self._album.type == Album.T_AUTO_PHOTO:
            condition &= Q(kind=FileSystem.IMAGE_KIND) & Q(id__in=file_ids)
        elif self._album.type == Album.T_AUTO_VIDEO:
            condition &= Q(kind=FileSystem.VIDEO_KIND) & Q(id__in=file_ids)
        else:
            condition &= Q(kind__in=(FileSystem.IMAGE_KIND, FileSystem.VIDEO_KIND)) & Q(id__in=file_ids)
        condition &= Q(status=FileSystem.FILE_ACTIVE)
        return FileSystem.objects.filter(condition)

    def get_auto_photo_album(self):
        try:
            album_object = Album.objects.get(container=self.container, name='Photos', type=Album.T_AUTO_PHOTO)
        except Album.DoesNotExist:
            album_object = Album()
            album_object.name = 'Photos'
            album_object.container = self.container
            album_object.type = Album.T_AUTO_PHOTO
            album_object.permissions = Album.PERMISSION_READ
            album_object.status = Album.ALBUM_ACTIVE
            album_object.save()
        return album_object

    def get_auto_video_album(self):
        try:
            album_object = Album.objects.get(container=self.container, name='Videos', type=Album.T_AUTO_VIDEO)
        except Album.DoesNotExist:
            album_object = Album()
            album_object.name = 'Videos'
            album_object.container = self.container
            album_object.type = Album.T_AUTO_VIDEO
            album_object.permissions = Album.PERMISSION_READ
            album_object.status = Album.ALBUM_ACTIVE
            album_object.save()
        return album_object

    @classmethod
    def remove_item_from_all_albums(cls, filesystem):
        # Query file input is the id
        if isinstance(filesystem, str) or isinstance(filesystem, unicode):
            filesystem = FileSystem.objects.get(pk=filesystem)

        # Check the mapper
        queryset = FileAlbum.objects.filter(filesystem=filesystem).select_related('album')
        for current_file_album in queryset:
            try:
                # If the removed item is the cover
                album_object = current_file_album.album
                if album_object.last_added_item_id == filesystem:
                    # Find the other image to replace the cover
                    condition = Q(album=album_object) & Q(filesystem__kind=FileSystem.IMAGE_KIND) & ~Q(filesystem=filesystem)
                    try:
                        query = FileAlbum.objects.filter(condition).select_related('filesystem').first()
                        if album_object.type == Album.T_USER_DEFINED_MIXED:
                            # update oldest image to cover in case user defined album
                            specified_file_album = query.first()
                        else:
                            # update latest image to cover in case auto album
                            specified_file_album = query.last()
                        album_object.last_added_item = specified_file_album
                        album_object.cover_thumbnail_url_tmpl = specified_file_album.filesystem.large_thumbnail_url_tmpl
                    except FileAlbum.DoesNotExist:
                        album_object.last_added_item = None
                        album_object.cover_thumbnail_url_tmpl = None
                # Reduce count
                album_object.file_count = F('file_count') - 1
                album_object.save()
                current_file_album.delete()
            except Exception as e:
                logger.exception(e)
        return True

    def set_cover(self, filesystem, check_permission = True):
        try:
            if check_permission and not self.is_creatable():
                raise ParseError('Bạn không có quyền tạo cover cho album này')

            if isinstance(filesystem, str) or isinstance(filesystem, unicode):
                filesystem = FileSystem.objects.get(id=filesystem, status=FileSystem.FILE_ACTIVE)

            self._album.last_added_item = filesystem
            self._album.cover_thumbnail_url_tmpl = filesystem.large_thumbnail_url_tmpl
            self._album.save()
            return True
        except Exception as e:
            logger.exception(e)
            return False