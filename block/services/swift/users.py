import logging
import pickle

from django.conf import settings
from django.core.cache import cache as _cache_con
from keystoneauth1 import session
from keystoneauth1.identity import v3
from keystoneclient import client

_logger = logging.getLogger(__name__)
# openstack user create --domain default --project demo --password user_password user_test
# openstack role add --project demo --user user_test swiftoperator



class SwiftUserManager(object):
    """
    Adapter for local mysql store with swift remote system
    """

    def __init__(self):
        self.auth = v3.Password(auth_url=settings.SWIFT_AUTH_URL,
                            username=settings.KEYSTONE_ADMIN_USERNAME,
                            password=settings.KEYSTONE_ADMIN_KEY,
                            user_domain_id=settings.KEYSTONE_USER_DOMAIN_ID,
                            project_domain_id=settings.KEYSTONE_PROJECT_DOMAIN_ID,
                            project_name=settings.KEYSTONE_PROJECT_NAME)
        self.domain = None
        self.project = None
        self.keystone = None
        self._init_client()

    def _get_project(self):
        for project in  self.keystone.projects.list():
            if project.name == settings.KEYSTONE_USER_PROJECT_NAME:
                self.project = project
                break
        return self.project

    def _get_domain(self):
        for domain in self.keystone.domains.list():
            if domain.name == settings.KEYSTONE_USER_DOMAIN_NAME:
                self.domain = domain
                break
        return self.domain

    def _init_client(self):
        try:
            _logger.debug("Init session ",str(self.auth))
            sess = session.Session(auth=self.auth)
            _logger.debug("Init client ", str(self.auth) , str(sess))
            self.keystone = client.Client(session=sess)
            self.project = self._get_project()
            self.domain = self._get_domain()
        except Exception as ex:
            print ex
            _logger.error("Connection fail ",str(self.auth))
            return None
        return self.keystone

    def _get_client(self):
        cached_keystone = pickle.loads(_cache_con.get("KEYSTONECLIENTCACHE"))
        if not cached_keystone:
            if self._init_client():
                _cache_con.set("KEYSTONECLIENTCACHE",pickle.dumps(self.keystone), 1800)
            else:
                raise LookupError("Client is not created")
        else:
            self.keystone = cached_keystone
            return self.keystone

    def create_user(self, username , password , project, temp_key_1 , temp_key_2):
        """
        Creates a user.
        Error response codes:201,413,415,405,404,403,401,400,503,409,
        :param username:
        :param password:
        :param project:
        :param temp_key_1:
        :param temp_key_2:
        :return: keystoneclient.v3.users.User
        """
        return self.keystone.users.create(name=username, password=password, project=project,
                                        x_account_meta_temp_url_key=temp_key_1,
                                        x_account_meta_temp_url_key_2=temp_key_2)

    def update_user(self, user , username , password , email = '', description = '', domain = 'default', project = 'admin'):
        """
        Update user.
        Normal response codes: 200 Error response codes:413,405,404,403,401,400,503,

        :param user:
        :param username:
        :param password:
        :param email:
        :param description:
        :param domain:
        :param project:
        :return: keystoneclient.v3.users.User
        """
        self.keystone.user(user, username, domain=domain, project=project, password=password, email=email, description=description)

    def get_user(self, user_id):
        """
        :param username:
        :return: keystoneclient.v3.users.User
        """
        return self.keystone.users.get(user_id)

    def delete_user(self, user_id):
        """
        Delete user based on user_id
        Normal response codes: 200 Error response codes:413,405,404,403,401,400,503,
        :return:
        """
        user = self.keystone.users.get(user_id)
        return user.delete()

    def list_roles(self):
        """
        List all roles in db
        :return:
        """
        return self.keystone.roles.list()

    def get_role(self, role_id):
        """
        :param role_id:
        :return:
        """
        return self.keystone.roles.get(role_id)

    def assign_role(self, user , role):
        return self.keystone.roles.grant(role, user, domain=self.domain)


