#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from keystoneclient.exceptions import AuthorizationFailure
from swiftclient.utils import generate_temp_url
from block.models.swift import SwiftContainer
from django.conf import settings
import time
import logging
import requests
import boto
import boto.s3.connection
from django_redis import get_redis_connection

CACHE_DOWNLOAD = get_redis_connection("url_rewrite")

logger = logging.getLogger(__name__)


class SwiftStorage():

    def __init__(self, account, container):
        self.account = account
        self.container = container
        self.container_id = container.id
        self.account_id = account.username
        self.account_path = container.path
        self.bucket = None
        self.get_token()

    def get_token(self):
        """
        ref : https://github.com/openstack/python-keystoneclient/blob/71af540c81ecb933d912ef5ecde128afcc0deeeb/keystoneclient/access.py
        :return: access.AccessInfo
        """
        if self.container.type == SwiftContainer.S3:
            try:
                self.s3_conn = boto.connect_s3(
                    aws_access_key_id = self.account.access_key,
                    aws_secret_access_key = self.account.secret_key,
                    host = settings.S3_HOST,
                    port = settings.S3_PORT,
                    is_secure = False,
                    calling_format = boto.s3.connection.OrdinaryCallingFormat()
                )
                self.bucket = self.s3_conn.get_bucket(self.container.id)
            except Exception as ex:
                logger.error(ex)
                return None
            return self.bucket
        else:
            try:
                self.auth_object = settings.KEYSTONE.get_raw_token_from_identity_service(auth_url=settings.KEYSTONE_HOST,
                                                                               username=self.account_id,
                                                                               password=self.account.password,
                                                                               project_id=self.account.project_id,
                                                                               project_name=self.account.project_name,
                                                                               user_domain_id=settings.KEYSTONE_USER_DOMAIN_ID,
                                                                               project_domain_id=settings.KEYSTONE_PROJECT_DOMAIN_ID)
            except AuthorizationFailure as ex:
                logger.error(ex)
                return None

            self.upload_host = ""
            self.auth_token = self.auth_object.auth_token
            self.expires = self.auth_object.expires
            return self.auth_object

    def upload_chunk(self, local_file_path, file_id, chunk_count):
        if self.container.type == SwiftContainer.DEFAULT or self.container.type == SwiftContainer.SWIFT_RADOSGW:
            try:
                chunk_count += 1
                chunk_count = str(chunk_count)
                chunk_index = "00000000"[:len(chunk_count) * -1] + chunk_count
                upload_path = "{host}{account_path}/{container_id}/{file_id}/{chunk_index}".format(host=settings.SWIFT_HOST,
                                                                                                   account_path=self.account_path,
                                                                                                   container_id=self.container_id,
                                                                                                   file_id=file_id,
                                                                                                   chunk_index = chunk_index)
                with open(local_file_path,'rb') as f:
                    response_data = requests.put(upload_path, data=f, headers={"X-Auth-Token": self.auth_token, "Content-Type": ""})
                    return response_data.headers['Etag'] , response_data.headers['X-Trans-Id']
            except Exception as ex:
                logger.error(ex)
                return False
        elif self.container.type == SwiftContainer.S3:
            try:
                chunk_count += 1
                if chunk_count == 1:
                    mp = self.bucket.initiate_multipart_upload(file_id)
                    with open(local_file_path, 'rb') as fp:
                        mp.upload_part_from_file(fp, chunk_count)
                else:
                    for mp in self.bucket.list_multipart_uploads():
                        if mp.key_name == file_id:
                            with open(local_file_path, 'rb') as fp:
                                mp.upload_part_from_file(fp, chunk_count)
                            break
            except Exception as ex:
                logger.error(ex)
                return False

    def upload_single_file(self, local_file_path, file_id):
        if self.container.type == SwiftContainer.DEFAULT or self.container.type == SwiftContainer.SWIFT_RADOSGW:
            try:
                upload_path = "{host}{account_path}/{container_id}/{file_id}".format(host=settings.SWIFT_HOST,account_path=self.account_path,
                                                                                  container_id=self.container_id,file_id=file_id)
                with open(local_file_path,'rb') as f:
                    response_data = requests.put(upload_path, data=f, headers={"X-Auth-Token": self.auth_token, "Content-Type": ""})
                    return response_data.headers['Etag'] , response_data.headers['X-Trans-Id']
            except Exception as ex:
                logger.error(ex)
                return False
        elif self.container.type == SwiftContainer.S3:
            try:
                key = self.bucket.new_key(file_id)
                key.set_contents_from_filename(local_file_path)
                return True
            except Exception as ex:
                logger.exception(ex)
                return False

    def upload_manifest(self , file_id):
        if self.container.type == SwiftContainer.DEFAULT or self.container.type == SwiftContainer.SWIFT_RADOSGW:
            try:
                upload_path = "{host}{account_path}/{container_id}/{file_id}".format(host=settings.SWIFT_HOST,account_path=self.account_path,
                                                                                  container_id=self.container_id,file_id=file_id)
                manifest = "/".join(upload_path.split("/")[-2:])
                response_data = requests.put(upload_path, headers={"X-Auth-Token": self.auth_token, "X-Object-Manifest": manifest})
                return response_data.headers['Etag'] , response_data.headers['X-Trans-Id']
            except Exception as ex:
                logger.error(ex)
                return False
        elif self.container.type == SwiftContainer.S3:
            for file_uploaded in self.bucket.list_multipart_uploads():
                if file_uploaded.key_name == file_id:
                    file_uploaded.complete_upload()

    def generate_download_url(self, file_id, download_method = "GET"):
        if self.container.type == SwiftContainer.DEFAULT or self.container.type == SwiftContainer.SWIFT_RADOSGW:
            duration_in_seconds = 60 * 60 * 36                  # 24 hours
            build_path = '/v1{account_path}/{container_id}/{file}'.format(account_path=self.container.path,
                                                                       container_id=self.container_id,file=file_id)
            return settings.SWIFT_EXTERNAL_DOMAIN + generate_temp_url(build_path, duration_in_seconds, str(self.container.temp_url_secret_key_2), download_method)
        elif self.container.type == SwiftContainer.S3:
            duration_in_seconds = 60 * 60 * 36
            return self.s3_conn.generate_url(duration_in_seconds, "GET", self.container.id, file_id)


    @staticmethod
    def get_download_url(container, file_object, download_method="GET"):
        file_id = file_object.id
        file_name = file_object.file_name
        new_file_id = file_id.replace("fid:", "")
        if container.type == SwiftContainer.DEFAULT or container.type == SwiftContainer.SWIFT_RADOSGW:
            download_url = "{host}/{file_id}/{file_name}".format(
                host=settings.DOWNLOAD_HOST,
                file_id=new_file_id,
                file_name=file_name.replace(" ","-")
            )
            build_path = "/v1{account_path}/{container_id}/{file}".format(account_path=container.path,
                                                                          container_id=container.id, file=file_id)

            source_download_url = generate_temp_url(build_path, settings.DOWNLOAD_URL_DURATION,
                                                    container.temp_url_secret_key_2,
                                                    download_method)
            CACHE_DOWNLOAD.set(new_file_id, source_download_url[1:], settings.DOWNLOAD_URL_DURATION)
            return download_url, source_download_url
        elif container.type == SwiftContainer.S3:
            download_url = "{host}/{file_id}/{file_name}".format(
                host=settings.DOWNLOAD_HOST,
                file_id=file_id.replace("fid:",""),
                file_name=file_name.replace(" ","-")
            )
            account = container.account
            s3_conn = boto.connect_s3(
                aws_access_key_id=account.access_key,
                aws_secret_access_key=account.secret_key,
                host=settings.S3_HOST,
                port=settings.S3_PORT,
                is_secure=False,
                calling_format=boto.s3.connection.OrdinaryCallingFormat()
            )
            source_download_url = s3_conn.generate_url(settings.DOWNLOAD_URL_DURATION,
                                                       download_method, container.id,
                                                       file_id)

            if settings.S3_PORT == 80:
                url_search = "http://" + str(settings.S3_HOST) + "/"
            else:
                url_search = "http://" + settings.S3_HOST + ':' + str(settings.S3_PORT) + "/"

            CACHE_DOWNLOAD.set(new_file_id,
                               source_download_url.replace(url_search, ""),
                               settings.DOWNLOAD_URL_DURATION)

            return download_url, source_download_url


    @staticmethod
    def get_share_url(container, file_object, download_method="GET"):
        file_id = file_object.id
        file_name = file_object.file_name
        new_file_id = file_id.replace("fid:", "")
        if container.type == SwiftContainer.DEFAULT or container.type == SwiftContainer.SWIFT_RADOSGW:
            download_url = "{host}/{file_id}/{file_name}".format(
                host=settings.SHARE_HOST,
                file_id=new_file_id,
                file_name=file_name.replace(" ","-")
            )
            build_path = "/v1{account_path}/{container_id}/{file}".format(account_path=container.path,
                                                                          container_id=container.id, file=file_id)

            source_download_url = generate_temp_url(build_path, settings.DOWNLOAD_URL_DURATION,
                                                    container.temp_url_secret_key_2,
                                                    download_method)
            CACHE_DOWNLOAD.set(new_file_id, source_download_url[1:], settings.DOWNLOAD_URL_DURATION)
            return download_url, source_download_url
        elif container.type == SwiftContainer.S3:
            download_url = "{host}/{file_id}/{file_name}".format(
                host=settings.SHARE_HOST,
                file_id=file_id.replace("fid:",""),
                file_name=file_name.replace(" ","-")
            )
            account = container.account
            s3_conn = boto.connect_s3(
                aws_access_key_id=account.access_key,
                aws_secret_access_key=account.secret_key,
                host=settings.S3_HOST,
                port=settings.S3_PORT,
                is_secure=False,
                calling_format=boto.s3.connection.OrdinaryCallingFormat()
            )
            source_download_url = s3_conn.generate_url(settings.DOWNLOAD_URL_DURATION,
                                                       download_method, container.id,
                                                       file_id)

            if settings.S3_PORT == 80:
                url_search = "http://" + str(settings.S3_HOST) + "/"
            else:
                url_search = "http://" + settings.S3_HOST + ':' + str(settings.S3_PORT) + "/"

            CACHE_DOWNLOAD.set(new_file_id,
                               source_download_url.replace(url_search, ""),
                               settings.DOWNLOAD_URL_DURATION)
            return download_url, source_download_url

    @staticmethod
    def get_temp_url(container, file_id, download_method="GET", use_https=getattr(settings, 'THUMB_URL_HTTPS_SCHEME', True)):
        if container.type == SwiftContainer.DEFAULT or container.type == SwiftContainer.SWIFT_RADOSGW:
            duration_in_seconds = 60 * 60 * 36                  # 24 hours
            build_path = '/v1{account_path}/{container_id}/{file}'.format(account_path=container.path,
                                                                       container_id=container.id,file=file_id)
            return settings.SWIFT_EXTERNAL_DOMAIN + generate_temp_url(build_path, duration_in_seconds, str(container.temp_url_secret_key_2), download_method)
        elif container.type == SwiftContainer.S3:
            account = container.account
            duration_in_seconds = 60 * 60 * 36
            s3_conn = boto.connect_s3(
                aws_access_key_id=account.access_key,
                aws_secret_access_key=account.secret_key,
                host=settings.S3_HOST,
                port=settings.S3_PORT,
                is_secure=False,
                calling_format=boto.s3.connection.OrdinaryCallingFormat()
            )
            url = s3_conn.generate_url(duration_in_seconds, "GET", container.id, file_id)
            url = url.replace("http://", "https://") if use_https else url
            url = url.replace('172.17.0.4', '103.68.252.191:8081') # dev only
            return url

    @staticmethod
    def get_temp_thumb_url(container, file_id, download_method="GET", use_https=getattr(settings, 'THUMB_URL_HTTPS_SCHEME', True)):
        if container.type == SwiftContainer.DEFAULT or container.type == SwiftContainer.SWIFT_RADOSGW:
            duration_in_seconds = 60 * 60 * 36                  # 24 hours
            build_path = '/v1{account_path}/{container_id}/{file}'.format(account_path=container.path,
                                                                       container_id=container.id,file=file_id)
            return settings.SWIFT_EXTERNAL_DOMAIN + generate_temp_url(build_path, duration_in_seconds, str(container.temp_url_secret_key_2), download_method)
        elif container.type == SwiftContainer.S3:
            account = container.account
            duration_in_seconds = 60 * 60 * 36
            s3_conn = boto.connect_s3(
                aws_access_key_id=account.access_key,
                aws_secret_access_key=account.secret_key,
                host=settings.S3_HOST,
                port=settings.S3_PORT,
                is_secure=False,
                calling_format=boto.s3.connection.OrdinaryCallingFormat()
            )
            url = s3_conn.generate_url(duration_in_seconds, "GET", container.id, file_id)
            url = url.replace("http://", "https://") if use_https else url
            url = url.replace('172.17.0.4', '103.68.252.191:8081')  # dev only
            return url
            #.replace("http://" + settings.S3_HOST, "https://thumbs.upbox.vn")

    def get_file_from_storage(self, file_id, filename, temp_file_path, container):
        import urllib
        try:
            download = urllib.URLopener()
            user_path = settings.SITE_ROOT + settings.MEDIA_URL + temp_file_path
            full_path = os.path.join(user_path, filename)
            url_file = SwiftStorage.get_temp_url(container, file_id, "GET")
            print("---- url file: ", url_file)
            # url_file = self.generate_download_url(file_id=file_id)
            download.retrieve(url_file, full_path)
            return full_path
        except Exception as ex:
            logger.exception(ex)
            return ''
