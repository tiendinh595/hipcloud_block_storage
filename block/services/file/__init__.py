#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db.models import Q

from block.services.swift.files import SwiftStorage
from block.models.swift import SwiftContainer, FileSystem, SwiftAccount
from block.services.album import AlbumUtils
from rest_framework.exceptions import PermissionDenied, ParseError
from django.conf import settings
from django.db.models import Sum
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.utils import timezone
import re
import hashlib
import logging
import os
import copy

logger = logging.getLogger(__name__)


class FileContainer(object):
    """
        FilePath object for managing state of filesystem session interaction
    """

    def __init__(self, user_id):
        self.user_id = user_id
        self._container = SwiftContainer.objects.get(account__user_id=user_id)

    @property
    def container(self):
        return self._container

    @container.setter
    def container(self, value):
        self.user_id = value
        self._container = SwiftContainer.objects.get(account__user_id=self.user_id)


class FileSystemUtils(object):
    @staticmethod
    def valid_file_name(file_name):
        try:
            if not re.match(r'^[^\\/?%*:|"<>]+$', file_name):
                return False
            else:
                return True
        except:
            return True


class FileSystemPath(object):
    """
        FilePath object for managing state of filesystem session interaction
    """

    def __init__(self, fq_path):
        self.fq_path = fq_path
        self._path = fq_path
        self.parent_path = None
        self.path_hash = None
        self.path_data = None
        self.is_dir = False
        self.is_root = False
        self.container = None
        self.build_path()

    def md5(self, value):
        m = hashlib.md5()
        m.update(value)
        return m.hexdigest()

    def build_path(self):
        value = self._path
        self.path_hash = self.md5(value)
        if value == "/":
            self.is_dir = True
            self.is_root = True
            self.parent_path = "/"
        else:
            self.parent_path = value.rsplit('/', 1)[0] + "/"

    def __eq__(self, other):
        return self.fq_path == other.fq_path

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value
        self.build_path()

    def init_user_container(self, user_id):
        # Get swift account or create one if not exists
        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            self.container = container
            return self
        except Exception as ex:
            raise ParseError("Container not found")

    def valid_file_name(file_name):
        try:
            if not re.match(r'^[^\\/?%*:|"<>\.]+$', file_name):
                return False
            else:
                return True
        except:
            return True

    def check_permissions(self, permission):
        """
        Check permission
        :return:
        """
        if self.is_root:
            return True
        return (self.path_data.permissions & permission) == permission

    def get_permissions(self):
        """
        Get permission of folder / file
        :return:
        """
        return self.path_data.permissions

    def is_creatable(self):
        """
        Check whether new files or folders can be created inside this folder
        :return:
        """
        if self.is_root:
            return True
        return (self.path_data.permissions & FileSystem.PERMISSION_CREATE) == FileSystem.PERMISSION_CREATE

    def is_readable(self):
        """
        Check if the file or folder is readable
        :return:
        """
        if self.is_root:
            return True
        return (self.path_data.permissions & FileSystem.PERMISSION_READ) == FileSystem.PERMISSION_READ

    def is_updateable(self):
        """
        Check if a file is writable
        :return:
        """
        if self.is_root:
            return True
        return (self.path_data.permissions & FileSystem.PERMISSION_UPDATE) == FileSystem.PERMISSION_UPDATE

    def is_deletable(self):
        """
        Check if a file or folder can be deleted
        :return:
        """
        if self.is_root:
            return True
        return (self.path_data.permissions & FileSystem.PERMISSION_DELETE) == FileSystem.PERMISSION_DELETE

    def is_shareable(self):
        """
        Check if a file or folder can be shared
        :return:
        """
        if self.is_root:
            return True
        return (self.path_data.permissions & FileSystem.PERMISSION_SHARE) == FileSystem.PERMISSION_SHARE

    def list(self, ordering=[], only_folder=None):
        """
        List all item by current path instance
        :return:
        """
        if self.is_root:
            files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                          parent=None,
                                                                          status=FileSystem.FILE_ACTIVE)
        else:
            files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                          parent=self.path_data.id,
                                                                          status=FileSystem.FILE_ACTIVE)

        if only_folder != None:
            files = files.filter(is_dir=only_folder)

        files = files.order_by(*ordering)

        return files

    def list_recursive(self, recursive, ordering=[]):
        """
        List all item by current path instance
        :return:
        """
        if self.is_root:
            if recursive:
                files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                              status=FileSystem.FILE_ACTIVE).order_by(
                    *ordering)
            else:
                files = FileSystem.objects.select_related('container').filter(container=self.container, parent=None,
                                                                              status=FileSystem.FILE_ACTIVE).order_by(
                    *ordering)
        else:
            if recursive:
                files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                              fq_path__startswith=self.path_data.fq_path,
                                                                              status=FileSystem.FILE_ACTIVE).order_by(
                    *ordering)

            else:
                files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                              parent=self.path_data.id,
                                                                              status=FileSystem.FILE_ACTIVE).order_by(
                    *ordering)
        return files

    def list_custom(self, condition={}, ordering=[]):
        """
        List item by condition
        :return:
        """
        base_condition = {"container": self.container, "status": FileSystem.FILE_ACTIVE}
        base_condition.update(condition)
        if self.is_root:
            base_condition['parent'] = None
            files = FileSystem.objects.select_related('container').filter(**base_condition).filter(Q(kind=FileSystem.IMAGE_KIND) | Q(kind=FileSystem.VIDEO_KIND)).order_by(*ordering)
        else:
            base_condition['parent'] = self.path_data.id
            files = FileSystem.objects.select_related('container').filter(**base_condition).filter(Q(kind=FileSystem.IMAGE_KIND) | Q(kind=FileSystem.VIDEO_KIND)).order_by(*ordering)
        return files

    def list_recents(self, ordering=[]):
        """
        List all files
        :return:
        """
        if self.is_root:
            files = FileSystem.objects.select_related('container').filter(container=self.container, is_dir=False,
                                                                          status=FileSystem.FILE_ACTIVE).order_by(
                *ordering)
        else:
            files = FileSystem.objects.select_related('container').filter(container=self.container, is_dir=False,
                                                                          status=FileSystem.FILE_ACTIVE).order_by(
                *ordering)
        return files

    def list_deleted(self, ordering=[]):
        """
        List all deleted file / folder
        :return:
        """
        if self.is_root:
            files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                          status=FileSystem.FILE_DELETED).order_by(
                *ordering)
        else:
            files = FileSystem.objects.select_related('container').filter(container=self.container,
                                                                          status=FileSystem.FILE_DELETED).order_by(
                *ordering)
        return files

    def notify(self):
        pass

    def sync_photos_folder(self):
        """
        Create photos folder in root if not exists
        :return:
        """
        # Camera Uploads
        try:
            photos_folder = FileSystem.objects.get(container=self.container,
                                                   fq_path_reference=self.path_hash,
                                                   type=FileSystem.ALBUM_FOLDER_TYPE)
            # If previous camera folder deleted - reactive it
            if photos_folder.status == FileSystem.FILE_DELETED:
                photos_folder.status = FileSystem.FILE_ACTIVE
                photos_folder.save()
                photos_folder.index()
        except:
            photos_folder = FileSystem(
                container=self.container,
                type=FileSystem.ALBUM_FOLDER_TYPE,
                kind='folder',
                icon='album_1',
                fq_path_reference=self.md5(settings.PHOTOS_ROOT),
                fq_path=self.fq_path,
                file_name=settings.PHOTOS_FOLDER,
                swift_href=self.container.name,
                is_dir=True,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_ACTIVE
            )
            photos_folder.save()
            photos_folder.index()

        self.path_data = photos_folder
        self.is_dir = photos_folder.is_dir
        return self

    def sync_path(self):
        """
        Query path from db to local instance
        :return:
        """
        if self.is_root:
            self.path_data = None
        else:
            try:
                data = FileSystem.objects.get(container=self.container, fq_path_reference=self.path_hash)
                self.path_data = data
                self.is_dir = data.is_dir
                return self
            except Exception as ex:
                raise ParseError("Path not found")

    def rename(self, new_name):
        """
        Rename current file path to a new name
        :param new_name: str
        :return:
        """
        if self.is_root:
            raise ParseError("Root can't be rename")

        if self.path_data.type == FileSystem.ALBUM_FOLDER_TYPE and self.fq_path == "/Camera Uploads":
            raise ParseError("Xin không đổi tên thư mục Camera Upload")

        if not self.is_updateable():
            raise PermissionDenied("Permission denied")

        new_name = new_name.encode('utf-8')
        if new_name == self.path_data.file_name:
            return self.path_data

        original_parent_path = self.path_data.fq_path + "/"
        self.path_data.file_name = new_name
        new_path = self.parent_path + new_name
        if self.is_dir:
            self.path_data.user_last_modified = self.path_data.user_last_modified
        else:
            self.path_data.user_last_modified = timezone.now()
        self.path_data.fq_path = new_path
        self.path_data.fq_path_reference = self.md5(new_path)
        try:
            self.path_data.save()
            self.path_data.index()
            if self.is_dir:
                sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
                for item in sub_items:
                    sub_alt_path = item.fq_path.replace(original_parent_path, new_path + "/")
                    item.fq_path = sub_alt_path
                    item.user_last_modified = item.user_last_modified
                    item.fq_path_reference = self.md5(sub_alt_path)
                    item.save()
                    item.index()
        except IntegrityError as ex:
            raise ParseError("Please try another name")
        except Exception as ex:
            raise ParseError(ex)
        self.notify()
        return self.path_data

    def create_album(self, album_name):
        folder_name = album_name.encode('utf-8')

        if not self.is_dir:
            raise ParseError("Path is not valid for create folder")

        # Check permission
        if not self.is_creatable():
            raise PermissionDenied("Permission denied")

        # Check current folder exits - if exists turn status on
        try:
            fq_path_ref = self.md5("{path}/{folder_name}".format(path=self.path, folder_name=folder_name))
            filesystem = FileSystem.objects.get(parent=self.path_data,
                                                container=self.container,
                                                fq_path_reference=fq_path_ref,
                                                type=FileSystem.ALBUM_FOLDER_TYPE)
            filesystem.status = FileSystem.FILE_ACTIVE
            filesystem.save()
            filesystem.index()
            self.notify()
            return filesystem
        except Exception as ex:
            full_path = "{path}/{folder_name}".format(path=self.path, folder_name=folder_name)
            album_filesystem = FileSystem(
                parent=self.path_data,
                container=self.container,
                type=FileSystem.ALBUM_FOLDER_TYPE,
                kind='folder',
                icon='album_1',
                fq_path_reference=self.md5(full_path),
                fq_path=full_path,
                file_name=folder_name,
                is_dir=True,
                bytes=0,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_ACTIVE
            )
            try:
                album_filesystem.save()
                album_filesystem.index()
                self.notify()
                return album_filesystem
            except Exception as ex:
                logger.error(ex)
                raise ParseError(ex)

    def create_sub_folder(self, folder_name):
        folder_name = folder_name.encode('utf-8')

        if not self.is_dir:
            raise ParseError("Path is not valid for create folder")

        # Check permission
        if not self.is_creatable():
            raise PermissionDenied("Permission denied")

        # Check current folder exits - if exists turn status on
        try:
            if self.is_root:
                fq_path_ref = self.md5("/" + folder_name)
            else:
                fq_path_ref = self.md5("{path}/{folder_name}".format(path=self.path, folder_name=folder_name))

            filesystem = FileSystem.objects.get(fq_path_reference=fq_path_ref, type=FileSystem.FOLDER_TYPE,
                                                container=self.container)
            filesystem.status = FileSystem.FILE_ACTIVE
            filesystem.save()
            filesystem.index()
            return filesystem
        except Exception as ex:
            pass
        # Create folder
        if self.is_root:
            filesystem = FileSystem(
                parent=None,
                container=self.container,
                type=FileSystem.FOLDER_TYPE,
                kind='folder',
                icon='folder_1',
                fq_path_reference=self.md5("/" + folder_name),
                fq_path="/" + folder_name,
                file_name=folder_name,
                is_dir=True,
                bytes=0,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_ACTIVE
            )
        else:
            full_path = "{path}/{folder_name}".format(path=self.path, folder_name=folder_name)
            filesystem = FileSystem(
                parent=self.path_data,
                container=self.container,
                type=FileSystem.FOLDER_TYPE,
                kind='folder',
                icon='folder_1',
                fq_path_reference=self.md5(full_path),
                fq_path=full_path,
                file_name=folder_name,
                is_dir=True,
                bytes=0,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_ACTIVE
            )

        try:
            filesystem.save()
            filesystem.index()
        except Exception as ex:
            logger.error(ex)
            raise ParseError(ex)
        self.notify()
        return filesystem

    def move(self, to_path):
        """
        :param to_path: FileSystemPath object
        :return:
        """
        to_fq_path = to_path.fq_path

        original_parent_path = self.path_data.fq_path + "/"
        root_alt_path = to_fq_path + "/" + self.path_data.file_name
        # Case move to root

        if to_path.is_root:
            root_alt_path = to_fq_path + self.path_data.file_name
        else:
            root_alt_path = to_fq_path + "/" + self.path_data.file_name
        # Case move to root
        if to_path.path_data:
            self.path_data.parent = to_path.path_data
            self.path_data.fq_path = root_alt_path
            self.path_data.fq_path_reference = self.md5(root_alt_path)
        # Case move to folder
        else:
            self.path_data.parent = None
            self.path_data.fq_path = root_alt_path
            self.path_data.fq_path_reference = self.md5(root_alt_path)
        self.path_data.save()
        self.path_data.index()

        if self.is_dir:
            sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
            for item in sub_items:
                sub_alt_path = item.fq_path.replace(original_parent_path, root_alt_path + "/")
                item.user_last_modified = timezone.now()
                item.fq_path = sub_alt_path
                item.fq_path_reference = self.md5(sub_alt_path)
                item.save()
                item.index()
        self.notify()
        return self.path_data

    def move_v2(self, to_path, action=None):
        """
        :param to_path: FileSystemPath object
        :return:
        """
        to_fq_path = to_path.fq_path

        original_parent_path = self.path_data.fq_path + "/"
        root_alt_path = to_fq_path + "/" + self.path_data.file_name
        # Case move to root

        if to_path.is_root:
            root_alt_path = to_fq_path + self.path_data.file_name
        else:
            root_alt_path = to_fq_path + "/" + self.path_data.file_name

        print('root_alt_path', root_alt_path)

        try:
            file_move_filesystem = FileSystem.objects.get(fq_path=root_alt_path, container=self.container)
        except:
            file_move_filesystem = None

        if file_move_filesystem == None:
            # Case move to folder
            if to_path.path_data:
                self.path_data.parent = to_path.path_data
            # Case move to root
            else:
                self.path_data.parent = None
        else:
            if self.is_dir:
                self.path_data.count = file_move_filesystem.count
                sub_files = FileSystem.objects.filter(parent=file_move_filesystem, container=self.container)
                for item in sub_files:
                    print('sub_files: ', item.file_name)
                    item.parent = self.path_data
                    item.save()
                file_move_filesystem.delete()
            else:
                if action == 'rename':
                    print('----rename-----')
                    count = file_move_filesystem.count
                    if not count:
                        count = 1

                    new_filename = get_increasing_file(file_move_filesystem.file_name, count, self.path_data.is_dir)
                    self.path_data.file_name = new_filename

                    if to_path.is_root:
                        root_alt_path = to_fq_path + new_filename
                    else:
                        root_alt_path = to_fq_path + "/" + new_filename

                    file_move_filesystem.count += 1
                    file_move_filesystem.save()
                elif action == 'override':
                    print('----override-----')
                    self.path_data.count = file_move_filesystem.count
                    sub_files = FileSystem.objects.filter(parent=file_move_filesystem, container=self.container)
                    for item in sub_files:
                        print('---sub_files---', item.file_name)
                        item.parent = self.path_data
                        item.save()
                    file_move_filesystem.delete()
                    try:
                        file_move_filesystem.delete_index()
                    except:
                        pass
                elif action == 'skip':
                    print('----skip-----')
                    return self.path_data
        self.path_data.fq_path = root_alt_path
        self.path_data.fq_path_reference = self.md5(root_alt_path)

        if to_path.path_data:
            self.path_data.parent = to_path.path_data
        else:
            self.path_data.parent = None
        self.path_data.save()
        self.path_data.index()

        if self.is_dir:
            print('--is_dir----')
            sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
            for item in sub_items:
                print ('----sub_items----', item.fq_path)
                sub_alt_path = item.fq_path.replace(original_parent_path, root_alt_path + "/")
                print('----sub_alt_path----', sub_alt_path)

                try:
                    sub_file_move_filesystem = FileSystem.objects.get(fq_path=sub_alt_path, container=self.container)
                except:
                    sub_file_move_filesystem = None

                if sub_file_move_filesystem != None:
                    if item.is_dir:
                        print('----item.is_dir---')
                        sub_files = FileSystem.objects.filter(parent=sub_file_move_filesystem, container=self.container)
                        for it in sub_files:
                            print('----sub_files---', it.file_name)
                            it.parent = item
                            it.save()

                        item.count = sub_file_move_filesystem.count
                        sub_file_move_filesystem.delete()
                    else:
                        if action == 'rename':
                            count = sub_file_move_filesystem.count
                            if not count:
                                count = 1
                            new_filename = get_increasing_file(sub_file_move_filesystem.file_name, count,
                                                               sub_file_move_filesystem.is_dir)
                            tmp_sub_alt_path = sub_alt_path.split('/')
                            tmp_sub_alt_path[-1] = new_filename
                            sub_alt_path = '/'.join(tmp_sub_alt_path)
                            item.file_name = new_filename
                            sub_file_move_filesystem.count += 1
                            sub_file_move_filesystem.save()
                            print('----item rename', new_filename)

                        elif action == 'override':
                            print('----item override---')
                            item.count = sub_file_move_filesystem.count
                            sub_file_move_filesystem.delete()
                            try:
                                sub_file_move_filesystem.delete_index()
                            except:
                                pass
                        elif action == 'skip':
                            print('----item skip---')
                            continue

                item.user_last_modified = timezone.now()
                item.fq_path = sub_alt_path
                item.fq_path_reference = self.md5(sub_alt_path)
                item.save()
                item.index()
        self.notify()
        return self.path_data

    def delete(self):
        # Disable all its child

        self.path_data.status = FileSystem.FILE_DELETED
        queryset = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container,
                                             status=FileSystem.FILE_ACTIVE)
        for filesystem in queryset:
            filesystem.status = FileSystem.FILE_DELETED
            filesystem.save()
            AlbumUtils.remove_item_from_all_albums(filesystem=filesystem)
        sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
        for item in sub_items:
            try:
                item.index()
            except: pass
        self.notify()
        return self.path_data

    def restore(self):
        self.path_data.status = FileSystem.FILE_ACTIVE
        queryset = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container,
                                             status=FileSystem.FILE_DELETED)
        album_utils = AlbumUtils(container_id=self.container)
        auto_photo_album = album_utils.get_auto_photo_album()
        auto_video_album = album_utils.get_auto_video_album()
        for filesystem in queryset:
            filesystem.status = FileSystem.FILE_ACTIVE
            # filesystem.user_last_modified = timezone.now()
            filesystem.save()
            # Update auto albums
            if filesystem.kind == FileSystem.IMAGE_KIND:
                if isinstance(filesystem.image_info, dict):
                    try:
                        if int(filesystem.image_info['width']) >= 800 and int(filesystem.image_info['height']) >= 800:
                            album_utils._album = auto_photo_album
                            album_utils.add_item_to_album(filesystem=filesystem, check_permission=False)
                    except KeyError:
                        pass
                    except ValueError:
                        pass
            elif filesystem.kind == FileSystem.VIDEO_KIND:
                album_utils._album = auto_video_album
                album_utils.add_item_to_album(filesystem=filesystem, check_permission=False)

        sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
        for item in sub_items:
            item.index()
        self.notify()
        return self.path_data

    def delete_permanent(self):
        self.path_data.status = FileSystem.FILE_CLEARED
        # sum all items
        total_size = FileSystem.objects.filter(fq_path__startswith=self.fq_path,
                                               container=self.container,
                                               status=FileSystem.FILE_DELETED).aggregate(Sum('bytes'))
        if total_size:
            total_size = total_size['bytes__sum']
        else:
            total_size = 0
        FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container,
                                  status=FileSystem.FILE_DELETED).update(
            status=FileSystem.FILE_CLEARED
        )
        # Remove quota code go here
        try:
            self.container.quota_count -= total_size
        except:
            pass
        sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
        for item in sub_items:
            try:
                item.delete_index()
            except:
                pass
        self.container.save()
        self.notify()
        return self.path_data

    def empty_trash(self):
        # sum all items
        total_size = FileSystem.objects.filter(container=self.container,
                                               status=FileSystem.FILE_DELETED).aggregate(Sum('bytes'))
        if total_size:
            total_size = total_size['bytes__sum']
        else:
            total_size = 0
        FileSystem.objects.filter(container=self.container,
                                  status=FileSystem.FILE_DELETED).update(
            status=FileSystem.FILE_CLEARED
        )
        # Remove quota code go here
        try:
            self.container.quota_count -= total_size
        except:
            pass
        self.container.save()
        return self.path_data

    def get_file_extension(self, file_name):
        try:
            return file_name.split(".")[-1]
        except:
            return ""

    @staticmethod
    def active_file(self, file_id, extra={}):
        try:
            extra['status'] = FileSystem.FILE_ACTIVE
            self.notify()
            activated_file = FileSystem.objects.get(id=file_id, status=FileSystem.FILE_UPLOADING).update(**extra)
            activated_file.index()
            return activated_file
        except:
            raise ParseError("File not found")

    def create_file(self, file_name, bytes):
        if not self.is_dir:
            raise ParseError("Path is not valid for create file")
        # Check permission and file exists
        if not self.is_creatable():
            raise PermissionDenied("Permission denied")

        extension = self.get_file_extension(file_name)
        # Create folder
        if self.is_root:
            full_path = "/" + file_name
            filesystem = FileSystem(
                container=self.container,
                type=FileSystem.FILE_TYPE,
                extension=extension,
                kind='file',
                icon='file_1',
                fq_path_reference=self.md5(full_path),
                fq_path=full_path,
                file_name=file_name,
                is_dir=False,
                is_starred=False,
                bytes=bytes,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_INIT
            )
        else:
            full_path = "{path}/{file_name}".format(path=self.path, file_name=file_name)
            filesystem = FileSystem(
                parent=self.path_data,
                container=self.container,
                type=FileSystem.FILE_TYPE,
                extension=extension,
                kind='file',
                icon='file_1',
                fq_path_reference=self.md5(full_path),
                fq_path=full_path,
                file_name=file_name,
                is_dir=False,
                is_starred=False,
                bytes=bytes,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_INIT
            )
        try:
            filesystem.save()
        except Exception as ex:
            raise ParseError(ex)
        self.notify()
        return filesystem

    def copy(self, to_path, storage):
        """
        :param to_path: FileSystemPath object
        :param storage: SwiftStorage object
        :return:
        """
        to_fq_path = to_path.fq_path
        if to_path.is_root:
            new_path = to_fq_path + self.path_data.file_name
        else:
            new_path = to_fq_path + "/" + self.path_data.file_name

        try:
            file_copy_filesystem = FileSystem.objects.get(fq_path=new_path, container=self.container)
        except:
            file_copy_filesystem = None

        try:
            if file_copy_filesystem == None:
                file_copy = FileSystem(
                    container=self.container,
                    parent=to_path.path_data,
                    type=self.path_data.type,
                    kind=self.path_data.kind,
                    extension=self.path_data.extension,
                    icon=self.path_data.icon,
                    fq_path_reference=self.md5(new_path),
                    fq_path=new_path,
                    file_name=self.path_data.file_name,
                    swift_href=self.path_data.swift_href,
                    is_dir=self.path_data.is_dir,
                    bytes=self.path_data.bytes,
                    user_last_modified=timezone.now(),
                    status=self.path_data.status
                )
                file_copy.save()
                file_copy.index()
            else:
                count = file_copy_filesystem.count
                if not count:
                    count = 1

                new_filename = get_increasing_file(file_copy_filesystem.file_name, count, self.path_data.is_dir)

                if to_path.is_root:
                    new_file_path = to_fq_path + new_filename
                else:
                    new_file_path = to_fq_path + "/" + new_filename

                file_copy = FileSystem(
                    container=self.container,
                    parent=to_path.path_data,
                    type=self.path_data.type,
                    kind=self.path_data.kind,
                    extension=self.path_data.extension,
                    icon=self.path_data.icon,
                    fq_path_reference=self.md5(new_file_path),
                    fq_path=new_file_path,
                    file_name=new_filename,
                    swift_href=self.path_data.swift_href,
                    is_dir=self.path_data.is_dir,
                    bytes=self.path_data.bytes,
                    user_last_modified=timezone.now(),
                    status=self.path_data.status
                )
                file_copy_filesystem.count += 1
                file_copy_filesystem.save()
                file_copy.save()
                file_copy.index()

            # clone file in storage
            if file_copy.is_dir == False:
                print('--start clone file')
                storage.bucket.copy_key(file_copy.id, storage.container.id, self.path_data.id)
                print('--ended clone file')

                # copy thumbnail only for image
                if self.path_data.extension in settings.IMAGE_EXTENSION:
                    if self.path_data.thumbnail_url_tmpl != '':
                        try:
                            small_thumb = generate_name_thumb(file_copy.file_name, file_copy.bytes, (120, 120),
                                                              self.path_data.extension)
                            storage.bucket.copy_key(small_thumb, storage.container.id,
                                                    self.path_data.thumbnail_url_tmpl)
                            file_copy.thumbnail_url_tmpl = small_thumb
                        except Exception as e:
                            print('-err thumbnail_url_tmpl', e)

                    if self.path_data.large_thumbnail_url_tmpl != '':
                        try:
                            large_thumb = generate_name_thumb(file_copy.file_name, file_copy.bytes, (350, 350),
                                                              self.path_data.extension)
                            storage.bucket.copy_key(large_thumb, storage.container.id,
                                                    self.path_data.large_thumbnail_url_tmpl)
                            file_copy.large_thumbnail_url_tmpl = large_thumb
                        except Exception as e:
                            print('-err large_thumbnail_url_tmpl', e)

                    if self.path_data.album_large_thumbnail_url_tmpl != '':
                        try:
                            album_large_thumb = generate_name_thumb(file_copy.file_name, file_copy.bytes, (1100, 1100),
                                                                    self.path_data.extension)
                            storage.bucket.copy_key(album_large_thumb, storage.container.id,
                                                    self.path_data.album_large_thumbnail_url_tmpl)
                            file_copy.album_large_thumbnail_url_tmpl = album_large_thumb
                        except Exception as e:
                            print('-err album_large_thumbnail_url_tmpl', e)

                    file_copy.user_last_modified = timezone.now()
                    file_copy.save()
            print('copied {} to {}'.format(self.path_data.fq_path, file_copy.fq_path))
            return file_copy, True
        except Exception as e:
            file_copy.delete()
            return self.fq_path, False

    def copy_v2(self, to_path, storage, action):
        """
        :param to_path: FileSystemPath object
        :param storage: SwiftStorage object
        :return:
        """
        to_fq_path = to_path.fq_path
        if to_path.is_root:
            new_path = to_fq_path + self.path_data.file_name
        else:
            new_path = to_fq_path + "/" + self.path_data.file_name

        try:
            file_copy_filesystem = FileSystem.objects.get(fq_path=new_path, container=self.container)
        except:
            file_copy_filesystem = None

        try:
            if file_copy_filesystem == None:
                file_copy = FileSystem(
                    container=self.container,
                    parent=to_path.path_data,
                    type=self.path_data.type,
                    kind=self.path_data.kind,
                    extension=self.path_data.extension,
                    icon=self.path_data.icon,
                    fq_path_reference=self.md5(new_path),
                    fq_path=new_path,
                    file_name=self.path_data.file_name,
                    swift_href=self.path_data.swift_href,
                    is_dir=self.path_data.is_dir,
                    bytes=self.path_data.bytes,
                    user_last_modified=timezone.now(),
                    status=self.path_data.status
                )
                file_copy.save()
                file_copy.index()
            else:
                if self.path_data.is_dir == True:
                    file_copy = file_copy_filesystem
                else:
                    if action == 'rename':
                        count = file_copy_filesystem.count
                        if not count:
                            count = 1

                        new_filename = get_increasing_file(file_copy_filesystem.file_name, count, self.path_data.is_dir)

                        if to_path.is_root:
                            new_file_path = to_fq_path + new_filename
                        else:
                            new_file_path = to_fq_path + "/" + new_filename

                        file_copy = FileSystem(
                            container=self.container,
                            parent=to_path.path_data,
                            type=self.path_data.type,
                            kind=self.path_data.kind,
                            extension=self.path_data.extension,
                            icon=self.path_data.icon,
                            fq_path_reference=self.md5(new_file_path),
                            fq_path=new_file_path,
                            file_name=new_filename,
                            swift_href=self.path_data.swift_href,
                            is_dir=self.path_data.is_dir,
                            bytes=self.path_data.bytes,
                            user_last_modified=timezone.now(),
                            status=self.path_data.status
                        )
                        file_copy_filesystem.count += 1
                        file_copy_filesystem.save()

                    else:
                        file_copy = file_copy_filesystem
                        file_copy.type = self.path_data.type
                        file_copy.kind = self.path_data.type
                        file_copy.extension = self.path_data.extension
                        file_copy.icon = self.path_data.icon
                        file_copy.is_dir = self.path_data.is_dir
                        file_copy.bytes = self.path_data.bytes
                        file_copy.user_last_modified = timezone.now()
                        file_copy.status = self.path_data.status

            file_copy.save()
            file_copy.index()
            # clone file in storage
            if file_copy.is_dir == False:
                print('--start clone file')
                storage.bucket.copy_key(file_copy.id, storage.container.id, self.path_data.id)
                print('--ended clone file')

                # copy thumbnail only for image
                if self.path_data.extension in settings.IMAGE_EXTENSION:
                    if self.path_data.thumbnail_url_tmpl != '':
                        try:
                            small_thumb = generate_name_thumb(file_copy.file_name, file_copy.bytes, (120, 120),
                                                              self.path_data.extension)
                            storage.bucket.copy_key(small_thumb, storage.container.id,
                                                    self.path_data.thumbnail_url_tmpl)
                            file_copy.thumbnail_url_tmpl = small_thumb
                        except Exception as e:
                            print('-err thumbnail_url_tmpl', e)

                    if self.path_data.large_thumbnail_url_tmpl != '':
                        try:
                            large_thumb = generate_name_thumb(file_copy.file_name, file_copy.bytes, (350, 350),
                                                              self.path_data.extension)
                            storage.bucket.copy_key(large_thumb, storage.container.id,
                                                    self.path_data.large_thumbnail_url_tmpl)
                            file_copy.large_thumbnail_url_tmpl = large_thumb
                        except Exception as e:
                            print('-err large_thumbnail_url_tmpl', e)

                    if self.path_data.album_large_thumbnail_url_tmpl != '':
                        try:
                            album_large_thumb = generate_name_thumb(file_copy.file_name, file_copy.bytes, (1100, 1100),
                                                                    self.path_data.extension)
                            storage.bucket.copy_key(album_large_thumb, storage.container.id,
                                                    self.path_data.album_large_thumbnail_url_tmpl)
                            file_copy.album_large_thumbnail_url_tmpl = album_large_thumb
                        except Exception as e:
                            print('-err album_large_thumbnail_url_tmpl', e)

                    file_copy.user_last_modified = timezone.now()
                    file_copy.save()
            print('copied {} to {}'.format(self.path_data.fq_path, file_copy.fq_path))
            return file_copy, True
        except Exception as e:
            file_copy.delete()
            return self.fq_path, False

    def size(self):
        if self.is_dir:
            sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
            size = 0
            for item in sub_items:
                size += item.bytes
            return size
        else:
            return self.path_data.bytes

    def exists(self, to_path):
        to_fq_path = to_path.fq_path

        original_parent_path = self.path_data.fq_path + "/"
        root_alt_path = to_fq_path + "/" + self.path_data.file_name

        if to_path.is_root:
            root_alt_path = to_fq_path + self.path_data.file_name
        else:
            root_alt_path = to_fq_path + "/" + self.path_data.file_name

        if self.is_dir == False:
            try:
                FileSystem.objects.get(fq_path=root_alt_path, container=self.container)
                return True
            except:
                return False
        else:
            sub_items = FileSystem.objects.filter(fq_path__startswith=self.fq_path, container=self.container)
            for item in sub_items:
                sub_alt_path = item.fq_path.replace(original_parent_path, root_alt_path + "/")
                print('sub_alt_path', sub_alt_path)
                print('item.fq_path', item.fq_path)
                try:
                    FileSystem.objects.get(fq_path=sub_alt_path, container=self.container, is_dir=False)
                    return True
                except:
                    pass
            return False


def generate_name_thumb(file_name, original_size, size, extension):
    h = hashlib.sha1(':'.join([file_name, ':'.join(str(original_size)),
                               ':'.join(str(i) for i in size)])).hexdigest()
    tmp_name = h[:3] + h[3:]
    return 'thumb.' + str(tmp_name) + "." + extension


def get_increasing_file(filename, index, is_folder):
    if is_folder:
        return filename + '(' + str(index) + ')'

    extension = filename.split(".")[-1]
    filename_only = filename.replace("." + extension, "")
    return filename_only + "(" + str(index) + ")." + extension
