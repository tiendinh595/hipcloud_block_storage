#!/usr/bin/env python
# -*- coding: utf-8 -*-
import hashlib
import logging
from django.conf import settings
from block.models.swift import FileSystem

logger = logging.getLogger(__name__)

def hash_path(path):
    m = hashlib.md5()
    m.update(path)
    return m.hexdigest()


def get_file_kind(file_extension):
    if file_extension in settings.AUDIO_EXTENSION:
        return FileSystem.AUDIO_KIND
    elif file_extension in settings.VIDEO_EXTENSION:
        return FileSystem.VIDEO_KIND
    elif file_extension in settings.IMAGE_EXTENSION:
        return FileSystem.IMAGE_KIND
    return 'default'