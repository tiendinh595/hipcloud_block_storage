from django.http import JsonResponse
from rest_framework import status
from django.http import HttpResponse

def fail(message):
    # localize message here
    # custom data here
    return JsonResponse({"error":400,"message":message}, safe=False, status=status.HTTP_400_BAD_REQUEST)

def error(message):
    # localize message here
    # custom data here
    return JsonResponse({"error":500,"message":message}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

def unauthorize(message):
    # localize message here
    # custom data here
    return JsonResponse({"error":401,"message":message}, safe=False, status=status.HTTP_401_UNAUTHORIZED)

def forbidden(message):
    # localize message here
    # custom data here
    return JsonResponse({"error":403,"message":message}, safe=False, status=status.HTTP_403_FORBIDDEN)

def confirm(message):
    return JsonResponse({"error":0,"message":message}, safe=False, status=status.HTTP_200_OK)

def success(data):
    return JsonResponse(data, safe=False, status=status.HTTP_200_OK)

def ipn_success():
    return HttpResponse('')

