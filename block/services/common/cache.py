#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.cache import caches
import pickle
try: import ujson as json
except ImportError: import json


class AdvancedRedisCache(object):
    """
    :type connection: redis.client.StrictRedis
    """
    connection = None
    prefix = ''

    def __init__(self, alias='default'):
        """
        Initial method
        :param alias: the alias of redis cache
        """
        cache = caches[alias]
        if not hasattr(cache.client, 'get_client'):
            raise NotImplementedError("This backend does not supports this feature")

        self.connection = cache.client.get_client(True)
        self.prefix = cache.key_prefix

    def get_key(self, key):
        return self.prefix + key

    def set_object(self, key, value, expire=18000):
        """
        Set simple key to redis
        :param key: The key of cache
        :type key: str
        :param expire: Expired time
        :type expire: int
        """
        pickled_object = pickle.dumps(value)
        self.connection.set(self.get_key(key), pickled_object, expire)

    def get_object(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        data = self.connection.get(self.get_key(key))
        if data:
            return pickle.loads(data)
        else:
            return None

    def set(self, key, value, expire=18000):
        """
        Set simple key to redis
        :param key: The key of cache
        :type key: str
        :param expire: Expired time
        :type expire: int
        """
        self.connection.set(self.get_key(key), value, timeout = expire)

    def get(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        return self.connection.get(self.get_key(key))

    def incr(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        return self.connection.incr(self.get_key(key))


    def delete(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        return self.connection.delete(self.get_key(key))

    def hkeys(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        return self.connection.hkeys(self.get_key(key))

    def hmset(self, key, mapping, expire=1200):
        """
        Multiple set hash object via hmset by pipeline transaction
        :param key: Key of object
        :type key: str
        :param mapping: Hash object
        :type mapping: dict
        :param expire: Expired time
        :type expire: int
        :return: list
        """
        key = self.get_key(key)
        pipe = self.connection.pipeline()
        pipe.hmset(key, mapping)
        if expire == 0:
            pipe.persist(key)
        else:
            pipe.expire(key, expire)

        return pipe.execute()

    def hmget(self, key, field):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hmget(key, field)

    def hlen(self, key):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hlen(key)

    def hdel(self, key, subkey):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hdel(key, subkey)

    def llen(self, key):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.llen(key)

    def lpush(self, key, field):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.lpush(key, field)

    def lpop(self, key):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.lpop(key)

    def rpop(self, key):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.rpop(key)


    def hsetnx(self, key, field, value):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hsetnx(key, field, value)

    def hgetall(self, key):
        """
        Multiple set hash object via hmset by pipeline transaction
        :param key: Key of object
        :type key: str
        """
        key = self.get_key(key)
        return self.connection.hgetall(key)

    def exists(self, key):
        """
        Check if key exists
        :param key: Key of object
        :type key: str
        :return: boolean
        """
        key = self.get_key(key)
        return self.connection.exists(key)

    def hexists(self, key, field):
        """
        Check if key exists
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        :return: boolean
        """
        key = self.get_key(key)
        return self.connection.hexists(key, field)

    def expire(self, key, seconds):
        """
        Check if key exists
        :param key: Key of object
        :type key: str
        :param seconds: Number of seconds this cache is expired
        :type seconds: int
        :return: boolean
        """
        key = self.get_key(key)
        return self.connection.expire(key, seconds)

    def ttl(self, key):
        """
        :type key: str
        :return: int
        """
        key = self.get_key(key)
        return self.connection.ttl(key)

    def flushdb(self):
        """
        Flush all key in database
        """
        return self.connection.flushdb()


CACHE_DOWNLOAD = caches['url_rewrite']
CACHE_WEB = AdvancedRedisCache('default')