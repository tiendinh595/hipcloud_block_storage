#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, re, subprocess, json

"""
Usage :
info = MediaInfo(filename = "/Users/thaing/Downloads/GOPR9715.MP4")
infoData = info.getInfo()
print infoData
"""

class MediaInfo:
    def __init__(self, **kwargs):
        self.filename = kwargs.get('filename')
        self.cmd = kwargs.get('cmd')
        self.info = dict()

        if self.filename == None:
            self.filename = ''

        if self.cmd == None:
            self.cmd = 'mediainfo'

    def getInfo(self):
        # if not os.path.exists(self.filename) or not os.path.exists(self.cmd) :
        if not os.path.exists(self.filename):
            return None

        cmdName = os.path.basename(self.cmd)

        self._mediainfoGetInfo()

        return self.info

    def _mediainfoGetInfo(self):
        cmd = self.cmd + ' -f ' + self.filename
        outputBytes = ''

        try:
            outputBytes = subprocess.check_output(cmd, shell=True)
        except subprocess.CalledProcessError as e:
            return ''

        outputText = outputBytes.decode('utf-8')

        self.info = self._mediainfoGetInfoRegex(outputText)

    def _mediainfoGetInfoRegex(self, sourceString):
        mediaInfo = dict()

        general = re.search("(^General\n.*?\n\n)", sourceString, re.S)
        if general:
            generalInfo = general.group(0)

            container = re.search("Format\s*:\s*([\w\_\-\\\/\. ]+)\n", generalInfo, re.S)
            fileSize = re.search("File size\s*:\s*(\d+)\.?\d*\n", generalInfo, re.S)
            duration = re.search("Duration\s*:\s*(\d+)\.?\d*\n", generalInfo, re.S)
            format_profile = re.search("Format profile\s*:\s*([\w\_\-\\\/\. ]+)\n", generalInfo, re.S)
            bitrate = re.search("Overall bit rate\s*:\s*(\d+)\.?\d*\n", generalInfo, re.S)
            is_steamable = re.search("IsStreamable\s*:\s*([\w\_\-\\\/\. ]+)\n", generalInfo, re.S)
            stream_size = re.search("Stream size\s*:\s*(\d+)\.?\d*\n", generalInfo, re.S)

            mediaInfo['format'] = container.group(1)
            mediaInfo['fileSize'] = fileSize.group(1)
            mediaInfo['duration'] = duration.group(1)
            mediaInfo['bitrate'] = bitrate.group(1)
            if format_profile:
                mediaInfo['formatProfile'] = format_profile.group(1)
            if is_steamable:
                mediaInfo['isStreamable'] = True if is_steamable.group(1) == 'Yes' else False
            if stream_size:
                mediaInfo['streamSize'] = stream_size.group(1)

        video = re.search("(Video[\s\#\d]*\n.*?\n\n)", sourceString, re.S)
        if video:
            videoInfo = video.group(0)

            videoCodec = re.search("Codec\s*:\s*([\w\_\-\\\/\. ]+)\n", videoInfo, re.S)
            videoCodecProfile = re.search("Codec profile\s*:\s*([\w\_\-\\\/\@\. ]+)\n", videoInfo, re.S)
            videoDuration = re.search("Duration\s*:\s*(\d+)\.?\d*\n", videoInfo, re.S)
            videoBitrate = re.search("Bit rate\s*:\s*(\d+)\n", videoInfo, re.S)
            videoWidth = re.search("Width\s*:\s*(\d+)\n", videoInfo, re.S)
            videoHeight = re.search("Height\s*:\s*(\d+)\n", videoInfo, re.S)
            videoAspectRatio = re.search("Display aspect ratio\s*:\s*([\d\.]+)\n", videoInfo, re.S)
            videoFrameRate = re.search("Frame rate\s*:\s*([\d\.]+)\n", videoInfo, re.S)
            videoFrameCount = re.search("Frame count\s*:\s*(\d+)\.?\d*\n", videoInfo, re.S)

            if videoCodec:
                mediaInfo['videoCodec'] = videoCodec.group(1)
            if videoCodecProfile:
                mediaInfo['videoCodecProfile'] = videoCodecProfile.group(1)
            if videoDuration:
                mediaInfo['videoDuration'] = videoDuration.group(1)
            if videoBitrate:
                mediaInfo['videoBitrate'] = videoBitrate.group(1)
            if videoWidth:
                mediaInfo['videoWidth'] = videoWidth.group(1)
            if videoHeight:
                mediaInfo['videoHeight'] = videoHeight.group(1)
            if videoAspectRatio:
                mediaInfo['videoAspectRatio'] = videoAspectRatio.group(1)
            if videoFrameRate:
                mediaInfo['videoFrameRate'] = videoFrameRate.group(1)
            if videoFrameCount:
                mediaInfo['videoFrameCount'] = videoFrameCount.group(1)

        audio = re.search("(Audio[\s\#\d]*\n.*?\n\n)", sourceString, re.S)
        if audio:
            audioInfo = audio.group(0)

            tmpAudioCodec = re.search("Codec\s*:\s*([\w\_\-\\\/ ]+)\n", audioInfo, re.S)
            audioCodec = re.search("\w+", tmpAudioCodec.group(1), re.S)
            audioCodecProfile = re.search("Codec profile\s*:\s*([\w\_\-\\\/\@\. ]+)\n", audioInfo, re.S)
            if audioCodecProfile is None:
                audioCodecProfile = re.search("Format profile\s*:\s*([\w\_\-\\\/\@\. ]+)\n", audioInfo, re.S)

            audioDuration = re.search("Duration\s*:\s*(\d+)\.?\d*\n", audioInfo, re.S)
            audioBitrate = re.search("Bit rate\s*:\s*(\d+)\n", audioInfo, re.S)
            audioChannel = re.search("Channel\(s\)\s*:\s*(\d+)\n", audioInfo, re.S)
            samplingRate = re.search("Sampling rate\s*:\s*([\w\_\-\\\/\@\. ]+)\n", audioInfo, re.S)
            if samplingRate:
                audioSamplingRate = re.search("\d+", samplingRate.group(1), re.S)
                if audioSamplingRate :
                    mediaInfo['audioSamplingRate'] = audioSamplingRate.group(0)

            if audioCodec:
                mediaInfo['audioCodec'] = audioCodec.group(0)
            if audioCodecProfile:
                mediaInfo['audioCodecProfile'] = audioCodecProfile.group(1)
            if audioDuration:
                mediaInfo['audioDuration'] = audioDuration.group(1)
            if audioBitrate:
                mediaInfo['audioBitrate'] = audioBitrate.group(1)
            if audioChannel:
                mediaInfo['audioChannel'] = audioChannel.group(1)

        return mediaInfo
