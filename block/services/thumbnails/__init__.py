#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image , ExifTags
from PIL.ExifTags import TAGS
from django.conf import settings
from block.tasks.tracking import track
import hashlib
import json
import logging

logger = logging.getLogger(__name__)

def get_exif(info):
    ret = {}
    for tag, value in info.items():
        decoded = TAGS.get(tag, tag)
        if isinstance(value, bytes):
            ret[decoded] = value.decode("utf-8", "replace")
        else:
            ret[decoded] = value
    return ret

def generate_filename(original_file_name, original_size, cropped_size):
    h = hashlib.sha1(':'.join([original_file_name,  ':'.join(str(i) for i in original_size), ':'.join(str(i) for i in cropped_size)])).hexdigest()
    return h[:3] + h[3:]

def generate_thumbnail(filename, absolute_file_path):
    extension = filename.split(".")[-1].lower()
    if extension not in settings.IMAGE_EXTENSION:
        return
    target_sizes = settings.THUMBNAIL_SIZES

    img = Image.open(absolute_file_path)
    original_size = img.size

    # Rotate image if exif
    track_exif = {'width': original_size[0], 'height': original_size[1]}
    try:
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                break
        exif = dict(img._getexif().items())
        print('exif[orientation]:  ', exif[orientation])
        if exif:
            track_exif.update(get_exif(exif))
        if exif[orientation] == 3:
            img = img.rotate(180, expand=True)
        elif exif[orientation] == 6:
            img = img.rotate(270, expand=True)
        elif exif[orientation] == 8:
            img = img.rotate(90, expand=True)
    except Exception as ex:
        logger.exception(ex)
        pass

    local_paths = []
    output_extension = "jpeg"
    if extension == "png" or extension == "gif":
        output_extension = "png"

    for size in target_sizes:
        thumb = img.copy()
        # Make thumbnail
        thumb.thumbnail(size)
        thumb_name = "thumb." + generate_filename(absolute_file_path, original_size , size) + "." + output_extension
        local_save_path = settings.THUMBNAIL_STORE_PATH +'/'+ thumb_name
        local_paths.append([ local_save_path, thumb_name])
        # Save output
        thumb.save(local_save_path, output_extension.upper())
    return local_paths , track_exif