#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers
from block.services.search.models import FileSystemSearch

class FileSystemSerializer(serializers.ModelSerializer):

    class Meta:
        model = FileSystemSearch
        fields = ("id", "type", "kind", "extension", "icon", "fq_path", "file_name", "is_dir")
