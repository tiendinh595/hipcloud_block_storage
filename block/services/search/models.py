#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import get_version
from block.models.swift import FileSystem
from elasticsearch import Elasticsearch
from django.conf import settings
from django.db.models import Model
from django.db.models.signals import post_save, post_delete
from django.db.models.signals import post_migrate
from django.db.models.signals import class_prepared
import requests
import json

from block.services.swift.files import SwiftStorage


class FileSystemSearch(FileSystem):
    """
    Model for searching data that's built on top of FileSystem model
    """
    class Meta:
        proxy = True

    @staticmethod
    def search(container, keyword, page = 0, limit = 100):
        res = settings.ES.search(index=settings.ELASTICSEARCH_INDEX, doc_type= settings.ELASTICSEARCH_DOCTYPE,
                                 body={
                                       "from": page,
                                       "size": limit,
                                       "_source": [
                                           "fid", "type", "kind", "extension", "icon", "fq_path", "file_name", "is_dir",
                                           "thumbnail_url_tmpl", "large_thumbnail_url_tmpl", "album_large_thumbnail_url_tmpl",
                                       ],
                                       "query": {
                                          "filtered": {
                                             "query": {
                                                "match": {
                                                   "_all": {
                                                      "query": keyword,
                                                      "operator": "and"
                                                   }
                                                }
                                             },
                                             "filter": {
                                                "bool": {
                                                   "must": [
                                                        {
                                                            "term": {
                                                                "status": FileSystemSearch.FILE_ACTIVE
                                                            }
                                                        },
                                                        {
                                                            "term": {
                                                                "container": container.id
                                                            }
                                                        }
                                                   ]
                                                }
                                             }
                                          }
                                       }
                                    })

        items = list()
        for i in res['hits']['hits']:
            # Skip in case bad value
            if (not i) or (not isinstance(i, dict)) or ('_source' not in i) or (not i['_source']) \
                    or (not isinstance(i['_source'], dict)):
                continue

            # Modify url thumb
            item = i['_source']
            if 'thumbnail_url_tmpl' in i['_source'] and item['thumbnail_url_tmpl']:
                item.update({
                    "thumbnail_url_tmpl": SwiftStorage.get_temp_thumb_url(container, item['thumbnail_url_tmpl'], "GET"),
                })
            else:
                item['thumbnail_url_tmpl'] = ''

            if 'large_thumbnail_url_tmpl' in item and item['large_thumbnail_url_tmpl']:
                item.update({
                    "large_thumbnail_url_tmpl": SwiftStorage.get_temp_thumb_url(container, item['large_thumbnail_url_tmpl'], "GET"),
                })
            else:
                item['large_thumbnail_url_tmpl'] = ''

            if 'album_large_thumbnail_url_tmpl' in item and item['album_large_thumbnail_url_tmpl']:
                item.update({
                    "album_large_thumbnail_url_tmpl": SwiftStorage.get_temp_thumb_url(container, item['album_large_thumbnail_url_tmpl'], "GET"),
                })
            else:
                item['album_large_thumbnail_url_tmpl'] = ''

            items.append(item)
        return {
            "items": items,
            "metadata": {
                "total": res['hits']['total'],
                "page": page,
                "limit": limit
            }
        }

