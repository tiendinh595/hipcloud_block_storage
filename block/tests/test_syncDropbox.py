#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import skip

from django.conf import settings
from django.test import TestCase
import logging

from block.models import FileSystem
from block.models import SyncSession
from block.tasks.sync_data_v2 import dropbox_check_quota_logic, sync_dropbox_logic

logger = logging.getLogger(__name__)

class DropboxTestCase(TestCase):
    fixtures = ['block_sync_dropbox_data.json']

    def setUp(self):
        settings.DEBUG = True
        self.dropbox_token = 'Gu7MyIyjalAAAAAAAAAANJs2T1beuGXflC4McLR8tgwwgFO3EIxKK-mybUl3C1F0'

    # @skip('K test')
    def test_check_quota_exceed_their_limit(self):
        user_id = 'tester_id_1'
        container_id = 'upbox_51074842f29cc3dceb2d77044c_test_1'
        try:
            result = dropbox_check_quota_logic(self.dropbox_token, container_id, user_id)
            self.assertEqual(result, 'error')
        except Exception as ex:
            self.fail(ex.message)

    # @skip('K test')
    def test_check_quota_allowed_to_sync(self):
        user_id = 'tester_id_2'
        container_id = 'upbox_51074842f29cc3dceb2d77044c_test_2'
        try:
            result = dropbox_check_quota_logic(self.dropbox_token, container_id, user_id)
            self.assertEqual(result, 'success')
        except Exception as ex:
            self.fail(ex.message)

    # @skip('K test')
    def test_sync_quota_allowed_to_sync(self):
        user_id = 'tester_id_3'
        container_id = 'upbox_51074842f29cc3dceb2d77044c_test_3'
        sync_session_id = '3'
        try:
            sync_dropbox_logic(self.dropbox_token, sync_session_id, container_id, user_id)
            files = FileSystem.objects.filter(container__id=container_id, is_dir=0)
            logger.info('-- Sync session --')
            sync_session = SyncSession.objects.get(id=sync_session_id)
            logger.info(sync_session.__dict__)
            logger.info('-- List files sync --')
            for item in files:
                logger.info(item.fq_path+' -- Sync status: '+str(item.status))
        except Exception as ex:
            self.fail(ex.message)