#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from block.tasks.sync_data import sync_dropbox_logic , sync_folders, sync_files, sync_google_drive_logic, validate_google_drive_token
from block.models.swift import FileSystem,SwiftContainer,SwiftAccount
from block.services.swift.files import SwiftStorage
from block.services.common import hash_path , get_file_kind
from django.utils import timezone
import dropbox
import json
from  rest_framework.response import Response
import math
import os
import logging
from django.test import Client


logger = logging.getLogger(__name__)


class SyncRemoteDataTestCase(TestCase):
    fixtures = ['block_filesystem.json']

    def setUp(self):
        self.user_id = "tester_id"
        self.access_token = "5075284997574d7f84dd8334a7c1d284"

        self.google_access_token = "ya29.Ci-vA8d_jRLfliSFWTuD0GIP1gw08pHb2EyUxeZFTgL0zbFoxmMaIOvkBBevJutpmw"

        # self.google_access_token = "ya29.Ci-rA4Me9TDShmleBiwhMW0meuCW5LP2JZSCvEstISi1xPWBzQPM0QgH6g6MYCX2tg"
        # self.google_refresh_token = "1/RyIGSHOqv2bEyHPMzSsF-CnHu3zwaOvsdu2XfVpnZSE"

        # self.dropbox_token = "0ntqt8UyQrAAAAAAAAAAW9N8NAmRas8cnJjSxmcuDDrlFJ2hNr8ALEz5166qppLF"
        self.dropbox_token = "HxWmOhyXfCAAAAAAAAABK_4xXglZVPmZtL7CyUb3KJIMAF9eWEKfrCLSIDH3P4aI"

    # def test_validate_google_drive_token_with_refresh(self):
        # self.assertEqual(validate_google_drive_token(self.google_access_token, self.google_refresh_token),True)
    #
    def test_validate_google_drive_token_without_refresh(self):
        self.assertEqual(validate_google_drive_token(self.google_access_token), True)

    def test_sync_google_drive(self):
        sync_google_drive_logic(self.google_access_token, 'test_container_id', ["root"])

    # def test_sync_dropbox_token(self):
    #     for i in FileSystem.objects.filter(container="test_account_1_container"):
    #         parent = i.parent.fq_path if i.parent else ""
    #
    #     sync_dropbox_logic(self.dropbox_token , ["/Camera Uploads"],
    #                        "/", "test_account_1_container", "test_account_1")
    #
    #     for i in FileSystem.objects.filter(container="test_account_1_container"):
    #         parent = i.parent.fq_path if i.parent else ""
    #         print(
    #             parent, " | ", i.fq_path, " su| ", i.fq_path_reference, " | ", i.status, " | ", i.kind, " | ",
    #             i.file_name,
    #             " | ", i.kind)


