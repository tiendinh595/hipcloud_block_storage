#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from block.models.swift import FileSystem
from rest_framework.test import APIClient
from block.services.file import FileSystemPath, FileSystemUtils
from django.test import TestCase
from rest_framework import status
import json
import logging
import hashlib

def md5(value):
    m = hashlib.md5()
    m.update(value)
    return m.hexdigest()

logger = logging.getLogger(__name__)

class AlbumTestCase(TestCase):
    fixtures = ['block_album.json']

    def setUp(self):
        self.user_id = "tester_id"
        self.access_token = "5075284997574d7f84dd8334a7c1d284"

    def test_create_album(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        album_name = "my_album_1"
        response = client.post(path="/block/album/cmd/", data={'name': album_name})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        try:
            my_album_folder = FileSystem.objects.get(fq_path="/Photos/"+album_name)
            self.assertEqual(my_album_folder.file_name, album_name)
            self.assertEqual(my_album_folder.fq_path_reference, md5("/Photos/"+album_name))
            self.assertEqual(my_album_folder.type, FileSystem.ALBUM_FOLDER_TYPE)
        except:
            self.fail("Album was not created")
        try:
            my_photo_folder = FileSystem.objects.get(fq_path="/Photos")
            self.assertEqual(my_photo_folder.file_name, "Photos")
            self.assertEqual(my_photo_folder.fq_path_reference, md5("/Photos"))
            self.assertEqual(my_photo_folder.type, FileSystem.ALBUM_FOLDER_TYPE)
        except:
            self.fail("Photos was not created")

    def test_create_deleted_album(self):
        # Create new album
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        album_name = "my_album_1"
        response = client.post(path="/block/album/cmd/", data={'name': album_name})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Delete created album
        file_path = FileSystemPath("/Photos/" + album_name)
        file_path.init_user_container(self.user_id).sync_path()
        file_path.delete()

        # Check status
        my_album_folder = FileSystem.objects.get(fq_path="/Photos/" + album_name)
        self.assertEqual(my_album_folder.status, FileSystem.FILE_DELETED)

        # Recreate delete album
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        album_name = "my_album_1"
        response = client.post(path="/block/album/cmd/", data={'name': album_name})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        try:
            my_album_folder = FileSystem.objects.get(fq_path="/Photos/"+album_name, status=FileSystem.FILE_ACTIVE)
            self.assertEqual(my_album_folder.file_name, album_name)
            self.assertEqual(my_album_folder.fq_path_reference, md5("/Photos/"+album_name))
            self.assertEqual(my_album_folder.type, FileSystem.ALBUM_FOLDER_TYPE)
        except:
            self.fail("Album was not created")


    def test_delete_album(self):
        # Create new album
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        album_name = "my_album_1"
        response = client.post(path="/block/album/cmd/", data={'name': album_name})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        try:
            my_album_folder = FileSystem.objects.get(fq_path="/Photos/"+album_name)
            self.assertEqual(my_album_folder.file_name, album_name)
            self.assertEqual(my_album_folder.fq_path_reference, md5("/Photos/"+album_name))
            self.assertEqual(my_album_folder.type, FileSystem.ALBUM_FOLDER_TYPE)
        except:
            self.fail("Album was not created")
        # Delete album
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        album_name = "my_album_1"
        response = client.delete(path="/block/album/cmd/", data={'name': album_name})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        try:
            my_album_folder = FileSystem.objects.get(fq_path="/Photos/"+album_name, status=FileSystem.FILE_DELETED)
            self.assertEqual(my_album_folder.file_name, album_name)
            self.assertEqual(my_album_folder.fq_path_reference, md5("/Photos/"+album_name))
            self.assertEqual(my_album_folder.type, FileSystem.ALBUM_FOLDER_TYPE)
        except:
            self.fail("Album was not deleted")


    def test_list_album(self):
        # Create new album 1
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        album_names = ["my_album_1","my_album_2","my_album_3","my_album_4"]
        for album_name in album_names:
            response = client.post(path="/block/album/cmd/", data={'name': album_name})
            data = json.loads(response.content)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        # List album
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/block/album/show/")
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(data['items']), 4)
        for album , created_album in zip(data['items'], album_names):
            self.assertTrue("/Photos/" in album['fq_path'])
            self.assertTrue(album['file_name'] in album_names)
            self.assertEqual(album['is_dir'], True)
