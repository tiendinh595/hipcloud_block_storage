#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from block.tasks.sync_data import sync_dropbox_logic , sync_folders, sync_files
from block.models.swift import FileSystem,SwiftContainer,SwiftAccount
from block.services.swift.files import SwiftStorage
from block.services.common import hash_path , get_file_kind
from block.services.file import FileSystemUtils
from block.views.uploader import *
from django.utils import timezone
import dropbox
import json
from  rest_framework.response import Response
import math
import os
import logging
import mock
import platform
from django.test import Client

logger = logging.getLogger(__name__)

class UploadFileTestCase(TestCase):
    fixtures = ['block_filesystem.json']

    def setUp(self):
        self.user_id = "tester_id"
        self.access_token = "5075284997574d7f84dd8334a7c1d284"


    # def test_upload_single_image_file(self):
    #     file_to_upload = "/Users/thaing/Downloads/TestUpload/wrong.JPG"
    #     c = Client()
    #     file_size = os.path.getsize(file_to_upload)
    #     total_chunks = int(math.ceil(float(file_size) / 8388608))
    #     response = c.get('/chunked_upload/', {'name': 'wrong.JPG', 'dest': '/',
    #                                           'reported_total_size': int(file_size), 'chunks': total_chunks },
    #                      HTTP_AUTHORIZATION=self.access_token)
    #     upload_session = json.loads(response.content)
    #     self.assertEqual(response.status_code, 200)
    #     with open(file_to_upload) as fp:
    #         uploaded_response = c.post('/chunked_upload/',
    #                                    {'name': upload_session['file_name'],
    #                                     'dest': upload_session['destination'],
    #                                     'reported_total_size': file_size,
    #                                     'upload_id': upload_session['id'],
    #                                     'offset': 0,
    #                                     'chunk': upload_session['current_chunk'],
    #                                     'chunks': upload_session['total_chunks'],
    #                                     'file': fp},
    #                      HTTP_AUTHORIZATION=self.access_token)
    #         self.assertEqual(uploaded_response.status_code, 200)
    #
    # def test_upload_single_mp4_file(self):
    #     file_to_upload = "/Users/thaing/Downloads/TestUpload/sample_mp4.mp4"
    #     c = Client()
    #     file_size = os.path.getsize(file_to_upload)
    #     total_chunks = int(math.ceil(float(file_size) / 8388608))
    #     response = c.get('/chunked_upload/', {'name': 'sample_mp4.mp4', 'dest': '/',
    #                                           'reported_total_size': int(file_size), 'chunks': total_chunks },
    #                      HTTP_AUTHORIZATION=self.access_token)
    #     upload_session = json.loads(response.content)
    #     self.assertEqual(response.status_code, 200)
    #     with open(file_to_upload) as fp:
    #         uploaded_response = c.post('/chunked_upload/',
    #                                    {'name': upload_session['file_name'],
    #                                     'dest': upload_session['destination'],
    #                                     'reported_total_size': file_size,
    #                                     'upload_id': upload_session['id'],
    #                                     'offset': 0,
    #                                     'chunk': upload_session['current_chunk'],
    #                                     'chunks': upload_session['total_chunks'],
    #                                     'file': fp},
    #                      HTTP_AUTHORIZATION=self.access_token)
    #         self.assertEqual(uploaded_response.status_code, 200)
    #     try:
    #         data_files = FileSystem.objects.get(file_name="sample_mp4.mp4", status=FileSystem.FILE_ACTIVE)
    #     except:
    #         self.fail("File not found")
    #
    # def test_upload_multiple_chunk_mp4_file(self):
    #     file_to_upload = "/Users/thaing/Downloads/TestUpload/Multipart/GOPR9715.MP4"
    #     c = Client()
    #     file_size = os.path.getsize(file_to_upload)
    #     total_chunks = int(math.ceil(float(file_size) / 8388608))
    #     response = c.get('/chunked_upload/', {'name': 'GOPR9715.MP4', 'dest': '/',
    #                                           'reported_total_size': int(file_size), 'chunks': total_chunks },
    #                      HTTP_AUTHORIZATION=self.access_token)
    #     upload_session = json.loads(response.content)
    #     self.assertEqual(response.status_code, 200)
    #     with open(file_to_upload) as fp:
    #         uploaded_response = c.post('/chunked_upload/',
    #                                    {'name': upload_session['file_name'],
    #                                     'dest': upload_session['destination'],
    #                                     'reported_total_size': file_size,
    #                                     'upload_id': upload_session['id'],
    #                                     'offset': 0,
    #                                     'chunk': upload_session['current_chunk'],
    #                                     'chunks': upload_session['total_chunks'],
    #                                     'file': fp},
    #                      HTTP_AUTHORIZATION=self.access_token)
    #         self.assertEqual(uploaded_response.status_code, 200)
    #     try:
    #         data_files = FileSystem.objects.get(file_name="GOPR9715.MP4",status=FileSystem.FILE_ACTIVE)
    #     except:
    #         self.fail("File not found")

    # def test_sync_folders(self):
    #     dropbox_path = "/FolderToSyncs/SubFolderToSync"
    #     container_id = "test_account_1_container"
    #     account_id = "test_account_1"
    #
    #     container = SwiftContainer.objects.get(pk=container_id)
    #     dropbox_path_root = "/Dropbox"
    #     dropbox_folder = FileSystem(
    #         parent=None,
    #         container=container,
    #         type=FileSystem.FOLDER_TYPE,
    #         kind='folder',
    #         icon='folder_1',
    #         fq_path_reference=hash_path(dropbox_path_root),
    #         fq_path=dropbox_path_root,
    #         file_name="Dropbox",
    #         is_dir=True,
    #         bytes=0,
    #         user_last_modified=timezone.now(),
    #         status=FileSystem.FILE_ACTIVE
    #     )
    #     dropbox_folder.save()
    #     dbox = dropbox.Dropbox("0ntqt8UyQrAAAAAAAAAAW9N8NAmRas8cnJjSxmcuDDrlFJ2hNr8ALEz5166qppLF")
    #
    #     account = SwiftAccount.objects.get(id=account_id)
    #     storage = SwiftStorage(account, container)
    #
    #     sync_folders(dropbox_folder, container, dbox, dropbox_path)
    #     sync_files(dropbox_folder, container, dbox, dropbox_path, storage)
    #
    #     for i in FileSystem.objects.filter(container="test_account_1_container"):
    #         parent = None
    #         if i.parent:
    #             parent = i.parent.fq_path
    #         print(parent ," | ", i.fq_path ," | ", i.fq_path_reference  ," | ", i.status, " | ", i.kind, " | ", i.file_name, " | ", i.kind)


    def test_valid_filename(self):
        current_system = platform.system()
        # If not windows , run check filename rule by linux rule
        if current_system != "Windows":
            self.assertEqual(FileSystemUtils.valid_file_name("This is bullshit"),True)
            self.assertEqual(FileSystemUtils.valid_file_name("This_is_bullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("Thisisbullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.Bullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.$bullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.@bullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.&bullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.$bullsh^it"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.~bullshit"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("ThisIsBullshit$"), True)
            self.assertEqual(FileSystemUtils.valid_file_name("This.is.`bullshit"), True)
        else:
            pass


    def test_validate_upload_chunk_param(self):
        file_size = 16777216
        file_upload = mock.Mock(size = 8388608)
        data = {
            'reported_total_size': file_size,
            'upload_id' : 'something_hero',
            'name': 'file_name',
            'dest': '/',
            'offset': 0,
            'chunk': 0,
            'chunks': 2
        }
        try:
            validate_upload_chunk_param( data, file_upload )
        except Exception as ex:
            self.fail(ex)

    def test_check_quota_upload_first_upload(self):
        container = mock.Mock()
        container.quota_count = 0
        container.max_quota = 16777216
        try:
            self.assertEqual(check_quota_upload(container, extra_bytes=10), True)
        except Exception as ex:
            self.fail(ex)

    def test_check_quota_upload_over_size_upload(self):
        container = mock.Mock()
        # Current quota is used
        container.quota_count = 16777216
        # Max quota
        container.max_quota = 16777216
        try:
            check_quota_upload(container, extra_bytes=10)
            # Fail if check quota pass , when the container is full
            self.fail("Over capacity but still valid")
        except Exception as ex:
            pass

    def test_check_quota_upload_exactly_full_upload(self):
        container = mock.Mock()
        # Current quota is used
        container.quota_count = 16777205
        # Max quota
        container.max_quota = 16777216
        try:
            check_quota_upload(container, extra_bytes=10)
        except Exception as ex:
            self.fail(ex)

    def test_get_increasing_file_1(self):
        filename = "file_test_1.txt"
        index = 1
        self.assertEqual(get_increasing_file(filename, index), "file_test_1(1).txt")

    def test_get_increasing_file_10(self):
        filename = "file_test_1.txt"
        index = 10
        self.assertEqual(get_increasing_file(filename, index), "file_test_1(10).txt")


    def test_get_increasing_file_999(self):
        filename = "file_test_1.txt"
        index = 999
        self.assertEqual(get_increasing_file(filename, index), "file_test_1(999).txt")

    def test_get_increasing_file_within_another(self):
        filename = "file_test_1(1).txt"
        index = 2
        self.assertEqual(get_increasing_file(filename, index), "file_test_1(1)(2).txt")

    def test_upload_single_chunk_image_jpg_file(self):
        pass

    def test_upload_single_chunk_image_jpeg_file(self):
        pass

    def test_upload_single_chunk_image_png_file(self):
        pass

    def test_upload_single_chunk_image_gif_file(self):
        pass

    def test_download_uploaded_single_chunk_file(self):
        pass

    def test_download_uploaded_multiple_chunk_file(self):
        pass