from block.services.file import FileSystemPath
from block.models.swift import SwiftAccount, SwiftProject, SwiftContainer, SwiftDomain
from block.models.authenticate import AccessToken
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from block.models.swift import FileSystem
from django.test import TestCase
from rest_framework import status
from django.test import Client
from jose import jwt
from unittest import skip
import json



class FileSystemPathTestCase(TestCase):
    fixtures = ['block_filesystem.json']

    def setUp(self):
        self.user_id = "tester_id"
        self.access_token = "5075284997574d7f84dd8334a7c1d284"

    def test_access_token_exists(self):
        try:
            account = AccessToken.objects.get(id=self.access_token)
        except:
            self.fail("Web token not found")

    def test_fixture_account_exists(self):
        try:
            account = SwiftAccount.objects.get(id='test_account_1')
        except:
            self.fail("Account not found")

    def test_fixture_project_exists(self):
        try:
            account = SwiftProject.objects.get(id='demo')
        except:
            self.fail("Project not found")

    def test_fixture_domain_exists(self):
        try:
            account = SwiftDomain.objects.get(id='default')
        except:
            self.fail("Domain not found")

    def test_fixture_container_exists(self):
        try:
            account = SwiftContainer.objects.get(id='test_account_1_container')
        except:
            self.fail("Container not found")

    def test_fixture_filer_exists(self):
        try:
            account = FileSystem.objects.get(id='1')
        except:
            self.fail("File not found")

    def test_fixture_folder_exists(self):
        try:
            account = FileSystem.objects.get(id='2')
        except:
            self.fail("Folder not found")

    # def test_sync_token(self):
    #     payload = jwt.encode({"iss": "swagger_default", "iat": 1500819370, "exp": 1500819380, "mth": "fb", "tok": "token_test", "sub": "subject_id", "context": {"profile": {"id": "batman", "username": "bat_wayne", } }, "admin": False },
    #                          'Y2auJS_2X,Btg6o7J+CVS5c7lHOr;a', algorithm='HS256')
    #     client = APIClient()
    #     client.credentials(HTTP_AUTHORIZATION=self.access_token)
    #     response = client.post(path="/private/sync_token/", data={"payload": payload})
    #     data = json.loads(response.content)
    #     list_folders = FileSystem.objects.all()
    #     for i in list_folders:
    #         print i.fq_path, i.status
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

    @skip("Don't want to test")
    def test_create_file(self, parent_folder, filename):
        file_path = FileSystemPath(parent_folder)
        file_path.init_user_container(self.user_id).sync_path()
        user_file = file_path.create_file(filename, 5000)
        try:
            file_created = FileSystem.objects.get(file_name=filename)
        except Exception as ex:
            self.fail("File was not created" + str(ex))
        self.assertEqual(file_created.id, user_file.id)
        self.assertEqual(file_created.file_name, user_file.file_name)
        self.assertEqual(user_file,file_created)

    @skip("Don't want to test")
    def test_create_folder(self, parent_folder, filename):
        file_path = FileSystemPath(parent_folder)
        file_path.init_user_container(self.user_id).sync_path()
        user_folder = file_path.create_sub_folder(filename)
        try:
            file_created = FileSystem.objects.get(file_name=filename)
        except Exception as ex:
            self.fail("Folder was not created")
        self.assertEqual(file_created.id, user_folder.id)
        self.assertEqual(file_created.file_name, user_folder.file_name)
        self.assertEqual(user_folder,file_created)

    @skip("Don't want to test")
    def test_rename(self, foldername, newname):
        file_path = FileSystemPath(foldername)
        file_path.init_user_container(self.user_id).sync_path()
        user_file = file_path.rename(newname)
        try:
            file_created = FileSystem.objects.get(file_name=newname)
            self.assertEqual(file_created.id, user_file.id)
            self.assertEqual(file_created.file_name, user_file.file_name)
        except Exception as ex:
            self.fail("Folder was not created")

    @skip("Don't want to test")
    def test_delete_file(self, filefullpath):
        file_path = FileSystemPath(filefullpath)
        file_path.init_user_container(self.user_id).sync_path()
        file_path.delete()
        subfolders = []
        try:
            file_created = FileSystem.objects.get(file_name = filefullpath)
            subfolders = FileSystem.objects.filter(fq_path__startswith = file_path.fq_path)
        except Exception as ex:
            self.fail(ex.message)

        self.assertEqual(FileSystem.FILE_DELETED, file_path.path_data.status)
        self.assertEqual(file_created.status, file_path.path_data.status)
        # Check status delete for all sub file / folder
        for folder in subfolders:
            self.assertEqual(FileSystem.FILE_DELETED, folder.status)

    @skip("Don't want to test")
    def test_move_success(self, list_files = [], to_folder = ""):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/cmd/move/", data={ "files" : json.dumps(list_files), "to_path" : to_folder})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @skip("Don't want to test")
    def test_move_fail(self, list_files = [], to_folder = ""):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/cmd/move/", data={ "files" : json.dumps(list_files), "to_path" : to_folder})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @skip("Don't want to test")
    def test_delete_success(self, list_files = []):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/cmd/delete/", data={ "files" : json.dumps(list_files)})
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_folder_root(self):
        file_path = FileSystemPath("/")
        file_path.init_user_container(self.user_id)
        user_files = file_path.list()
        self.assertGreater(len(user_files),0)

    def test_rename_folder_root(self):
        self.test_rename("/testfolder","testfolder_renamed")

    def test_rename_folder_duplicate(self):
        file_path = FileSystemPath("/testfolder")
        file_path.init_user_container(self.user_id).sync_path()
        try:
            user_file = file_path.rename("testfolder1")
        except Exception as ex:
            pass
            # self.fail(str(ex))

    def test_rename_folder_sub(self):
        file_path = FileSystemPath("/testfolder1/testfolder1.1")
        file_path.init_user_container(self.user_id).sync_path()
        user_file = file_path.rename("testfolder1.1_renamed")
        try:
            file_created = FileSystem.objects.get(file_name="testfolder1.1_renamed")
        except Exception as ex:
            self.fail("Folder was not created")
        self.assertEqual(file_created.id, user_file.id)
        self.assertEqual(file_created.file_name, user_file.file_name)

    def test_delete_folder_root(self):
        self.test_delete_success(["/testfolder1"])
        try:
            file_created = FileSystem.objects.get(file_name="testfolder1")
            self.assertEqual(FileSystem.FILE_DELETED, file_created.status)
            self.assertEqual(file_created.status, file_created.status)
            # check subdfolder
            subfolders = FileSystem.objects.filter(fq_path__startswith=file_created.fq_path)
            for fol in subfolders:
                self.assertEqual(FileSystem.FILE_DELETED, fol.status)
        except Exception as ex:
            self.fail(ex.message)

    def test_delete_folder_sub(self):
        self.test_delete_success(["/testfolder1/testfolder1.1"])
        try:
            file_created = FileSystem.objects.get(file_name="testfolder1.1")
            self.assertEqual(file_created.status, file_created.status)
        except Exception as ex:
            self.fail("Folder is not deleted")

    def test_delete_file_root(self):
        self.test_delete_success(["/testfile.txt"])
        try:
            file_created = FileSystem.objects.get(file_name="testfile.txt")
        except Exception as ex:
            self.fail("[test_delete_file_root] Exception")
        self.assertEqual(FileSystem.FILE_DELETED, file_created.status)

    def test_new_file_root(self):
        self.test_create_file("/", "file_test_1")

    def test_new_file_sub_folder(self):
        self.test_create_file("/testfolder", "file_test_2")

    def test_new_sub_folder_root(self):
        self.test_create_folder("/", "folder_test_1")

    def test_new_folder_sub_folder(self):
        self.test_create_folder("/testfolder", "folder_test_sub_1")

    def test_check_read_permission(self):
        # Check root
        file_path = FileSystemPath("/")
        file_path.init_user_container(self.user_id).sync_path()
        self.assertEqual(file_path.is_creatable(),True)
        # Check sub
        file_path = FileSystemPath("/testfolder")
        file_path.init_user_container(self.user_id).sync_path()
        self.assertEqual(file_path.is_creatable(), True)

    def test_browse_api(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/browse/", data={'file_path': '/'})
        data = json.loads(response.content)
        self.assertEqual(data['total_files'], 3)
        self.assertEqual(len(data['file_info']), data['total_files'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_browse_api_paging(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/browse/", data={'file_path': '/','page':1,'limit':1})
        data = json.loads(response.content)
        self.assertEqual(len(data['file_info']), 1)
        self.assertNotEqual(len(data['file_info']), data['total_files'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_browse_api_fail(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/browse/", data={'file_path': '/something_not_found'})
        data = json.loads(response.content)
        self.assertEqual(data['error'], 400)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_api_fail(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=self.access_token)
        response = client.post(path="/cmd/delete/", data={'files': '["/something_not_found"]'})
        data = json.loads(response.content)
        self.assertEqual(data['error'], 400)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_move_api_single_file_success(self):
        self.test_move_success(["/testfile.txt"],"/testfolder1/testfolder1.1")
        try:
            file_created = FileSystem.objects.get(file_name="testfile.txt")
        except Exception as ex:
            self.fail("File was not created "+ str(ex))
        self.assertEqual(file_created.fq_path, "/testfolder1/testfolder1.1/testfile.txt")

    def test_move_api_single_folder_success(self):
        self.test_move_success(["/testfolder1/testfolder1.2"], "/testfolder1/testfolder1.1")
        try:
            file_created = FileSystem.objects.get(file_name="testfolder1.2")
            self.assertEqual(file_created.fq_path, "/testfolder1/testfolder1.1/testfolder1.2")
        except Exception as ex:
            self.fail("File was not created "+ str(ex))

    def test_move_api_multiple_folder_success(self):
        self.test_move_success(["/testfolder1/testfolder1.2","/testfolder1/testfolder1.1"], "/testfolder")
        try:
            file_created = FileSystem.objects.get(file_name="testfolder1.1")
            self.assertEqual(file_created.fq_path, "/testfolder/testfolder1.1")
            self.assertEqual(file_created.fq_path, "/testfolder/testfolder1.1")
            file_created = FileSystem.objects.get(file_name="testfolder1.2")
            self.assertEqual(file_created.fq_path, "/testfolder/testfolder1.2")
            self.assertEqual(file_created.fq_path, "/testfolder/testfolder1.2")
        except Exception as ex:
            self.fail("File was not created " + str(ex))

    def test_move_api_folder_with_multiple_subfolder_to_another_same_root_success(self):
        self.test_move_success(["/testfolder1"], "/testfolder")
        try:
            root_parent = FileSystem.objects.get(file_name="testfolder")
            file_created1 = FileSystem.objects.get(file_name="testfolder1")
            file_created2= FileSystem.objects.get(file_name="testfolder1.1")
            file_created3 = FileSystem.objects.get(file_name="testfolder1.2")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        self.assertEqual(file_created1.fq_path, "/testfolder/testfolder1")
        self.assertEqual(file_created1.parent, root_parent)
        self.assertEqual(file_created2.fq_path, "/testfolder/testfolder1/testfolder1.1")
        self.assertEqual(file_created2.parent, file_created1)
        self.assertEqual(file_created3.fq_path, "/testfolder/testfolder1/testfolder1.2")
        self.assertEqual(file_created3.parent, file_created1)


    def test_move_api_folder_subfolder_to_subroot_success(self):
        self.test_move_success(["/testfolder1/testfolder1.1"], "/testfolder")
        try:
            root_parent = FileSystem.objects.get(file_name="testfolder")
            file_created1 = FileSystem.objects.get(file_name="testfolder1.1")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        self.assertEqual(file_created1.fq_path, "/testfolder/testfolder1.1")
        self.assertEqual(file_created1.parent, root_parent)


    def test_move_api_folder_subfolder_to_root_success(self):
        self.test_move_success(["/testfolder1/testfolder1.1"], "/")
        try:
            file_created1 = FileSystem.objects.get(file_name="testfolder1.1")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        self.assertEqual(file_created1.fq_path, "/testfolder1.1")


    def test_move_api_multiple_sub_to_root_success(self):
        self.test_move_success(["/testfolder1/testfolder1.1","/testfolder1/testfolder1.2"], "/")
        try:
            file_created1 = FileSystem.objects.get(file_name="testfolder1.1")
            file_created2 = FileSystem.objects.get(file_name="testfolder1.2")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        self.assertEqual(file_created1.fq_path, "/testfolder1.1")
        self.assertEqual(file_created2.fq_path, "/testfolder1.2")

    def test_new_move_delete_file_root(self):
        # Create
        self.test_create_file("/", "file_test_1")
        try:
            file_created1 = FileSystem.objects.get(file_name="file_test_1")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        # Move
        self.test_move_success(["/file_test_1"], "/testfolder")
        try:
            file_created1 = FileSystem.objects.get(file_name="file_test_1")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        self.assertEqual(file_created1.fq_path, "/testfolder/file_test_1")
        # Delete
        self.test_delete_success(["/testfolder"])
        try:
            file_created1 = FileSystem.objects.get(file_name="file_test_1")
        except Exception as ex:
            self.fail("File was not created " + str(ex))
        self.assertEqual(file_created1.fq_path, "/testfolder/file_test_1")
        self.assertEqual(FileSystem.FILE_DELETED, file_created1.status)
        subfolders = FileSystem.objects.filter(fq_path__startswith=file_created1.fq_path)
        for folder in subfolders:
            self.assertEqual(FileSystem.FILE_DELETED, folder.status)

    def test_new_move_delete_sub(self):

        pass

    def test_new_move_delete_multiple_root(self):
        pass

    def test_new_move_delete_multiple_sub(self):
        pass

    def test_new_delete_move_root(self):
        pass

    def test_new_delete_move_sub(self):
        pass

    def test_new_delete_move_multiple_root(self):
        pass

    def test_new_move_delete_multiple_sub(self):
        pass