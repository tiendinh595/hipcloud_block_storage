import time
from django.core.management.base import BaseCommand, CommandError
from block.models import FileSystem

class Command(BaseCommand):
    help = 'Get all active line in mysql and index to elasticsearch'

    # def add_arguments(self, parser):
    #     parser.add_argument('all', nargs='+', type=int)

    def handle(self, *args, **options):
        total = FileSystem.objects.filter(status__in=[FileSystem.FILE_ACTIVE,FileSystem.FILE_DELETED]).exclude(status=FileSystem.FILE_CLEARED).count()
        polls = FileSystem.objects.select_related('container').filter(status__in=[FileSystem.FILE_ACTIVE,FileSystem.FILE_DELETED])
        count = 0
        for poll in polls:
            count += 1
            if count % 1000 == 0:
                self.stdout.write('Indexed: %s / %s' % (str(count), str(total)))
                time.sleep(1)
            try:
                poll.index()
            except Exception as ex:
                self.stdout.write(ex)

        self.stdout.write('Successfully sync elasticsearch total %s records' % total)