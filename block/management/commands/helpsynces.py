from django.core.management.base import BaseCommand, CommandError
from block.models import FileSystem

class Command(BaseCommand):
    help = 'Get all active line in mysql and index to elasticsearch'

    # def add_arguments(self, parser):
    #     parser.add_argument('all', nargs='+', type=int)

    def handle(self, *args, **options):
        total = FileSystem.objects.filter(status__in=[FileSystem.FILE_ACTIVE,FileSystem.FILE_DELETED]).count()
        total_delete = FileSystem.objects.filter(status=FileSystem.FILE_CLEARED).count()
        total_sync = FileSystem.objects.filter(status__in=[FileSystem.FILE_ACTIVE,FileSystem.FILE_DELETED]).count()
        self.stdout.write('Total sync  %s records' % total)
        self.stdout.write('Total has to delete %s records' % total_delete)
        self.stdout.write('Total has to sync %s records' % total_sync)