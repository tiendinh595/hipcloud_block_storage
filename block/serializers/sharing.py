from rest_framework import serializers

from block.models import SharingSession
from django.conf import settings

class SharingSessionSerializer(serializers.ModelSerializer):
    sharing_url = serializers.SerializerMethodField(source='get_sharing_url')

    class Meta:
        model = SharingSession
        fields = ('id', 'container_owner', 'file_path', 'sharing_url', 'expire_at')

    def get_sharing_url(self, sharing_folder):
        return settings.SHARE_FOLDER_HOST + '/' + str(sharing_folder.id) + '/' + str(sharing_folder.file_path).replace('/', '%2F')
