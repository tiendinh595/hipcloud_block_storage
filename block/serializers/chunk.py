#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers
from block.models.chunk import UploadSession


class UploadSessionSerializer(serializers.ModelSerializer):

    upload_host = serializers.SerializerMethodField(source = 'get_upload_host', help_text = "Url to upload file")

    class Meta:
        model = UploadSession
        fields = ("id", "user_id", "file_name", "filesystem", "destination", "current_chunk",
                  "total_chunks", "reported_total_size", "expiry_at", "status", "upload_host")

    def get_upload_host(self, upload_session):
        return self.context['host']