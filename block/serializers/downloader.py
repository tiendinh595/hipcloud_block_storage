#!/usr/bin/env python
# -*- coding: utf-8 -*-
from block.services.swift.files import SwiftStorage
from block.models.swift import FileSystem, SwiftContainer
from rest_framework import serializers
from django.conf import settings



class FileSystemShareDownloaderSerializer(serializers.ModelSerializer):

    direct_blockserver_link = serializers.SerializerMethodField(source = "get_direct_blockserver_link", help_text = "Url to download file")
    large_thumbnail_url_tmpl = serializers.SerializerMethodField(source = "get_large_thumbnail_url_tmpl", help_text = "Url to large thumbnail image normal size")
    thumbnail_url_tmpl = serializers.SerializerMethodField(source ="get_thumbnail_url_tmpl", help_text = "Url to thumbnail image normal size")

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id","type", "kind", "extension", "icon",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified","color","is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl","album_large_thumbnail_url_tmpl")

    def get_direct_blockserver_link(self, filesystem):
        if filesystem.is_dir == False:
            real_download_url, source_download_url = SwiftStorage.get_share_url(filesystem.container, filesystem, "GET")
            return real_download_url

    def get_large_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_url(filesystem.container, filesystem.large_thumbnail_url_tmpl, "GET")

    def get_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_url(filesystem.container, filesystem.thumbnail_url_tmpl, "GET")


class FileSystemDownloaderSerializer(serializers.ModelSerializer):

    direct_blockserver_link = serializers.SerializerMethodField(source = "get_direct_blockserver_link", help_text = "Url to download file")
    large_thumbnail_url_tmpl = serializers.SerializerMethodField(source = "get_large_thumbnail_url_tmpl", help_text = "Url to large thumbnail image normal size")
    thumbnail_url_tmpl = serializers.SerializerMethodField(source ="get_thumbnail_url_tmpl", help_text = "Url to thumbnail image normal size")

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id","type", "kind", "extension", "icon",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified","color","is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl","album_large_thumbnail_url_tmpl")

    def get_direct_blockserver_link(self, filesystem):
        if filesystem.is_dir == False:
            real_download_url, source_download_url = SwiftStorage.get_download_url(filesystem.container, filesystem, "GET")
            return real_download_url

    def get_large_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_url(filesystem.container, filesystem.large_thumbnail_url_tmpl, "GET")

    def get_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_url(filesystem.container, filesystem.thumbnail_url_tmpl, "GET")


