#!/usr/bin/env python
# -*- coding: utf-8 -*-
from block.models import SyncSession
from block.services.swift.files import SwiftStorage
from block.models.swift import FileSystem, SwiftContainer, Album, FileAlbum
from block.models.dropbox import DropboxSync
from rest_framework import serializers
from block.serializers.swift import FileSystemSerializer, PhotoFileSystemSerializer, PhotoPreviewFileSystemSerializer

class FileDocumentSerializer(serializers.ModelSerializer):

    direct_blockserver_link = serializers.CharField()
    large_thumbnail_url_tmpl = serializers.CharField()
    thumbnail_url_tmpl = serializers.CharField()

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id","type", "kind", "extension", "icon",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified","color","is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl","album_large_thumbnail_url_tmpl")


class FileSystemDocumentSerializer(serializers.ModelSerializer):
    """
    Serializer for display document. Not using for response data
    """

    total_files = serializers.IntegerField()
    current_page = serializers.IntegerField()
    total_pages = serializers.IntegerField()
    file_info = FileSystemSerializer(many=True, read_only=True)

    class Meta:
        model = FileSystem
        fields = ("total_files", "current_page","total_pages","file_info")



class MetadataSerializer(serializers.ModelSerializer):
    """
    Serializer for display document. Not using for response data
    """

    limit = serializers.IntegerField()
    page = serializers.IntegerField()

    class Meta:
        model = FileSystem
        fields = ("limit","page")

class MetadataTotalSerializer(serializers.ModelSerializer):
    """
    Serializer for display document. Not using for response data
    """
    total = serializers.IntegerField()
    limit = serializers.IntegerField()
    page = serializers.IntegerField()

    class Meta:
        model = FileSystem
        fields = ("total","limit","page")


class PhotoPreviewFileSystemDocumentSerializer(serializers.ModelSerializer):
    """
    Serializer for display document. Not using for response data
    """

    header = PhotoPreviewFileSystemSerializer(many=True, read_only=True)
    items = PhotoPreviewFileSystemSerializer(many=True, read_only=True)
    metadata = MetadataSerializer()

    class Meta:
        model = FileSystem
        fields = ("header","items","metadata")

class FileSystemSearchDocumentSerializer(serializers.ModelSerializer):
    """
    Serializer for display document. Not using for response data
    """

    items = FileSystemSerializer(many=True, read_only=True)
    metadata = MetadataTotalSerializer()

    class Meta:
        model = FileSystem
        fields = ("items","metadata")


class AlbumFileSystemDocumentSerializer(serializers.ModelSerializer):
    """
    Serializer for display document. Not using for response data
    """

    items = PhotoPreviewFileSystemSerializer(many=True, read_only=True)
    metadata = MetadataSerializer()

    class Meta:
        model = FileSystem
        fields = ("items","metadata")


class QuotaContainerDocumentSerializer(serializers.ModelSerializer):

    dropbox_synced = serializers.BooleanField()
    ggdrive_synced = serializers.BooleanField()
    ggphotos_synced = serializers.BooleanField()

    class Meta:
        model = SwiftContainer
        fields = ("max_quota", "quota_count","dropbox_synced","ggdrive_synced","ggphotos_synced")

class AlbumSerializer(serializers.ModelSerializer):
    cover_thumbnail_url_tmpl = serializers.SerializerMethodField(source='cover_thumbnail_url_tmpl',
                                                                 help_text="Url to cover thumbnail image")
    class Meta:
        model = Album
        fields = ("id", "type", "name", "file_count", "permissions", "status", "cover_thumbnail_url_tmpl")

    def get_cover_thumbnail_url_tmpl(self, album):
        if album.container:
            return SwiftStorage.get_temp_thumb_url(album.container, album.cover_thumbnail_url_tmpl, "GET")

class SyncStatusDocumentSerializer(serializers.ModelSerializer):
    sync_session_id = serializers.StringRelatedField()
    email = serializers.EmailField()
    class Meta:
        model = SyncSession
        fields = ("status", "email", "sync_session_id")