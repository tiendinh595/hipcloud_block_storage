from rest_framework import serializers

from block.models.sync import SyncSession

class SyncStatusSerializer(serializers.ModelSerializer):

    status = serializers.SerializerMethodField(help_text='Status sync')
    email = serializers.SerializerMethodField(help_text='Sync email account')
    sync_session_id = serializers.SerializerMethodField(help_text='ID session sync')

    class Meta:
        model = SyncSession
        fields = ('status', 'email', 'sync_session_id')
    def get_sync_session_id(self, sync_session):
        return sync_session.id
    def get_status(self, sync_session):
        return sync_session.status
    def get_email(self, sync_session):
        return sync_session.email_sync

class SyncSessionSerializer(serializers.ModelSerializer):

    total_size = serializers.SerializerMethodField(help_text='Total size files sync')
    email = serializers.SerializerMethodField(help_text='Sync email account')
    sync_value = serializers.SerializerMethodField(help_text='Json sync info {total_files, total_size, current_file, current_file_path, sync_success_size, sync_success_files}')

    class Meta:
        model = SyncSession
        fields = ('status', 'email', 'sync_type', 'total_size', 'sync_value')
    def get_total_size(self, sync_session):
        return sync_session.total_size
    def get_sync_value(self, sync_session):
        return sync_session.sync_value
    def get_email(self, sync_session):
        return sync_session.email_sync