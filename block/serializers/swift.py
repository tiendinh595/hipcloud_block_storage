#!/usr/bin/env python
# -*- coding: utf-8 -*-
from block.models import SyncSession
from block.services.swift.files import SwiftStorage
from block.models.swift import FileSystem, SwiftContainer
from block.models.dropbox import DropboxSync
from rest_framework import serializers


class FileSystemSerializer(serializers.ModelSerializer):
    direct_blockserver_link = serializers.SerializerMethodField(source='get_direct_blockserver_link',
                                                                help_text="Url to download file")
    large_thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_large_thumbnail_url_tmpl',
                                                                 help_text="Url to large thumbnail image normal size")
    thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_thumbnail_url_tmpl',
                                                           help_text="Url to thumbnail image normal size")

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id", "type", "kind", "extension", "icon",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified", "color", "is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl", "album_large_thumbnail_url_tmpl")

    def get_direct_blockserver_link(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_url(filesystem.container, filesystem.id, "GET")

    def get_large_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.large_thumbnail_url_tmpl, "GET")

    def get_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.thumbnail_url_tmpl, "GET")


class PhotoPreviewFileSystemSerializer(serializers.ModelSerializer):
    large_thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_large_thumbnail_url_tmpl',
                                                                 help_text="Url to large thumbnail image normal size")
    thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_thumbnail_url_tmpl',
                                                           help_text="Url to thumbnail image normal size")

    class Meta:
        model = FileSystem
        fields = ("id", "large_thumbnail_url_tmpl", "thumbnail_url_tmpl")

    def get_large_thumbnail_url_tmpl(self, album):
        if album.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(album.container, album.large_thumbnail_url_tmpl, "GET")

    def get_thumbnail_url_tmpl(self, album):
        if album.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(album.container, album.thumbnail_url_tmpl, "GET")


class PhotoFileSystemSerializer(serializers.ModelSerializer):
    album_large_thumbnail_url_tmpl = serializers.SerializerMethodField(source='album_large_thumbnail_url_tmpl',
                                                                       help_text="Url to large album thumbnail image normal size")
    large_thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_large_thumbnail_url_tmpl',
                                                                 help_text="Url to large thumbnail image normal size")
    thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_thumbnail_url_tmpl',
                                                           help_text="Url to thumbnail image normal size")

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id", "type", "kind", "extension", "icon",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified", "color", "is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl", "album_large_thumbnail_url_tmpl")

    def get_large_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.large_thumbnail_url_tmpl, "GET")

    def get_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.thumbnail_url_tmpl, "GET")

    def get_album_large_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.album_large_thumbnail_url_tmpl,
                                                   "GET")


class AlbumFileSystemSerializer(serializers.ModelSerializer):
    preview_images = serializers.SerializerMethodField(help_text="List of preview images")

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id", "type", "kind", "extension", "icon", "preview_images",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified", "color", "is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl", "album_large_thumbnail_url_tmpl", "file_count")

    def get_preview_images(self, album):
        album.file_count = FileSystem.objects.filter(container=album.container,
                                                     parent=album,
                                                     kind=FileSystem.IMAGE_KIND,
                                                     status=FileSystem.FILE_ACTIVE).count()

        preview_images = FileSystem.objects.filter(container=album.container,
                                                   status=FileSystem.FILE_ACTIVE,
                                                   kind=FileSystem.IMAGE_KIND,
                                                   parent=album).order_by("-user_last_modified")[:9]
        if len(preview_images) > 0:
            temp_album = preview_images[0]
            album.album_large_thumbnail_url_tmpl = SwiftStorage.get_temp_thumb_url(temp_album.container,
                                                                                   temp_album.album_large_thumbnail_url_tmpl,
                                                                                   "GET")
            album.large_thumbnail_url_tmpl = SwiftStorage.get_temp_thumb_url(temp_album.container,
                                                                             temp_album.large_thumbnail_url_tmpl, "GET")
            album.thumbnail_url_tmpl = SwiftStorage.get_temp_thumb_url(temp_album.container,
                                                                       temp_album.large_thumbnail_url_tmpl, "GET")
        return PhotoPreviewFileSystemSerializer(preview_images, many=True).data


class ContainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = SwiftContainer


class QuotaContainerSerializer(serializers.ModelSerializer):
    dropbox_synced = serializers.SerializerMethodField(source='get_large_thumbnail_url_tmpl', help_text="true/false")
    ggdrive_synced = serializers.SerializerMethodField(source='get_ggdrive_synced', help_text="true/false")
    ggphotos_synced = serializers.SerializerMethodField(source='get_ggphotos_synced', help_text="true/false")

    class Meta:
        model = SwiftContainer
        fields = ("max_quota", "quota_count","dropbox_synced","ggdrive_synced","ggphotos_synced")

    def get_dropbox_synced(self, container):
        is_sync = True
        try:
            SyncSession.objects.get(container = container, status = SyncSession.LINKED, sync_type=SyncSession.SYNC_DROPBOX)
        except:
            is_sync = False
        return is_sync

    def get_ggdrive_synced(self, container):
        is_sync = True
        try:
            SyncSession.objects.get(container = container, status = SyncSession.LINKED, sync_type=SyncSession.SYNC_GOOGLE_DRIVE)
        except:
            is_sync = False
        return is_sync

    def get_ggphotos_synced(self, container):
        is_sync = True
        try:
            SyncSession.objects.get(container = container, status = SyncSession.LINKED, sync_type=SyncSession.SYNC_GOOGLE_PHOTOS)
        except:
            is_sync = False
        return is_sync

class FileShareSerializer(serializers.ModelSerializer):
    direct_blockserver_link = serializers.SerializerMethodField(source='get_direct_blockserver_link',
                                                                help_text="Url to download file")
    large_thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_large_thumbnail_url_tmpl',
                                                                 help_text="Url to large thumbnail image normal size")
    thumbnail_url_tmpl = serializers.SerializerMethodField(source='get_thumbnail_url_tmpl',
                                                           help_text="Url to thumbnail image normal size")

    class Meta:
        model = FileSystem
        fields = ("direct_blockserver_link", "id", "type", "kind", "extension", "icon",
                  "fq_path", "file_name", "is_dir", "bytes", "user_last_modified", "color", "is_starred",
                  "large_thumbnail_url_tmpl", "thumbnail_url_tmpl", "album_large_thumbnail_url_tmpl")

    def get_direct_blockserver_link(self, filesystem):
        if filesystem.is_dir == False:
            download_url, source_download_url = SwiftStorage.get_download_url(filesystem.container, filesystem, "GET")
            return download_url

    def get_large_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.large_thumbnail_url_tmpl, "GET")

    def get_thumbnail_url_tmpl(self, filesystem):
        if filesystem.is_dir == False:
            return SwiftStorage.get_temp_thumb_url(filesystem.container, filesystem.thumbnail_url_tmpl, "GET")
