from rest_framework import authentication
from rest_framework import exceptions
from block.models.authenticate import AccessToken
from jose import jwt


class SecretKeyAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            if token != "smOxe9yKpQ45MCddTSEZ":
                raise exceptions.AuthenticationFailed('Token not found')
            else:
                return ('admin', 'admin')
        except Exception as ex:
            return (None, None)

class TokenAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token)
                except Exception as ex:
                    raise exceptions.AuthenticationFailed('Token not found')
                return (token, token.user_id)
            else:
                raise exceptions.AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)


class TokenAuthenticationGet(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.GET.get('auth')
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token)
                except:
                    raise exceptions.AuthenticationFailed('Token not found')
                return (token, token.user_id)
            else:
                raise exceptions.AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)

class TokenAuthenticationGoogleRedirect(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.GET.get('state')
        token = token.split("/")[1]
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token)
                except:
                    raise exceptions.AuthenticationFailed('Token not found')
                return (token, token.user_id)
            else:
                raise exceptions.AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)
