#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import include, url
import block.views.uploader as uploader
import block.views.metadata as metadata
from block.views import downloader
from django.contrib import admin
import block.views.album as album
import block.views.demo as demo
import block.views.authentication as auth
import block.views.sync_dropbox_v2 as sync_dropbox_v2
import block.views.sync_gg_drive_v2 as sync_gg_drive_v2
import block.views.sync_gg_photos_v2 as sync_gg_photos_v2
import block.views.radosgw as radosgw
import block.views.s3 as s3
import block.views.monitor as monitor
import block.views.backend as backend
from block.views import list_filesystem
from block.views import sharing_folder, sharing_album

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^docs/', include('rest_framework_swagger.urls')),

    url(r'^chunked_upload/', uploader.UploadFileView.as_view()),
    url(r'^browse/', metadata.BrowseFileView.as_view()),
    url(r'^list_items/', list_filesystem.ListFilesystemView.as_view()),
    url(r'^recents/', metadata.RecentFileView.as_view()),
    url(r'^trash/', metadata.DeletedFileView.as_view()),
    url(r'^quota/', metadata.QuotaInfoView.as_view()),

    url(r'^album/info$', metadata.BrowseAlbumView.as_view()),
    url(r'^album/content$', metadata.ItemsAlbumView.as_view()),
    url(r'^album/sharing', sharing_album.ShareAlbumView.as_view()),
    url(r'^album/get_share', sharing_album.GetShareAlbumView.as_view()),
    url(r'^album/cover', metadata.CoverAlbumView.as_view()),

    url(r'^cmd/detail/', metadata.FileDetailView.as_view()),
    url(r'^cmd/new/', metadata.NewFolderCommandView.as_view()),
    url(r'^cmd/rename/', metadata.RenameFolderCommandView.as_view()),
    url(r'^cmd/delete/', metadata.DeleteFolderCommandView.as_view()),
    url(r'^cmd/move/', metadata.MoveFolderCommandView.as_view()),
    url(r'^cmd/v2/move/', metadata.MoveFolderCommandViewV2.as_view()),
    url(r'^cmd/restore/', metadata.RestoreCommandView.as_view()),
    url(r'^cmd/clear/', metadata.DeletePermanentCommandView.as_view()),
    url(r'^cmd/empty_trash/', metadata.EmptyTrashCommandView.as_view()),
    url(r'^cmd/search/', metadata.SearchFileView.as_view()),
    url(r'^cmd/share/', downloader.ShareFileUrlView.as_view()),
    url(r'^cmd/download/', downloader.DownloadFileUrlView.as_view()),
    url(r'^cmd/copy/', metadata.CopyFileCommandView.as_view()),
    url(r'^cmd/v2/copy', metadata.CopyFileCommandViewV2.as_view()),

    # url(r'^sync/dropbox/link', sync_dropbox.DropboxLinkView.as_view()),
    # url(r'^sync/dropbox/unlink', sync_dropbox.DropboxUnlinkView.as_view()),
    # url(r'^sync/dropbox/sync', sync_dropbox.DropboxSyncCommandView.as_view()),

    url(r'^sync/dropbox/v2/check_quota/', sync_dropbox_v2.DropboxCheckQuotaView.as_view()),
    url(r'^sync/dropbox/v2/sync/', sync_dropbox_v2.DropboxSyncCommandView.as_view()),
    url(r'^sync/dropbox/v2/stop_sync/', sync_dropbox_v2.DropboxStopSyncCommandView.as_view()),
    url(r'^sync/dropbox/v2/sync_status/', sync_dropbox_v2.DropboxSyncCommandStatusView.as_view()),

    # url(r'^sync/ggdrive/link', sync_gg_drive.GGDriveLinkView.as_view()),
    # url(r'^sync/ggdrive/unlink', sync_gg_drive.GGDriveUnlinkView.as_view()),
    # url(r'^sync/ggdrive/sync', sync_gg_drive.GGDriveSyncCommandView.as_view()),

    url(r'^sync/v2/auth_link/', sync_gg_drive_v2.GGDriveAuthLinkView.as_view()),
    url(r'^sync/v2/auth_token/', sync_gg_drive_v2.GGDriveGetTokenView.as_view()),

    url(r'^sync/ggdrive/v2/check_quota/', sync_gg_drive_v2.GGDriveCheckQuotaView.as_view()),
    url(r'^sync/ggdrive/v2/sync/', sync_gg_drive_v2.GGDriveSyncCommandView.as_view()),
    url(r'^sync/ggdrive/v2/stop_sync/', sync_gg_drive_v2.GGDriveStopSyncCommandView.as_view()),
    url(r'^sync/ggdrive/v2/sync_status/', sync_gg_drive_v2.GGDriveSyncCommandStatusView.as_view()),

    # url(r'^sync/ggphotos/v2/test_list/', sync_gg_photos_v2.GGPhotosTestListView.as_view()),
    url(r'^sync/ggphotos/v2/check_quota/', sync_gg_photos_v2.GGPhotosCheckQuotaView.as_view()),
    url(r'^sync/ggphotos/v2/sync/', sync_gg_photos_v2.GGPhotosSyncCommandView.as_view()),
    url(r'^sync/ggphotos/v2/stop_sync/', sync_gg_photos_v2.GGPhotosStopSyncCommandView.as_view()),
    url(r'^sync/ggphotos/v2/sync_status/', sync_gg_photos_v2.GGPhotosSyncCommandStatusView.as_view()),

    url(r'^sync/container', backend.ContainerUpdateView.as_view()),

    url(r'^album/show/', album.AlbumView.as_view()),
    url(r'^album/photo/', album.PhotoView.as_view()),
    url(r'^album/cmd/', album.AlbumCommandView.as_view()),

    url(r'^demo/task/', demo.TaskDemoView.as_view()),
    url(r'^demo/schedule/', demo.ScheduleDemoView.as_view()),

    url(r'^private/monitor/private/block/', monitor.ConfigView.as_view()),
    url(r'^private/test/radosgw/test/', radosgw.TestUploadFileView.as_view()),
    url(r'^private/test/s3/test/', s3.TestUploadFileView.as_view()),

    url(r'^private/sync_token/', auth.Sync_Token.as_view()),

    url(r'^cmd/sharing_folder/', sharing_folder.ShareFolderView.as_view()),
    url(r'^get_share_folder/', sharing_folder.GetShareFolderView.as_view()),
]