#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from block.services.common import response
from rest_framework.exceptions import PermissionDenied , ParseError
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.gdrive import GoogleAccount, GoogleSync, GoogleSyncCommand
from block.services.file import FileSystemPath
from block.models.swift import SwiftContainer, SwiftAccount
from block.tasks.sync_data import sync_google_drive
from apiclient import discovery, errors
from oauth2client import client
from celery.result import AsyncResult
import logging
import httplib2
import json



logger = logging.getLogger(__name__)

class GGDriveLinkView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Link google drive from syncing
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: google_drive_access_token
              description: access token oauth2 from google
              required: false
              type: string
              paramType: form

            - name: google_drive_refresh_token
              description: refresh token oauth2 from google
              required: false
              type: string
              paramType: form


        responseMessages:
            - code: 200
              message: '{"status" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        google_token = data.get('google_drive_access_token', None)
        if not google_token:
            raise ParseError("Token not found")

        container = SwiftContainer.objects.get(account__user_id=user_id)
        # Create dropbox account
        try:
            credentials = client.AccessTokenCredentials(google_token, 'my-user-agent/1.0')
            http = httplib2.Http()
            http = credentials.authorize(http)
            service = discovery.build('drive', 'v2', http=http)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Token is not valid")

        return response.success({"status":"success"})

class GGDriveUnlinkView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Unlink google drive from syncing
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"status" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)

        try:
            user_link = GoogleSync.objects.get(user_id = user_id, status = GoogleSync.LINKED)
            user_link.delete()
        except:
            raise ParseError("No link found")

        return response.success({"status":"success"})

class GGDriveSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Sync command
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: dropbox array of folder ids , with total len is 100 "['path_id_1','path_id_2',..]"
              required: true
              type: string
              paramType: form

            - name: file_path
              description: Path to store file /path/to/file
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files_str = data.get('files', None)
        file_path_str = data.get("file_path", None)

        try:
            files = json.loads(files_str)
            if len(files) > 100:
                return response.fail("Array is too large")
            if not file_path_str:
                return response.fail("File path not found")
        except Exception as ex:
            return response.fail("Files is not valid")

        # try:
        #     account = SwiftAccount.objects.get(user_id=user_id)
        #     user_link = GoogleSync.objects.get(user_id = user_id, status = GoogleSync.LINKED)
        #     dbox_cmd = GoogleSyncCommand(
        #         container=user_link.container,
        #         paths_to_sync=files_str,
        #         save_path=file_path_str
        #     )
        #     dbox_cmd.save()
        # except:
        #     raise ParseError("No link found")
        #
        # save_file_location = FileSystemPath(file_path_str)
        # save_file_location.init_user_container(user_id)
        # save_file_location.sync_path()
        #
        # # Spawn sync tasks
        # job = sync_google_drive.apply_async(([user_link.dropbox_token, files, file_path_str, user_link.container.id, account.id]))
        # task = AsyncResult(job.id)
        return response.success({"status": "success"})

