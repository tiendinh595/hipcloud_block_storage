#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from block.services.common import response
from block.authentication import SecretKeyAuthentication
from block.permissions import TokenIsAuthenticated
from django.core.cache import cache
from django.conf import settings
import logging


logger = logging.getLogger(__name__)

class ConfigView(APIView):

    authentication_classes = (SecretKeyAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def get(self, request, *args, **kwargs):
        """
        [BACKEND ONLY] View config
        ---

        parameters:
            - name: Authorization
              description: admin key
              required: true
              type: string
              paramType: header

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        return response.success({"CURRENT_STORAGE":settings.CURRENT_STORAGE})

    def post(self, request, *args, **kwargs):
        """
        [BACKEND ONLY] Update config
        ---
        parameters:
            - name: Authorization
              description: admin key
              required: true
              type: string
              paramType: header

            - name: current_storage
              description: token string
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        data = request.POST
        current_storage = data.get('current_storage', 'swift1')
        if current_storage in settings.ALLOWS_STORAGE:
            settings.CURRENT_STORAGE = settings.ALLOWS_STORAGE[current_storage]
            return response.success({'current_storage': settings.CURRENT_STORAGE})
        else:
            return response.fail("Not valid")