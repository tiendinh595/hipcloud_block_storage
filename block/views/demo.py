#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from block.services.common import response
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from django.utils import timezone
from datetime import timedelta
from celery.result import AsyncResult
import json
import logging
from block.tasks.upload import upload_to_swift

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class TaskDemoView(APIView):
    """
    Demo backend only - front end dont use
    """

    def get(self, request, *args, **kwargs):
        """
        Create task
        ---
        parameters:
            - name: param1
              description: first parameter
              required: false
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        param = int(request.GET['param1'])
        job = upload_to_swift.delay(param)
        task = AsyncResult(job.id)
        return response.success({"result" : task.result, "state": task.state})

class ScheduleDemoView(APIView):
    """
    Demo backend only - front end dont use
    """

    def get(self, request, *args, **kwargs):
        """
        Create task schedule
        ---
        parameters:
            - name: param1
              description: first parameter
              required: false
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        param = request.GET['param1']
        job = fft_random.delay(int(param))
        return response.success({"data": job})
