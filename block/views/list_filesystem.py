#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from block.services.common import response
from rest_framework.exceptions import PermissionDenied , ParseError
from block.services.common.cache import CACHE_DOWNLOAD
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftContainer
from block.models.swift import FileSystem
from block.services.file import FileSystemPath, FileSystemUtils
from block.serializers.swift import FileSystemSerializer, QuotaContainerSerializer
from django.core.paginator import EmptyPage, PageNotAnInteger
from block.services.search.models import FileSystemSearch
from django.core.paginator import Paginator
import json
import logging
from rest_framework.exceptions import APIException

logger = logging.getLogger(__name__)


class ListFilesystemView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        List file/folder by path
        ---

        serializer: block.serializers.api_document.FileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: recursive
              description: 1 / 0 recursive for list all child's child folder and file
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 2 )
              required: false
              type: string
              paramType: form

            - name: file_path
              description: Path to file /path/to/file, default will list all
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_path = data.get('file_path', "/")
        page = data.get('page', '1')
        limit = data.get('limit', '20')
        recursive = data.get('recursive', '0')
        is_xhr = data.get('is_xhr', None)
        try:
            recursive = int(recursive)
        except:
            raise ParseError("Recursive is 1 / 0")

        try:

            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        file_path = FileSystemPath(file_path)
        file_path.init_user_container(user_id).sync_path()
        user_files = file_path.list_recursive( recursive , ["fq_path"])
        paginator = Paginator(user_files, limit)
        browse_data = paginator.page(page)
        try:
            response_data = FileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = FileSystemSerializer([], many=True)

        return response.success({
            "total_files" : paginator.count,
            "total_pages" : paginator.num_pages,
            "current_page" : page,
            "file_info" : response_data.data
        })
