#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from block.services.common import response
from rest_framework.exceptions import PermissionDenied , ParseError
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.dropbox import DropboxAccount , DropboxSync, DropboxSyncCommand
from block.services.file import FileSystemPath
from block.models.swift import SwiftContainer, SwiftAccount
import dropbox as dbx
from block.tasks.sync_data import sync_dropbox
from celery.result import AsyncResult
import logging
import json



logger = logging.getLogger(__name__)

class DropboxLinkView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Link dropbox from syncing
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: dropbox_token
              description: token oauth2 from dropbox
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"status" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        dropbox_token = data.get('dropbox_token', None)
        if not dropbox_token:
            raise ParseError("Token not found")
        dbox = dbx.Dropbox(dropbox_token)
        container = SwiftContainer.objects.get(account__user_id=user_id)
        # Create dropbox account
        try:
            user_account = dbox.users_get_current_account()
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Token is not valid")

        try:
            dropbox_account = DropboxAccount.objects.get(user_id=user_id, account_id=user_account.account_id)
        except DropboxAccount.DoesNotExist:
            dropbox_account = DropboxAccount(
                account_id=user_account.account_id,
                user_id=user_id,
                display_name=user_account.name.display_name,
                email=user_account.email,
                email_verified=user_account.email_verified,
                locale=user_account.locale,
                referral_link=user_account.referral_link,
                country=user_account.country,
                team=user_account.team.id if user_account.team else None,
                team_member_id=user_account.team_member_id,
                disabled=user_account.disabled
            )
            dropbox_account.save()

        try:
            # Create dropbox sync
            dropbox_sync, created = DropboxSync.objects.update_or_create(
                user_id=user_id,
                container=container,
                dropbox_account=dropbox_account,
                dropbox_token=dropbox_token,
                status=DropboxSync.LINKED
            )
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Token not valid")
        return response.success({"status":"success"})

class DropboxUnlinkView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Unlink dropbox from syncing
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"status" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)

        try:
            user_link = DropboxSync.objects.get(user_id = user_id, status = DropboxSync.LINKED)
            user_link.delete()
        except:
            raise ParseError("No link found")

        return response.success({"status":"success"})

class DropboxSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Sync command
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: dropbox array of paths , with total len is 100 "[/path1,/path2]"
              required: true
              type: string
              paramType: form

            - name: file_path
              description: Path to store file /path/to/file
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files_str = data.get('files', None)
        file_path_str = data.get("file_path", None)

        try:
            files = json.loads(files_str)
            if len(files) > 100:
                return response.fail("Array is too large")
            if not file_path_str:
                return response.fail("File path not found")
        except Exception as ex:
            return response.fail("Files is not valid")

        try:
            account = SwiftAccount.objects.get(user_id=user_id)
            user_link = DropboxSync.objects.get(user_id = user_id, status = DropboxSync.LINKED)
            dbox_cmd = DropboxSyncCommand(
                container=user_link.container,
                paths_to_sync=files_str,
                save_path=file_path_str
            )
            dbox_cmd.save()
        except:
            raise ParseError("No link found")

        save_file_location = FileSystemPath(file_path_str)
        save_file_location.init_user_container(user_id)
        save_file_location.sync_path()

        # Spawn sync tasks
        job = sync_dropbox.apply_async(([user_link.dropbox_token, files, file_path_str, user_link.container.id, account.id]))
        task = AsyncResult(job.id)
        return response.success({"status": "success"})

