#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from block.services.common import response, get_file_kind
from rest_framework.exceptions import ParseError
from django.db.models import F
from block.exceptions import OutOfQuota
from block.models.chunk import UploadSession, UploadProgress
from block.services.file import FileSystemUtils
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftAccount, SwiftContainer, FileSystem
from block.services import mediainfo
from block.services.swift.files import SwiftStorage
from block.services.album import AlbumUtils
from block.serializers.chunk import UploadSessionSerializer
# from block.services.thumbnails import generate_thumbnail
# from block.tasks.image import generate_thumb
# from block.tasks.tracking import track
from block.tasks import *
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
from ipware.ip import get_ip
import hashlib
import logging
import json
import math
import re
import os

logger = logging.getLogger(__name__)


def md5(str):
    # settings.HASH_PATH_SALT
    m = hashlib.md5()
    m.update(str)
    return m.hexdigest()


def validate_upload_chunk_param(data, file_upload):
    try:
        data['upload_id']
        data['name']
        data['dest']
        filesize = int(data['reported_total_size'])
        int(data['offset'])
        int(data['chunk'])
        total_chunk = int(data['chunks'])
    except Exception as ex:
        logger.error(ex)
        raise ParseError("Missing parameters")

    expect_total_chunks = int(math.ceil(float(filesize) / settings.UPLOAD_CHUNK_SIZE))

    if int(total_chunk) != expect_total_chunks:
        raise ParseError("Total chunk is not valid with size of the file , expected "
                         + str(expect_total_chunks) + " chunk")

    if settings.FILE_UPLOAD_MAX_MEMORY_SIZE < file_upload.size:
        raise ParseError("File size over-limited")
    return True


def check_quota_upload(container, extra_bytes=0):
    """
    :param container:
    :return: raise ParseError
    """
    if (container.quota_count + extra_bytes) >= container.max_quota:
        raise OutOfQuota("Out of quota")
    return True


def validate_create_session(data_parameters):
    try:
        data_parameters['name']
        data_parameters['dest']
        data_parameters['reported_total_size']
        data_parameters['chunks']
    except:
        raise ParseError("Missing parameter")

    if data_parameters['dest'][0] != "/":
        raise ParseError("Path not valid")


def check_upload_destination(destination_path, container):
    """
    Check dest exists
    :param destination_path: str
    :param container: Container
    :return: FileSystem
    """
    if destination_path == "/":
        return None
    try:
        dest_filesystem = FileSystem.objects.get(fq_path_reference=md5(destination_path), container=container)
        if not dest_filesystem.is_dir:
            raise ParseError("Destination must be folder")
        return dest_filesystem
    except Exception as ex:
        logger.info(ex)
        raise ParseError("Destination not found")


def _init_swift(user_id):
    # Get swift account or create one if not exists
    account = None
    try:
        account = SwiftAccount.objects.get(user_id=user_id)
        # account_swift = SwiftUserManager().get_user(user_id=user_id)
    except Exception as ex:
        raise ParseError("Internal Error")

    # Get container or create one if not exists
    try:
        container = SwiftContainer.objects.get(account=account)
    except Exception as ex:
        raise ParseError("Internal Error")
    return account, container


def get_increasing_file(filename, index):
    extension = filename.split(".")[-1]
    filename_only = filename.replace("." + extension, "")
    return filename_only + "(" + str(index) + ")." + extension


def handle_uploaded_file(filestream, temp_file_path, filename):
    md5 = hashlib.md5()
    user_path = settings.SITE_ROOT + settings.MEDIA_URL + temp_file_path
    full_path = os.path.join(user_path, filename)
    try:
        if not os.path.exists(user_path):
            os.makedirs(user_path)
        with open(full_path, 'wb+') as destination:
            for chunk in filestream.chunks():
                md5.update(chunk)
                destination.write(chunk)
            md5sum = md5.hexdigest()
            return full_path, md5sum, True
    except OSError as exc:  # Guard against race condition
        logger.error(exc)
        return None, None, False
    except Exception as exc:
        logger.error(exc)
        return None, None, False


def get_account_container(user_id):
    try:
        account = SwiftAccount.objects.get(user_id=user_id)
        container = SwiftContainer.objects.get(account=account)
        return account, container
    except Exception as ex:
        logger.error(ex)
        raise ParseError("Internal Error")


class UploadFileView(APIView):
    """
    Uploads large files in multiple chunks. Also, has the ability to resume
    if the upload is interrupted.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def validate_upload_chunk_session(self, upload_session, filesize, reported_total_size, byte_offset, current_chunk,
                                      reported_chunks):
        if (byte_offset + filesize) == upload_session.reported_total_size:
            pass

    def extract_file_count(self, filename):
        """
        :param filename: str
        :return: int / None
        """
        number_index_str = re.search('\(([0-9]*)\)$', filename, re.IGNORECASE)
        if number_index_str:
            try:
                count_number = number_index_str.group(1)
                return int(count_number)
            except:
                return None
        else:
            return None

    def check_file_upload_status(self, destination_path, filename, container):
        """
        Check file exists in filesystem, status
        :param destination_path: str
        :param filename: str
        :param container: Container
        :return: FileSystem
        """
        try:
            filesystem = FileSystem.objects.get(fq_path_reference=md5(destination_path + filename), container=container)
        except:
            return None
        return filesystem

    def get(self, request, *args, **kwargs):
        """
        Create upload session
        ---

        serializer: block.serializers.chunk.UploadSessionSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest ( Call via web )
              required: false
              type: string
              paramType: query

            - name: name
              description: name of the file ( max 256 char )
              required: false
              type: string
              paramType: query

            - name: dest
              description: relative path from root / ( /folder_name  )
              required: false
              type: string
              paramType: query

            - name: reported_total_size
              description: total size of the file from client ( in bytes )
              required: false
              type: integer
              paramType: query

            - name: chunks
              description: total chunks
              required: false
              type: integer
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 413
              message: '{"error" : 413 , "message" : "Out of quota", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.GET
        filename = data.get('name', None)
        user_ip = get_ip(request)
        destination_path = data.get('dest', None)
        try:
            reported_total_size = int(data.get('reported_total_size', None))
            total_chunks = int(data.get('chunks', None))
        except:
            raise ParseError("Parameter missing")
        # Validate filename
        # if not FileSystemUtils.valid_file_name(filename):
        #     raise ParseError("Not valid filename")
        # Validate data
        validate_create_session(data)
        # Check user has swift account
        account, container = _init_swift(user_id)
        # Check quota
        check_quota_upload(container, reported_total_size)
        # Check destination valid
        destination_filesystem = check_upload_destination(destination_path, container)
        # Check file to upload status
        # Handle create session for upload process
        absolute_destination_path = destination_path
        if destination_path[-1] != "/":
            destination_path = destination_path + "/"

        try:
            file_upload_filesystem = FileSystem.objects.get(fq_path_reference=md5(destination_path + filename),
                                                            container=container)
        except:
            file_upload_filesystem = None

        # Extract extension and file name
        try:
            file_extension = filename.split(".")
            if len(file_extension) > 1:
                file_extension = str(file_extension[-1]).lower()
            else:
                file_extension = None
                # file_name_only = filename.split('.')[0]
        except:
            file_extension = None
            # file_name_only = filename

        file_path = (destination_path + filename)
        create_time = timezone.now()
        create_time_str = str(create_time)
        kind_file = get_file_kind(file_extension)

        if file_upload_filesystem == None:
            file_upload_filesystem = FileSystem(
                container=container,
                parent=destination_filesystem,
                type=FileSystem.FILE_TYPE,
                kind=kind_file,
                extension=file_extension,
                icon='default',
                fq_path_reference=md5(file_path),
                fq_path=file_path,
                file_name=filename,
                swift_href=container.name,
                is_dir=False,
                bytes=reported_total_size,
                user_last_modified=create_time,
                status=FileSystem.FILE_INIT
            )
            upload_session = UploadSession(
                user_id=user_id,
                file_name=filename,
                filesystem=file_upload_filesystem,
                destination=absolute_destination_path,
                total_chunks=total_chunks,
                reported_total_size=reported_total_size,
                expiry_at=create_time + timedelta(days=2),
                user_ip=user_ip,
                status=UploadSession.INIT
            )
            file_upload_filesystem.save()
            upload_session.save()
        else:
            count = file_upload_filesystem.count
            if not count:
                count = 1
            new_filename = get_increasing_file(file_upload_filesystem.file_name, count)
            new_file_path = destination_path + new_filename

            new_file_upload_filesystem = FileSystem(
                container=container,
                parent=destination_filesystem,
                type=FileSystem.FILE_TYPE,
                kind=kind_file,
                extension=file_extension,
                icon='default',
                fq_path_reference=md5(new_file_path),
                fq_path=new_file_path,
                file_name=new_filename,
                swift_href=container.name,
                is_dir=False,
                bytes=reported_total_size,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_INIT
            )
            upload_session = UploadSession(
                user_id=user_id,
                filesystem=new_file_upload_filesystem,
                file_name=new_filename,
                destination=absolute_destination_path,
                total_chunks=total_chunks,
                reported_total_size=reported_total_size,
                expiry_at=create_time + timedelta(days=2),
                user_ip=user_ip,
                status=UploadSession.INIT
            )
            file_upload_filesystem.count += 1
            file_upload_filesystem.save()
            new_file_upload_filesystem.save()
            upload_session.save()

        upload_host = settings.GET_BLOCK_HOST()
        response_data = UploadSessionSerializer(upload_session, context={"host": upload_host})
        return response.success(response_data.data)

    def post(self, request, *args, **kwargs):
        """
            Upload file by session
            ---

            type:
                status:
                    required: true
                    type: string
                    description: success
                is_uploaded_to_swift:
                    required: true
                    type: boolean
                    description: This is is not important for front-end

            parameters:
                - name: Authorization
                  description: user access token ( debug token 5075284997574d7f84dd8334a7c1d284 )
                  required: true
                  type: string
                  paramType: header

                - name: is_xhr
                  description: flag indicate JavaScript XMLHttpRequest
                  required: false
                  type: string
                  paramType: form

                - name: upload_id
                  description: id of session
                  required: true
                  type: string
                  paramType: form

                - name: album_id
                  description: id of album
                  required: false
                  type: string
                  paramType: form

                - name: file
                  description: total chunks
                  required: true
                  type: file
                  paramType: body

                - name: name
                  description: name of the file ( max 256 char )
                  required: true
                  type: string
                  paramType: form

                - name: dest
                  description: relative path from root / ( /folder_name  )
                  required: true
                  type: string
                  paramType: form

                - name: reported_total_size
                  description: total size of the file from client ( in bytes )
                  required: true
                  type: integer
                  paramType: form

                - name: offset
                  description: byte offset of the file ( start at 0 , end at reported_total_size - chunk_size )
                  required: true
                  type: integer
                  paramType: form

                - name: chunk
                  description: current chunk index ( start at 0 end at chunks - 1 )
                  required: true
                  type: integer
                  paramType: form

                - name: chunks
                  description: total chunks
                  required: true
                  type: integer
                  paramType: form

            responseMessages:
                - code: 200
                  message: Success Json Object
                - code: 400
                  message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
                - code: 403
                  message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
                - code: 404
                  message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
                - code: 413
                  message: '{"error" : 413 , "message" : "Out of quota", "data" : {}}'
                - code: 500
                  message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

            consumes:
                - application/json

            produces:
                - application/json
            """
        user_id = request.auth
        file_upload = request.FILES.get('file')
        param_data = request.POST
        user_ip = get_ip(request)
        validate_upload_chunk_param(param_data, file_upload)
        upload_id = param_data['upload_id']
        report_filename = param_data['name']
        destination_path = param_data['dest']
        reported_total_size = int(param_data['reported_total_size'])
        byte_offset = int(param_data['offset'])
        current_chunk = int(param_data['chunk'])
        reported_chunks = int(param_data['chunks'])

        album_id = param_data.get('album_id', None)

        if not FileSystemUtils.valid_file_name(report_filename):
            raise ParseError("Not valid filename")

        try:
            upload_session = UploadSession.objects.get(id=upload_id)
        except Exception as ex:
            logger.error("Session not found in db " + str(ex))
            raise ParseError("Session not found")

        filename = file_upload.name
        filesize = file_upload.size

        if upload_session.status >= UploadSession.RECEIVED or upload_session.expiry_at <= timezone.now():
            raise ParseError("Session expired")

        if upload_session.current_chunk != current_chunk:
            raise ParseError("Chunk is not follow by order")

        current_upload_session = UploadSession(
            user_id=user_id,
            file_name=report_filename,
            destination=destination_path,
            current_chunk=current_chunk,
            total_chunks=reported_chunks,
            reported_total_size=reported_total_size,
            user_ip=user_ip
        )
        if current_upload_session != upload_session:
            raise ParseError("Session not valid")

        # Get status is first chunk
        is_first_chunk = False
        if current_chunk == 0:
            is_first_chunk = True

        # Get status is last chunk
        is_last_chunk = False
        if current_chunk == (upload_session.total_chunks - 1):
            is_last_chunk = True

        account, container = get_account_container(user_id)
        # Check quota
        check_quota_upload(container, reported_total_size)

        # If upload first chunk - change status to uploading
        if upload_session.INIT == UploadSession.INIT:
            upload_session.status = UploadSession.UPLOADING
            filesystem = upload_session.filesystem
            filesystem.status = FileSystem.FILE_UPLOADING
            filesystem.save()
            upload_session.save()
        # Create progress for every chunk
        progress = UploadProgress(
            session=upload_session,
            chunk=current_chunk,
            offset=byte_offset,
            user_ip=user_ip,
            status=UploadProgress.UPLOADING
        )
        progress.save()
        # Upload to local file
        file_id = upload_session.filesystem_id
        full_path, md5sum, file_status = handle_uploaded_file(file_upload,
                                                              user_id + "_temp/" + timezone.now().strftime("%Y-%m-%d"),
                                                              filename + "_" + file_id + "." + str(current_chunk))

        storage = SwiftStorage(account, container)

        # Sync file to swift synchronously
        is_uploaded = False
        if current_upload_session.total_chunks == 1:
            is_uploaded = storage.upload_single_file(full_path, file_id)
        else:
            is_uploaded = storage.upload_chunk(full_path, file_id, upload_session.current_chunk)

        if file_status:
            # Generate thumbnail if file upload
            progress.user_ip = user_ip
            progress.local_file = full_path
            progress.file_hash = md5sum
            progress.status = UploadProgress.RECEIVED
            progress.completed_at = timezone.now()
            progress.save()

            filesystem = upload_session.filesystem

            if is_first_chunk:
                if filesystem.extension in settings.AUDIO_EXTENSION:
                    try:
                        info = mediainfo.MediaInfo(filename=full_path)
                        infoData = info.getInfo()
                        infoData['fid'] = filesystem.id
                        infoData['fileSize'] = reported_total_size
                        filesystem.kind = FileSystem.AUDIO_KIND
                        track.delay("media_upload", json.dumps(infoData), "audio")
                    except Exception as ex:
                        logger.error(ex)
                elif filesystem.extension in settings.VIDEO_EXTENSION:
                    try:
                        info = mediainfo.MediaInfo(filename=full_path)
                        infoData = info.getInfo()
                        infoData['fid'] = filesystem.id
                        infoData['fileSize'] = reported_total_size
                        filesystem.kind = FileSystem.VIDEO_KIND
                        track.delay("media_upload", json.dumps(infoData), "video")
                    except Exception as ex:
                        logger.error(ex)

            if is_last_chunk:
                upload_session.status = UploadSession.RECEIVED
                if current_upload_session.total_chunks > 1:
                    storage.upload_manifest(file_id)
                    # full_path = storage.get_file_from_storage(file_id, filesystem.file_name,
                    #                                           user_id + "_temp/" + timezone.now().strftime("%Y-%m-%d"), container)
                    # If only one chunk : generate thumbnail and save to swift
                # if filesystem.extension in settings.IMAGE_EXTENSION:
                #     filesystem.kind = FileSystem.IMAGE_KIND
                #     local_thumbnails_paths, track_exif = generate_thumbnail(filename, full_path)
                #     try:
                #         track_exif['fid'] = filesystem.id
                #         track_exif['FileSize'] = reported_total_size
                #         if track_exif:
                #             track_exif['exif'] = True
                #             track.delay("media_upload", json.dumps(track_exif), "image")
                #         else:
                #             track_exif['exif'] = False
                #             track.delay("media_upload", json.dumps(track_exif), "image")
                #     except Exception as ex:
                #         logger.exception(ex)
                #
                #     if local_thumbnails_paths:
                #         try:
                #             small_thumb = local_thumbnails_paths[0]
                #             storage.upload_single_file(small_thumb[0], small_thumb[1])
                #             large_thumb = local_thumbnails_paths[1]
                #             storage.upload_single_file(large_thumb[0], large_thumb[1])
                #             album_large_thumb = local_thumbnails_paths[2]
                #             storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])
                #
                #             filesystem.thumbnail_url_tmpl = small_thumb[1]
                #             filesystem.large_thumbnail_url_tmpl = large_thumb[1]
                #             filesystem.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                #         except Exception as ex:
                #             logger.exception(ex)

                # generate thumbnail and save to swift
                if filesystem.extension in settings.IMAGE_EXTENSION:
                    generate_thumb.delay(user_id, filesystem.id, full_path)
                # consider_image_file = filesystem.extension in settings.IMAGE_EXTENSION
                # if not consider_image_file:
                #     for _prefix in settings.IMAGE_PREFIX:
                #         consider_image_file = filename.startswith(_prefix)
                #         if consider_image_file:
                #             break
                # if consider_image_file:
                #     generate_thumb.delay(user_id, upload_session.id, filename, full_path)
                    # generate_thumb(user_id, upload_session.id, filename, full_path)

                if filesystem.kind == FileSystem.VIDEO_KIND:
                    album_utils = AlbumUtils(container_id=container)
                    auto_video_album = album_utils.get_auto_video_album()
                    album_utils._album = auto_video_album
                    album_utils.add_item_to_album(filesystem=filesystem, check_permission=False)

                filesystem.user_last_modified = timezone.now()
                filesystem.status = FileSystem.FILE_ACTIVE
                filesystem.save()
                # Increase quota
                SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + filesystem.bytes)
                # Index to elasticsearch
                filesystem.index()
                upload_session.save()

                # add file to album
                if album_id != None:
                    album_utils = AlbumUtils(user_id=user_id, album_id=album_id)
                    album_utils.add_item_to_album(filesystem=file_id, check_permission=True)
            else:
                upload_session.current_chunk = upload_session.current_chunk + 1
                upload_session.save()
            return response.success({"status": "success", "is_uploaded_to_swift": is_uploaded})
        else:
            upload_session.status = UploadSession.FAILED
            progress.status = UploadProgress.FAILED
            progress.completed_at = timezone.now()
            upload_session.save()
            progress.save()
            raise ParseError("System Error , Please re-upload file with current session")

            # Upload to swift
