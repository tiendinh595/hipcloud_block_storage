#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import threading

import boto
import boto.s3.connection

from django.conf import settings
from rest_framework.views import APIView
from block.models.sync import SyncSession
from block.serializers.sync import SyncSessionSerializer, SyncStatusSerializer
from block.services.common import response
from rest_framework.exceptions import ParseError
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftContainer, SwiftAccount
from block.services.file import FileSystemPath
from block.tasks.sync_data_v2 import sync_dropbox, dropbox_check_quota, dropbox_check_quota_logic, sync_dropbox_logic
import dropbox
import logging

logger = logging.getLogger(__name__)

class CheckS3View(APIView):

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Check quota for sync dropbox
        ---

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            S3_HOST = '103.68.252.10'
            S3_PORT = 8081
            account = SwiftAccount.objects.get(user_id='8f2cf68feb6f468790c5e1d0685a655a')
            container = SwiftContainer.objects.get(pk='upbox_3c7f0e43408740efd8d8c50862')
            s3_conn = boto.connect_s3(
                aws_access_key_id=account.access_key,
                aws_secret_access_key=account.secret_key,
                host=S3_HOST,
                port=S3_PORT,
                is_secure=False,
                calling_format=boto.s3.connection.OrdinaryCallingFormat()
            )
            bucket = s3_conn.get_bucket(container.id)
            print(bucket)

            key = bucket.new_key('12345678')
            print(key)

        except Exception as ex:
            print(ex)

        return response.success({"status": -1})

class DropboxCheckQuotaView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Check quota for sync dropbox
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: dropbox_token
              description: token oauth2 from dropbox
              required: true
              type: string
              paramType: form

            - name: resync
              description: confirm delete all files (only stop status)
              required: false
              type: boolean
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        dropbox_token = data.get('dropbox_token', None)
        resync = data.get('resync', False)

        container = SwiftContainer.objects.get(account__user_id=user_id)

        # Check tooken
        dbox = dropbox.Dropbox(dropbox_token)
        try:
            dbox_user = dbox.users_get_current_account()
        except Exception as ex:
            logger.error(ex)
            return response.fail("Token not valid")

        # check SyncSession
        try:
            sync_session = SyncSession.objects.get(container__id=container.id, sync_type=SyncSession.SYNC_DROPBOX, email_sync=dbox_user.email)
        except:
            sync_session = None

        if sync_session != None:
            if sync_session.status == SyncSession.CHECK_QUOTA or (sync_session.status == SyncSession.CANCEL and str(resync).lower() != 'true') or sync_session.status == SyncSession.UNLINKED or sync_session.status == SyncSession.LINKED or (sync_session.status == SyncSession.SUCCESS and str(resync).lower() != 'true'):
                response_data = SyncStatusSerializer([sync_session], many=True)
                return response.success(response_data.data[0])
            if sync_session.status == SyncSession.CANCEL or sync_session.status == SyncSession.SUCCESS:
                try:
                    file_path = FileSystemPath(SyncSession.FOLDER_DROPBOX+"/" + sync_session.email_sync)
                    file_path.init_user_container(user_id).sync_path()
                    file_path.delete()
                    file_path.delete_permanent()
                except Exception as ex:
                    logger.error(ex)
                    pass

        # update or create SyncSession
        try:
            if sync_session != None:
                sync_session.status = SyncSession.CHECK_QUOTA
                sync_session.token = dropbox_token
            else:
                sync_session = SyncSession(
                    container = container,
                    token = dropbox_token,
                    status = SyncSession.CHECK_QUOTA,
                    sync_type = SyncSession.SYNC_DROPBOX,
                    total_size = 0,
                    email_sync = dbox_user.email
                )
            sync_session.save()
            logger.info("sync_session.id: "+sync_session.id)
        except Exception as ex:
            logger.error(ex)
            return response.fail("Session not created")

        # test logic check quota
        # threading.Thread(
        #     target=dropbox_check_quota_logic,
        #     args=(sync_session.id, container.id, request.META.get('HTTP_AUTHORIZATION'), user_id)
        # ).start()
        # return response.success({"status": "success", "sync_session_id": sync_session.id})

        # Spawn sync tasks
        job = dropbox_check_quota.apply_async(([sync_session.id, container.id, request.META.get('HTTP_AUTHORIZATION'), user_id]))
        # task = AsyncResult(job.id)
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class DropboxSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Start sync dropbox
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container)
        except:
            return response.fail("Session not found")

        if sync_session.status != SyncSession.UNLINKED:
            response_data = SyncStatusSerializer([sync_session], many=True)
            return response.success(response_data.data[0])

        # Check tooken
        dbox = dropbox.Dropbox(sync_session.token)
        try:
            dbox.users_get_current_account()
        except Exception as ex:
            logger.error(ex)
            return response.fail("Token not valid")

        # update sync status
        sync_session.status = SyncSession.LINKED
        sync_session.save()

        # test logic sync
        # threading.Thread(
        #     target=sync_dropbox_logic,
        #     args=(sync_session.id, sync_session.container.id, request.META.get('HTTP_AUTHORIZATION'), user_id)
        # ).start()
        # return response.success({"status": "success", "sync_session_id": sync_session.id})

        # Spawn sync tasks
        job = sync_dropbox.apply_async(([sync_session.id, sync_session.container.id, request.META.get('HTTP_AUTHORIZATION'), user_id]))
        # task = AsyncResult(job.id)
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class DropboxStopSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Stop sync dropbox
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container)
        except:
            return response.fail("Session not found")

        if sync_session.status != SyncSession.LINKED:
            return response.fail("Sync not running")

        # update stop status
        sync_session.status = SyncSession.CANCEL
        sync_session.save()
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class DropboxSyncCommandStatusView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Get status sync dropbox
        ---

        serializer: block.serializers.sync.SyncSessionSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container)
        except:
            return response.fail("Session not found")

        response_data = SyncSessionSerializer([sync_session], many=True)
        return response.success(response_data.data[0])