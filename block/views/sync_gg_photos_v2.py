#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import os
import re
import threading
from collections import OrderedDict

import datetime

import requests
import time
from django.utils import timezone
from django.conf import settings
from googleapiclient.http import MediaIoBaseDownload
from oauth2client.client import OAuth2WebServerFlow
from rest_framework.views import APIView

from block.models import SyncSession
from block.serializers.sync import SyncSessionSerializer, SyncStatusSerializer
from block.services.common import response
from rest_framework.exceptions import PermissionDenied , ParseError
from block.authentication import TokenAuthentication, TokenAuthenticationGoogleRedirect
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftContainer
from apiclient import discovery
from oauth2client import client
import logging
import httplib2
import json
from dateutil.parser import parse

from block.services.file import FileSystemPath
from block.tasks.sync_data_v2 import gphotos_check_quota_logic, sync_gphotos_logic, gphotos_check_quota, sync_gphotos, \
    sync_gphotos_parse_files, ggoauth_refresh_token

logger = logging.getLogger(__name__)

class GGPhotosTestListView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Check quota for sync google photos
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: google_photos_access_token
              description: access token oauth2 from google
              required: true
              type: string
              paramType: form

            - name: google_photos_refresh_token
              description: refresh token oauth2 from google
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST

        google_token = data.get('google_photos_access_token', None)
        google_refresh_token = data.get('google_photos_refresh_token', None)
        if not google_token:
            return response.fail("Token not found")

        # req = ggoauth_refresh_token(google_refresh_token)
        # if 'access_token' not in req:
        #     return response.fail("Refresh token is not valid")
        # else:
        #     google_token = req['access_token']

        try:
            credentials = client.AccessTokenCredentials(google_token, 'my-user-agent/1.0')
            http = httplib2.Http()
            http = credentials.authorize(http)
            gphotos_user = discovery.build('oauth2', version='v2', http=http)
            user_info = gphotos_user.userinfo().get().execute()
            print 'email: ' + user_info['email']

            credentials = client.AccessTokenCredentials(google_token, 'my-user-agent/1.0')
            http = httplib2.Http()
            http = credentials.authorize(http)
            service = discovery.build('drive', 'v3', http=http)

            files = sync_gphotos_parse_files(service, '123456')
            for item in files:
                print item['name']

            # param = {
            #     'pageSize': 20,
            #     'fields': 'nextPageToken, files(id, name, size, ownedByMe, explicitlyTrashed, createdTime, modifiedTime)',
            #     'spaces': 'photos'
            # }
            # result = service.files().list(**param).execute()
            # list = []
            # for item in result.get('files', []):
            #     createad_at = parse(item['createdTime'])
            #     updated_at = parse(item['modifiedTime'])
            #     list.append({
            #         'name': item['name'],
            #         'size': item['size'],
            #         'created_at': createad_at.strftime('%Y-%m-%d %H:%M:%S'),
            #         'updated_at': updated_at.strftime('%Y-%m-%d %H:%M:%S')
            #     })
            # print (json.dumps(list))
        except Exception as ex:
            logger.error(ex)
            return response.fail("Token is not valid")

        return response.success({"status": "success"})

class GGPhotosCheckQuotaView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Check quota for sync google photos
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        # check SyncSession
        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(id = sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_PHOTOS)
        except:
            return response.fail("Session not found")

        if sync_session.status == SyncSession.CHECK_QUOTA or sync_session.status == SyncSession.UNLINKED or sync_session.status == SyncSession.LINKED or sync_session.status == SyncSession.SUCCESS:
            response_data = SyncStatusSerializer([sync_session], many=True)
            return response.success(response_data.data[0])

        if sync_session.status == SyncSession.CANCEL or sync_session.status == SyncSession.SUCCESS:
            try:
                file_path = FileSystemPath(SyncSession.FOLDER_GOOGLE_PHOTOS+"/"+sync_session.email_sync)
                file_path.init_user_container(user_id).sync_path()
                file_path.delete()
                file_path.delete_permanent()
            except Exception as ex:
                logger.error(ex)
                pass

        # Check tooken
        req = ggoauth_refresh_token(sync_session.refresh_token)
        if 'access_token' not in req:
            return response.fail("Refresh token is not valid")

        google_token = req['access_token']

        # update or create SyncSession
        try:
            sync_session.status = SyncSession.CHECK_QUOTA
            sync_session.token = google_token
            sync_session.save()
            logger.info("sync_session.id: " + sync_session.id)
        except Exception as ex:
            logger.error(ex)
            return response.fail("Session not updated")

        # test logic check quota
        # threading.Thread(
        #     target=gphotos_check_quota_logic,
        #     args=(sync_session.id, container.id, request.META.get('HTTP_AUTHORIZATION'), user_id)
        # ).start()
        # return response.success({"status": "success", "sync_session_id": sync_session.id})

        # Spawn sync tasks
        job = gphotos_check_quota.apply_async(([sync_session.id, container.id, request.META.get('HTTP_AUTHORIZATION'), user_id]))
        # task = AsyncResult(job.id)
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class GGPhotosSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Start sync google photos
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_PHOTOS)
        except:
            return response.fail("Session not found")

        if sync_session.status != SyncSession.UNLINKED:
            response_data = SyncStatusSerializer([sync_session], many=True)
            return response.success(response_data.data[0])

        # update sync status
        sync_session.status = SyncSession.LINKED
        sync_session.save()

        # test logic sync
        # threading.Thread(
        #     target=sync_gphotos_logic,
        #     args=(sync_session.id, sync_session.container.id, request.META.get('HTTP_AUTHORIZATION'), user_id)
        # ).start()
        # return response.success({"status": "success", "sync_session_id": sync_session.id})

        # Spawn sync tasks
        job = sync_gphotos.apply_async(([sync_session.id, sync_session.container.id, request.META.get('HTTP_AUTHORIZATION'), user_id]))
        # task = AsyncResult(job.id)
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class GGPhotosStopSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Stop sync google photos
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_PHOTOS)
        except:
            return response.fail("Session not found")

        if sync_session.status != SyncSession.LINKED:
            return response.fail("Sync not running")

        # update stop status
        sync_session.status = SyncSession.CANCEL
        sync_session.save()
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class GGPhotosSyncCommandStatusView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Get status sync google photos
        ---

        serializer: block.serializers.sync.SyncSessionSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_PHOTOS)
        except:
            return response.fail("Session not found")

        response_data = SyncSessionSerializer([sync_session], many=True)
        return response.success(response_data.data[0])