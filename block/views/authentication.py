#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from block.services.common import response
from django.apps import apps
get_model = apps.get_model
from block.models.authenticate import AccessToken
from block.models.swift import SwiftAccount, SwiftContainer
from rest_framework.exceptions import PermissionDenied , ParseError
from block.services.file import FileSystem, FileSystemPath
from block.services.common import hash_path
from django.utils import timezone
from django.conf import settings
from jose import jwt
import swiftclient
import boto
import boto.s3.connection
import logging

logger = logging.getLogger(__name__)

class Sync_Token(APIView):

    def init_swift_container(self, account, container, project_name):
        if settings.CURRENT_STORAGE == settings.ALLOWS_STORAGE['s3']:
            try:
                conn = boto.connect_s3(
                    aws_access_key_id=account.access_key,
                    aws_secret_access_key=account.secret_key,
                    host=settings.S3_HOST,
                    port=settings.S3_PORT,
                    is_secure=False,
                    calling_format=boto.s3.connection.OrdinaryCallingFormat()
                )
                conn.create_bucket(container.id)
                return True
            except Exception as ex:
                logger.error(ex)
                return False
        else:
            try:
                conn = swiftclient.client.Connection(
                    authurl = settings.KEYSTONE_HOST,
                    user = account.username,
                    key = account.password,
                    auth_version = settings.SWIFT_AUTH_VERSION,
                    os_options = {
                        'user_domain_name': settings.KEYSTONE_USER_DOMAIN_ID,
                        'project_domain_name': settings.KEYSTONE_PROJECT_DOMAIN_ID,
                        'project_name': project_name,
                        'tenant_name': settings.KEYSTONE_TENANT_NAME
                    }
                )
                conn.put_container(container.id, headers={"X-Container-Meta-Quota-Count" : container.max_quota})
                conn.post_account(
                    headers={
                        "X-Account-Meta-Temp-URL-Key": container.temp_url_secret_key_1,
                        "X-Account-Meta-Temp-URL-Key-2": container.temp_url_secret_key_2
                    }
                )
                return True
            except Exception as ex:
                logger.error(ex)
                return False

    def init_swift_account(self, username, password):
        try:
            user_project = settings.KEYSTONE.projects.create( username, settings.KEYSTONE_USER_DOMAIN_ID)
            upbox_user = settings.KEYSTONE.users.create(name = username, password = password,
                                                        project = user_project,
                                                        domain = settings.KEYSTONE_USER_DOMAIN_ID,
                                                        enabled = True)
            user_role = settings.KEYSTONE.roles.get(settings.KEYSTONE_USER_ROLE_ID)
            settings.KEYSTONE.roles.grant(user_role, upbox_user, project = user_project)
            ec2 = settings.KEYSTONE.ec2.create(upbox_user.id, user_project.id)
            return upbox_user, user_project, ec2
        except Exception as ex:
            logger.error(ex)
            return None

    def init_user_swift(self, user_id):
        try:
            account = SwiftAccount.objects.get(user_id = user_id)
            container = SwiftContainer.objects.get(account = account)
        except:
            # Init account
            account = SwiftAccount(user_id=user_id, username=user_id,
                                   domain_id=settings.KEYSTONE_PROJECT_DOMAIN_ID)
            swift_profile , user_project, ec2 = self.init_swift_account(user_id, account.password)
            account.id = swift_profile.id
            account.project_id = user_project.id
            account.project_name = user_project.name
            account.access_key = ec2.access
            account.secret_key = ec2.secret
            account.save()
            # Init container
            container = SwiftContainer(account=account, name=user_id,
                                       type=settings.CURRENT_STORAGE,
                                       path="/AUTH_" + user_project.id)
            self.init_swift_container(account, container, user_project.name)
            container.save()
        return account, container

    def init_pre_exist_folders(self, container):
        path_hash = hash_path(settings.CAMERA_UPLOAD_ROOT)
        try:
            camera_folder = FileSystem.objects.get(container = container, fq_path_reference = path_hash)
            # If previous camera folder deleted - reactive it
            if camera_folder.status == FileSystem.FILE_DELETED:
                camera_folder.status = FileSystem.FILE_ACTIVE
                camera_folder.save()
                camera_folder.index()
        except:
            camera_folder = FileSystem(
                container = container,
                type = FileSystem.ALBUM_FOLDER_TYPE,
                kind = 'default',
                icon = 'default',
                fq_path_reference = path_hash,
                fq_path = settings.CAMERA_UPLOAD_ROOT,
                file_name = settings.CAMERA_UPLOAD_FOLDER,
                swift_href = container.name,
                is_dir = True,
                user_last_modified = timezone.now(),
                status = FileSystem.FILE_ACTIVE
            )
            camera_folder.save()
            camera_folder.index()
        # Cloud Camera
        logger.info("Create Cloud Camera")
        path_hash = hash_path(settings.CLOUD_CAMERA_ROOT)
        try:
            cloud_camera_folder = FileSystem.objects.get(container = container,
                                                         fq_path_reference = path_hash)
            # If previous camera folder deleted - reactive it
            is_changed = False
            if cloud_camera_folder.type != FileSystem.ALBUM_FOLDER_TYPE:
                cloud_camera_folder.type = FileSystem.ALBUM_FOLDER_TYPE
                is_changed = True

            if cloud_camera_folder.status == FileSystem.FILE_DELETED:
                cloud_camera_folder.status = FileSystem.FILE_ACTIVE
                is_changed = True

            if is_changed:
                cloud_camera_folder.save()
                cloud_camera_folder.index()
        except:
            cloud_camera_folder = FileSystem(
                container = container,
                type = FileSystem.ALBUM_FOLDER_TYPE,
                kind = 'default',
                icon = 'default',
                fq_path_reference = path_hash,
                fq_path = settings.CLOUD_CAMERA_ROOT,
                file_name = settings.CLOUD_CAMERA_FOLDER,
                swift_href = container.name,
                is_dir = True,
                user_last_modified = timezone.now(),
                status = FileSystem.FILE_ACTIVE
            )
            cloud_camera_folder.save()
            cloud_camera_folder.index()
        # Photos
        logger.info("Create Photos")
        path_hash = hash_path(settings.PHOTOS_ROOT)
        try:
            photo_folder = FileSystem.objects.get(container = container,
                                                  fq_path_reference = path_hash)
            is_changed = False
            if photo_folder.status == FileSystem.ALBUM_FOLDER_TYPE:
                photo_folder.type = FileSystem.ALBUM_FOLDER_TYPE
                is_changed = True
            # If previous camera folder deleted - reactive it
            if photo_folder.status == FileSystem.FILE_DELETED:
                photo_folder.status = FileSystem.FILE_ACTIVE
                is_changed = True

            if is_changed:
                photo_folder.save()
                photo_folder.index()
        except:
            photo_folder = FileSystem(
                container = container,
                type = FileSystem.ALBUM_FOLDER_TYPE,
                kind = 'default',
                icon = 'default',
                fq_path_reference = path_hash,
                fq_path = settings.PHOTOS_ROOT,
                file_name = settings.PHOTOS_FOLDER,
                swift_href = container.name,
                is_dir = True,
                user_last_modified = timezone.now(),
                status = FileSystem.FILE_ACTIVE
            )
            photo_folder.save()
            photo_folder.index()

    def verify_token(self, payload):
        try:
            json_profile = jwt.decode(payload, settings.JWT_PRIVATE_SIGNATURE , algorithms=['HS256'])
            return json_profile
        except Exception as ex:
            logger.error("Invalid token : " + str(ex) + " " + payload)
            raise PermissionDenied("Invalid token")

    def sync_token(self, payload, action = 'create'):
        if action == 'create':
            try:
                # Init container for user
                account , container = self.init_user_swift(payload['iss'])
                self.init_pre_exist_folders(container)
                # Init camera upload folder
                access_token = AccessToken(id = payload['tok'], user_id = payload['iss'],
                                           type = payload['mth'], subject_id = payload['sub'],
                                           issued_at = payload['iat'], expiry_at = payload['exp'])
                access_token.save()
                return access_token
            except Exception as ex:
                logger.error(ex)
                raise ParseError(ex)
        else:
            token = payload['tok']
            try:
                token_data = AccessToken.objects.get(id = token)
                token_data.delete()
            except Exception as ex:
                raise ParseError("Token data not found")
            return True

    def post(self, request, *args, **kwargs):
        """
        [BACKEND ONLY] Sync token from backend service to block service via JWT

        Receive: jwt token with embed json data

        {
            "iss": "user_id",
            "iat": 1300819370,
            "exp": 1300819380,
            "mth": "fb/gg/em/mo",
            "tok": "token",
            "sub": "subject_id",
            "context": {
                "profile": {
                    "id": "batman",
                    "username": "bat_wayne",
                    ..
                }
            },
            "admin": false
        }

        ---
        parameters:
            - name: payload
              description: token string
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"message" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        data = request.POST
        payload = data.get('payload')
        if not payload:
            raise PermissionDenied("Unauthorized")
        # Verify token & get payload data
        json_data = self.verify_token(payload)

        # Insert to db
        self.sync_token(json_data)

        return response.success({"message" : "success"})

    def delete(self, request, *args, **kwargs):
        """
        Sync token from backend service to block service via JWT

        Receive: jwt token with embed json data

        {
            "iss": "user_id",
            "iat": 1300819370,
            "exp": 1300819380,
            "mth": "fb/gg/em/mo",
            "tok": "token",
            "sub": "subject_id",
            "context": {
                "profile": {
                    "id": "batman",
                    "username": "bat_wayne",
                    ..
                }
            },
            "admin": false
        }

        ---
        parameters:
            - name: payload
              description: token string
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"message" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        data = request.POST
        payload = data.get('payload')
        if not payload:
            raise PermissionDenied("Unauthorized")
        # Verify token & get payload data
        json_data = self.verify_token(payload)
        # Delete from
        self.sync_token(json_data , 'delete')

        return response.success({"message": "success"})