#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db.models import Q
from block.serializers.swift import FileSystemSerializer, AlbumFileSystemSerializer, PhotoFileSystemSerializer
from django.core.paginator import EmptyPage, PageNotAnInteger
from block.models.swift import SwiftContainer, FileSystem
from block.services.file import FileSystemUtils, FileSystemPath
from rest_framework.exceptions import ParseError
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.services.common import response
from rest_framework.views import APIView
from django.core.paginator import Paginator
from django.conf import settings
import hashlib
import logging
from block.tasks import *


logger = logging.getLogger(__name__)


def md5(str):
    # settings.HASH_PATH_SALT
    m = hashlib.md5()
    m.update(str)
    return m.hexdigest()


class AlbumCommandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Create album
        ---
        serializer: block.serializers.swift.FileSystemSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest ( mobile is false )
              required: false
              type: string
              paramType: form

            - name: name
              description: name of the album
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        album_name = data['name']

        if not FileSystemUtils.valid_file_name(album_name):
            return response.fail("Folder / File name is not valid")

        photos_folder = FileSystemPath(settings.PHOTOS_ROOT)
        photos_folder.init_user_container(user_id).sync_photos_folder()
        album_folder = photos_folder.create_album(album_name)

        response_data = FileSystemSerializer(album_folder)
        return response.success(response_data.data)

    def delete(self, request, *args, **kwargs):
        """
        Delete album
        ---

        type:
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest ( mobile is false )
              required: false
              type: string
              paramType: form

            - name: name
              description: name of the album
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        album_name = data['name']
        file_path = FileSystemPath(settings.PHOTOS_ROOT + "/" + album_name)
        file_path.init_user_container(user_id).sync_path()
        file_path.delete()
        return response.success({"status": "success"})


class AlbumView(APIView):
    """
    Get List Albums
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Get Albums
        ---
        serializer: block.serializers.api_document.AlbumFileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest ( mobile is false )
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 2 , max is 100 )
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        page, limit = int(data.get('page', 1)), int(data.get('limit', 10))
        try:
            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        # Query all albumn
        camera_upload_folder = FileSystemPath(settings.PHOTOS_ROOT)
        camera_upload_folder.init_user_container(user_id).sync_photos_folder()
        user_albums = camera_upload_folder.list_custom({"type": FileSystem.ALBUM_FOLDER_TYPE}, ["-user_last_modified"])
        paginator = Paginator(user_albums, limit)
        browse_data = paginator.page(page)
        try:
            response_data = AlbumFileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = AlbumFileSystemSerializer([], many=True)

        # Query preview for each album
        return response.success({
            "metadata": {
                "page": page,
                "limit": limit
            },
            "items": response_data.data
        })


class PhotoView(APIView):
    """
    Get Photos
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Get Photos ( All or of a album )
        ---
        serializer: block.serializers.api_document.PhotoPreviewFileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest ( mobile is false )
              required: false
              type: string
              paramType: form

            - name: album_name
              description: name of the album ( default is empty for query all )
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 2 , max is 100 )
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        album_name = data.get("album_name", None)
        page, limit = int(data.get('page', 1)), int(data.get('limit', 10))
        try:
            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")
        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
        except Exception as ex:
            raise ParseError("User not found")

        if not album_name:
            user_photos = FileSystem.objects.filter(container=container,
                                                    status=FileSystem.FILE_ACTIVE).filter(Q(kind=FileSystem.IMAGE_KIND) | Q(kind=FileSystem.VIDEO_KIND)).order_by("-user_last_modified")
        else:
            camera_upload_folder = FileSystemPath(settings.PHOTOS_ROOT + "/" + album_name)
            camera_upload_folder.init_user_container(user_id).sync_photos_folder()
            # user_photos = camera_upload_folder.list_custom({"kind" : FileSystem.IMAGE_KIND}, ["-user_last_modified"])
            user_photos = camera_upload_folder.list_custom({}, ["-user_last_modified"])
        paginator = Paginator(user_photos, limit)
        browse_data = paginator.page(page)

        for item in browse_data:
            if item.in_queue == False and item.extension in settings.IMAGE_EXTENSION and item.is_dir == False and (str(item.thumbnail_url_tmpl) == '' or str(item.large_thumbnail_url_tmpl) == '' or str(item.album_large_thumbnail_url_tmpl) == '' or str(item.thumbnail_url_tmpl) == 'None' or str(item.large_thumbnail_url_tmpl) == 'None' or str(item.album_large_thumbnail_url_tmpl) == 'None'):
                generate_thumb.delay(user_id, item.id)
                item.in_queue = True
                item.save()

        try:
            response_data = PhotoFileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = PhotoFileSystemSerializer([], many=True)
        # Query preview for each album
        return response.success(
            {
                "header": response_data.data[:5],
                "items": response_data.data,
                "metadata": {
                    "limit": limit,
                    "page": page,
                    "total_pages": paginator.num_pages,
                    "total_files": paginator.count
                }
            }
        )
