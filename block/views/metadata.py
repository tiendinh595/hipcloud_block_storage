#!/usr/bin/env python
# -*- coding: utf-8 -*-
import hashlib

from django.db.models import F
from rest_framework.exceptions import APIException

from block.exceptions import OutOfQuota
from block.services.swift.files import SwiftStorage
from rest_framework.views import APIView
from block.services.common import response
from rest_framework.exceptions import PermissionDenied, ParseError
from block.services.common.cache import CACHE_DOWNLOAD
from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftContainer, SwiftAccount
from block.models.swift import FileSystem, Album
from block.services.file import FileSystemPath, FileSystemUtils
from block.serializers.swift import FileSystemSerializer, QuotaContainerSerializer
from block.services.album import AlbumUtils
from block.serializers.api_document import AlbumSerializer
from django.core.paginator import EmptyPage, PageNotAnInteger
from block.services.search.models import FileSystemSearch
from django.core.paginator import Paginator
from django.conf import settings
import json
import logging
from block.tasks import *

logger = logging.getLogger(__name__)


class FileDetailView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Get file detail by id
        ---

        serializer: block.serializers.api_document.FileSystemSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: file_id
              description: id of the file
              required: true
              type: string
              paramType: form

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_id = data.get('file_id', "/")
        is_xhr = data.get('is_xhr', None)
        file_object = FileSystem.objects.get(pk=file_id, status=FileSystem.FILE_ACTIVE)
        response_data = FileSystemSerializer(file_object)
        return response.success(response_data.data)


class BrowseFileView(APIView):
    """
    Browsing files
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Browse file by path
        ---

        serializer: block.serializers.api_document.FileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: only_folder
              description: flag browe only folder
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 2 )
              required: false
              type: string
              paramType: form

            - name: file_path
              description: Path to file /path/to/file
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_path = data.get('file_path', "/")
        page = data.get('page', '1')
        limit = data.get('limit', '20')
        is_xhr = data.get('is_xhr', None)

        only_folder = data.get('only_folder', '-1')

        if only_folder != '0' and only_folder != '1':
            only_folder = None
        else:
            only_folder = True if only_folder == '1' else False

        try:
            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        file_path = FileSystemPath(file_path)
        file_path.init_user_container(user_id).sync_path()
        user_files = file_path.list(["-is_dir", "-user_last_modified"], only_folder)
        paginator = Paginator(user_files, limit)
        browse_data = paginator.page(page)

        for item in browse_data:
            if item.in_queue == False and item.extension in settings.IMAGE_EXTENSION and item.is_dir == False and (str(item.thumbnail_url_tmpl) == '' or str(item.large_thumbnail_url_tmpl) == '' or str(item.album_large_thumbnail_url_tmpl) == '' or str(item.thumbnail_url_tmpl) == 'None' or str(item.large_thumbnail_url_tmpl) == 'None' or str(item.album_large_thumbnail_url_tmpl) == 'None'):
                generate_thumb.delay(user_id, item.id)
                item.in_queue = True
                item.save()

        try:
            response_data = FileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = FileSystemSerializer([], many=True)

        return response.success({
            "total_files": paginator.count,
            "total_pages": paginator.num_pages,
            "current_page": page,
            "file_info": response_data.data
        })


class RecentFileView(APIView):
    """
    Recent files
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Recents files
        ---
        serializer: block.serializers.api_document.FileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 2 )
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_path = data.get('file_path', "/")
        page = data.get('page', '1')
        limit = data.get('limit', '20')
        is_xhr = data.get('is_xhr', None)

        try:
            page = int(page)
            limit = int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        # container = SwiftContainer.objects.get(account__user_id=user_id)

        file_path = FileSystemPath(file_path)
        file_path.init_user_container(user_id).sync_path()
        user_files = file_path.list_recents(["-user_last_modified"])
        paginator = Paginator(user_files, limit)
        browse_data = paginator.page(page)
        try:
            response_data = FileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = FileSystemSerializer([], many=True)

        return response.success({
            "total_files": paginator.count,
            "total_pages": paginator.num_pages,
            "current_page": page,
            "file_info": response_data.data
        })


class DeletedFileView(APIView):
    """
    Trash can
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Recents files
        ---
        serializer: block.serializers.api_document.FileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 2 )
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_path = data.get('file_path', "/")
        page = data.get('page', '1')
        limit = data.get('limit', '20')
        is_xhr = data.get('is_xhr', None)

        try:
            page = int(page)
            limit = int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        file_path = FileSystemPath(file_path)
        file_path.init_user_container(user_id).sync_path()
        user_files = file_path.list_deleted(["-user_last_modified"])
        paginator = Paginator(user_files, limit)
        browse_data = paginator.page(page)
        try:
            response_data = FileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = FileSystemSerializer([], many=True)

        return response.success({
            "total_files": paginator.count,
            "total_pages": paginator.num_pages,
            "current_page": page,
            "file_info": response_data.data
        })


class NewFolderCommandView(APIView):
    """
    Create folder command view
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='New file')
    def post(self, request, *args, **kwargs):
        """
        Create new folder
        ---
        serializer: block.serializers.swift.FileSystemSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: file_path
              description: /path/to/folder/name ( default empty for create folder in root / )
              required: true
              type: string
              paramType: form

            - name: folder_name
              description: name of folder
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_path = data.get('file_path', '/')
        folder_name = data.get('folder_name', None)
        is_xhr = data.get('is_xhr', None)

        if not FileSystemUtils.valid_file_name(folder_name):
            return response.fail("Folder / File name is not valid")

        file_path = FileSystemPath(file_path)
        file_path.init_user_container(user_id).sync_path()
        new_folder = file_path.create_sub_folder(folder_name)
        response_data = FileSystemSerializer(new_folder)
        return response.success(response_data.data)


class RenameFolderCommandView(APIView):
    """
    Rename folder command view
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Rename file')
    def post(self, request, *args, **kwargs):
        """
        Rename folder
        ---
        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest ( mobile is false )
              required: false
              type: string
              paramType: form

            - name: file_path
              description: /path/to/folder/name
              required: true
              type: string
              paramType: form

            - name: new_name
              description: new file name
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth

        data = request.POST
        file_path = data.get('file_path', None)
        is_xhr = data.get('is_xhr', None)
        new_name = data.get('new_name', None)

        if file_path in settings.LIST_UNCHANGABLE_PATHS:
            return response.success({"status": "success"})

        if not FileSystemUtils.valid_file_name(new_name):
            return response.fail("Folder / File name is not valid")

        file_path = FileSystemPath(file_path)
        file_path.init_user_container(user_id).sync_path()
        rename_folder = file_path.rename(new_name)

        return response.success({"status": "success"})


class DeleteFolderCommandView(APIView):
    """
    Delete folder command view
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Delete file')
    def post(self, request, *args, **kwargs):
        """
        Delete multiple folder / file
        ---

        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "[/path1,/path2]"
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)

        try:
            files = json.loads(files)
            if len(files) > 100:
                return response.fail("Array is too large")
        except Exception as ex:
            return response.fail("Files is not valid")
        # Validate all files is in the same dir

        # Build path reference list
        for file_path in files:
            if file_path in settings.LIST_UNCHANGABLE_PATHS:
                continue
            file_path = FileSystemPath(file_path)
            file_path.init_user_container(user_id).sync_path()
            file_path.delete()

        return response.success({"status": "success"})


class RestoreCommandView(APIView):
    """
    Restore file/folder
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Delete file')
    def post(self, request, *args, **kwargs):
        """
        Restore file/folder
        ---
        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "[/path1,/path2]"
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)

        try:
            files = json.loads(files)
            if len(files) > 20:
                return response.fail("Array is too large")
        except Exception as ex:
            return response.fail("Files is not valid")
        # Validate all files is in the same dir

        # Build path reference list
        for file_path in files:
            file_path = FileSystemPath(file_path)
            file_path.init_user_container(user_id).sync_path()
            file_path.restore()

        return response.success({"status": "success"})


class DeletePermanentCommandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Delete file')
    def post(self, request, *args, **kwargs):
        """
        Delete Permanent file/folder
        ---
        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "[/path1,/path2]"
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)

        try:
            files = json.loads(files)
            if len(files) > 20:
                return response.fail("Array is too large")
        except Exception as ex:
            return response.fail("Files is not valid")
        # Validate all files is in the same dir

        # Build path reference list
        for file_path in files:
            file_path = FileSystemPath(file_path)
            file_path.init_user_container(user_id).sync_path()
            file_path.delete_permanent()

        return response.success({"status": "success"})


class EmptyTrashCommandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Empty trash
        ---
        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)

        file_path = FileSystemPath('/')
        file_path.init_user_container(user_id).sync_path()
        file_path.empty_trash()

        return response.success({"status": "success"})


class MoveFolderCommandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Move file')
    def post(self, request, *args, **kwargs):
        """
        Delete folder
        ---
        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "[/path1,/path2]"
              required: true
              type: string
              paramType: form

            - name: to_path
              description: /path/to/destination/folder
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)
        to_path = data.get('to_path', None)

        try:
            files = json.loads(files)
            if len(files) > 20:
                return response.fail("Array is too large")
        except:
            return response.fail("Files is not valid")

        # Validate to_path
        to_file_path = FileSystemPath(to_path)
        to_file_path.init_user_container(user_id).sync_path()
        # Move files
        for move_path in files:
            if move_path in settings.LIST_UNCHANGABLE_PATHS:
                continue
            move_path = FileSystemPath(move_path)
            move_path.init_user_container(user_id).sync_path()
            if to_file_path == move_path:
                raise ParseError("Can't move file to itself")
            move_path.move(to_file_path)
        return response.success({"status": "success"})


class SearchFileView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Browse file by path
        ---
        serializer: block.serializers.api_document.FileSystemSearchDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: page
              description: page > 0 ( default is 0 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 100 )
              required: false
              type: string
              paramType: form

            - name: keyword
              description: keyword
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "detail" : "Bad Request – Your request sucks"}'
            - code: 401
              message: '{"error" : 401 , "detail" : "Unauthorized – Your API key is wrong"}'
            - code: 403
              message: '{"error" : 403 , "detail" : "Forbidden – The kitten requested is hidden for administrators only"}'
            - code: 405
              message: '{"error" : 405 , "detail" : "Method Not Allowed – You tried to access a kitten with an invalid method"}'
            - code: 410
              message: '{"error" : 410 , "detail" : "Gone – The kitten requested has been removed from our servers"}'
            - code: 429
              message: '{"error" : 429 , "detail" : "Too Many Requests – You’re requesting too many kittens! Slow down!"}'
            - code: 500
              message: '{"error" : 500 , "detail" : "Internal Error – We had a problem with our server. Try again later."}'
            - code: 503
              message: 'HTML Service Unavailable – We’re temporarially offline for maintanance. Please try again later.'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        keyword = data.get('keyword', None)
        page = data.get('page', '0')
        limit = data.get('limit', '100')
        is_xhr = data.get('is_xhr', None)

        if not keyword:
            raise ParseError("Provide some keyword")

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            page, limit = int(page), int(limit)
            if page < 0:
                page = 0
        except:
            raise ParseError("Page is not integer")
        result = FileSystemSearch.search(container, keyword, page, limit)
        return response.success(result)


class QuotaInfoView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def get(self, request, *args, **kwargs):
        """
        Show quota & sync dropbox/google drive, google photos status
        ---

        many: false

        serializer: block.serializers.api_document.QuotaContainerDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "detail" : "Bad Request – Your request sucks"}'
            - code: 401
              message: '{"error" : 401 , "detail" : "Unauthorized – Your API key is wrong"}'
            - code: 403
              message: '{"error" : 403 , "detail" : "Forbidden – The kitten requested is hidden for administrators only"}'
            - code: 405
              message: '{"error" : 405 , "detail" : "Method Not Allowed – You tried to access a kitten with an invalid method"}'
            - code: 410
              message: '{"error" : 410 , "detail" : "Gone – The kitten requested has been removed from our servers"}'
            - code: 429
              message: '{"error" : 429 , "detail" : "Too Many Requests – You’re requesting too many kittens! Slow down!"}'
            - code: 500
              message: '{"error" : 500 , "detail" : "Internal Error – We had a problem with our server. Try again later."}'
            - code: 503
              message: 'HTML Service Unavailable – We’re temporarially offline for maintanance. Please try again later.'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
        except:
            raise ParseError("Data not found")
        result = QuotaContainerSerializer(container)
        return response.success(result.data)


class ShareFileUrlView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Get share file url
        ---

        serializer: block.serializers.api_document.FileDocumentSerializer

        parameters_strategy: replace

        many: false

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: file_id
              description: id of the file
              required: false
              type: string
              paramType: form

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "detail" : "Bad Request – Your request sucks"}'
            - code: 401
              message: '{"error" : 401 , "detail" : "Unauthorized – Your API key is wrong"}'
            - code: 403
              message: '{"error" : 403 , "detail" : "Forbidden – The kitten requested is hidden for administrators only"}'
            - code: 405
              message: '{"error" : 405 , "detail" : "Method Not Allowed – You tried to access a kitten with an invalid method"}'
            - code: 410
              message: '{"error" : 410 , "detail" : "Gone – The kitten requested has been removed from our servers"}'
            - code: 429
              message: '{"error" : 429 , "detail" : "Too Many Requests – You’re requesting too many kittens! Slow down!"}'
            - code: 500
              message: '{"error" : 500 , "detail" : "Internal Error – We had a problem with our server. Try again later."}'
            - code: 503
              message: 'HTML Service Unavailable – We’re temporarially offline for maintanance. Please try again later.'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_id = data.get('file_id', None)
        is_xhr = data.get('is_xhr', None)
        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            file_to_share = FileSystem.objects.get(id=file_id, container=container,
                                                   status=FileSystem.FILE_ACTIVE)
            response_data = FileSystemSerializer(file_to_share)
            return response.success(response_data.data)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("File not found")


class CopyFileCommandView(APIView):
    """
    Copy folder, file command view
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Copy multiple folder / file
        ---

        type :
            status:
                required: true
                type: string
                description: success
            files_fail:
                required: true
                type: array
                description: ['/file.png']

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "['/path1','/path2']"
              required: true
              type: string
              paramType: form
            - name: to_path
              description: /path/to/destination/folder
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)
        to_path = data.get('to_path', None)
        try:
            files = json.loads(files)
        except Exception as e:
            return response.fail("Tập tin không hợp lệ")

        # Validate to_path
        to_file_path = FileSystemPath(to_path)
        to_file_path.init_user_container(user_id).sync_path()
        if to_file_path.is_dir == False:
            return response.fail("Không thể thực hiện thao tác này (00101)")

        account, container = get_account_container(user_id)
        storage = SwiftStorage(account, container)

        # check quota
        size = 0
        for path in files:
            size += get_total_size(path, container, size)

        free_size = sizeof_fmt(container.max_quota - container.quota_count)
        copy_size = sizeof_fmt(size)

        if (container.quota_count + size) >= container.max_quota:
            msg = "Tài khoản của bạn không đủ dung lượng trống để thực hiện thao tác này. Dung lượng cần thiết: {} - Dung lượng trống: {}".format(
                copy_size, free_size)
            raise OutOfQuota(msg)
        list_file_fail = []
        for move_path in files:
            list_file_fail = copy_recursive(move_path, to_file_path, user_id, storage, container, list_file_fail)
        return response.success({"status": "success", "files_fail": list_file_fail})


def copy_recursive(path_file, to_path, user_id, storage, container, list_file_fail):
    move_path = FileSystemPath(path_file)
    move_path.init_user_container(user_id).sync_path()

    if move_path.path_data.is_dir == True:
        validate_to_path(path_file, to_path.fq_path)

    file_copy, result = move_path.copy(to_path, storage)

    if result == True:
        # Increase quota
        SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + file_copy.bytes)

        if file_copy.is_dir == True:
            sub_items = FileSystem.objects.filter(parent=move_path.path_data.id, container=container,
                                                  status=FileSystem.FILE_ACTIVE)
            for item in sub_items:
                copy_recursive(item.fq_path, FileSystemPath(file_copy.fq_path).init_user_container(user_id).sync_path(),
                               user_id, storage, container, list_file_fail)
    else:
        list_file_fail.append(file_copy)
    return list_file_fail


# V2

class MoveFolderCommandViewV2(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Move file')
    def post(self, request, *args, **kwargs):
        """
        Delete folder
        ---
        type :
            status:
                required: true
                type: string
                description: success

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "[/path1,/path2]"
              required: true
              type: string
              paramType: form

            - name: to_path
              description: /path/to/destination/folder
              required: true
              type: string
              paramType: form
            - name: action
              description: skip, override, rename, cancel
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)
        to_path = data.get('to_path', None)

        try:
            files = json.loads(files)
            if len(files) > 20:
                return response.fail("Array is too large")
        except:
            return response.fail("Files is not valid")

        action = data.get('action', None)
        if action != None:
            if action not in settings.ACTION_FILE_EXISTS:
                return response.fail("xác nhận không hợp lệ")
            elif action == 'cancel':
                return response.success({"status": "canceled"})

        # Validate to_path
        to_file_path = FileSystemPath(to_path)
        to_file_path.init_user_container(user_id).sync_path()

        if action == None:
            for move_path in files:
                if move_path in settings.LIST_UNCHANGABLE_PATHS:
                    continue
                move_path = FileSystemPath(move_path)
                move_path.init_user_container(user_id).sync_path()
                flag_exists = check_file_exists(to_file_path, move_path.path_data.file_name, move_path.container)
                if flag_exists:
                    return response.success({"status": "confirm"})

        # Move files
        for move_path in files:
            if move_path in settings.LIST_UNCHANGABLE_PATHS:
                continue
            move_path = FileSystemPath(move_path)
            move_path.init_user_container(user_id).sync_path()
            if to_file_path == move_path:
                raise ParseError("Can't move file to itself")
            #
            # if action == 'skip':
            #     flag_exists = check_file_exists(to_file_path, move_path.path_data.file_name, move_path.container)
            #     if flag_exists:
            #         continue

            move_path.move_v2(to_file_path, action)
        return response.success({"status": "success"})


class CopyFileCommandViewV2(APIView):
    """
    Copy folder, file command view
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Copy multiple folder / file
        ---

        type :
            status:
                required: true
                type: string
                description: success
            files_fail:
                required: true
                type: array
                description: ['/file.png']

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: files
              description: array with total len is 20 "['/path1','/path2']"
              required: true
              type: string
              paramType: form

            - name: to_path
              description: /path/to/destination/folder
              required: true
              type: string
              paramType: form

            - name: action
              description: skip, override, rename, cancel
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        is_xhr = data.get('is_xhr', None)
        files = data.get('files', None)
        to_path = data.get('to_path', None)

        try:
            files = json.loads(files)
        except Exception as e:
            return response.fail("Tập tin không hợp lệ")

        action = data.get('action', None)
        if action != None:
            if action not in settings.ACTION_FILE_EXISTS:
                return response.fail("xác nhận không hợp lệ")
            elif action == 'cancel':
                return response.success({"status": "canceled"})

        # Validate to_path
        to_file_path = FileSystemPath(to_path)
        to_file_path.init_user_container(user_id).sync_path()
        if to_file_path.is_dir == False:
            return response.fail("Không thể thực hiện thao tác này (00101)")

        account, container = get_account_container(user_id)
        storage = SwiftStorage(account, container)

        # check quota
        size = 0
        tmp_size = 0
        flag_exists = False
        for path in files:
            if path in settings.LIST_UNCHANGABLE_PATHS:
                continue
            move_path = FileSystemPath(path)
            move_path.init_user_container(user_id).sync_path()
            size += move_path.size()
            file_exists_tmp = move_path.exists(to_file_path)
            if file_exists_tmp == True:
                flag_exists = True

        free_size = sizeof_fmt(container.max_quota - container.quota_count)
        copy_size = sizeof_fmt(size)

        if (container.quota_count + size) >= container.max_quota:
            msg = "Tài khoản của bạn không đủ dung lượng trống để thực hiện thao tác này. Dung lượng cần thiết: {} - Dung lượng trống: {}".format(
                copy_size, free_size)
            raise OutOfQuota(msg)

        if flag_exists == True and action == None:
            return response.success({"status": "confirm"})

        flag_exists = []
        list_file_fail = []
        for move_path in files:
            if move_path in settings.LIST_UNCHANGABLE_PATHS:
                continue
            # move_path = FileSystemPath(move_path)
            # move_path.init_user_container(user_id).sync_path()
            # move_path.copy_v3(to_file_path, storage,  action)

            list_file_fail = copy_recursive_v2(move_path, to_file_path, user_id, storage, container, list_file_fail,
                                               action)
        return response.success({"status": "success", "files_fail": list_file_fail})


def copy_recursive_v2(path_file, to_path, user_id, storage, container, list_file_fail, action):
    move_path = FileSystemPath(path_file)
    move_path.init_user_container(user_id).sync_path()

    if move_path.path_data.is_dir == True:
        validate_to_path(path_file, to_path.fq_path)

    # skip when exists
    check_exists = check_file_exists(to_path, move_path.path_data.file_name, container)
    if action == 'skip' and check_exists == True and move_path.path_data.is_dir == False:
        return list_file_fail

    file_copy, result = move_path.copy_v2(to_path, storage, action)

    if result == True:
        # Increase quota
        SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + file_copy.bytes)

        if file_copy.is_dir == True:
            sub_items = FileSystem.objects.filter(parent=move_path.path_data.id, container=container,
                                                  status=FileSystem.FILE_ACTIVE)
            for item in sub_items:
                copy_recursive_v2(item.fq_path,
                                  FileSystemPath(file_copy.fq_path).init_user_container(user_id).sync_path(),
                                  user_id, storage, container, list_file_fail, action)
    else:
        list_file_fail.append(file_copy)
    return list_file_fail


def validate_to_path(move_path, to_path):
    split_move_path = move_path.split('/')
    split_to_path = to_path.split('/')
    if len(split_move_path) < len(split_to_path):
        for index, folder in enumerate(split_move_path):
            if split_move_path[index] == split_to_path[index] and split_move_path[index] != '':
                raise ParseError("Thư mục đích không được là thư mục con của thư mục nguồn")


def get_total_size(path_file, container, size):
    try:
        file_system = FileSystem.objects.get(fq_path=path_file, container=container, status=FileSystem.FILE_ACTIVE)
        if file_system.is_dir == False:
            size += file_system.bytes
        else:
            sub_items = FileSystem.objects.filter(parent=file_system, container=container,
                                                  status=FileSystem.FILE_ACTIVE)
            for item in sub_items:
                size = get_total_size(item.fq_path, container, size)
        return size
    except:
        return size


def get_total_size_v2(path_file, container, size, to_path, file_exists):
    try:
        print('counting ...', path_file)
        file_system = FileSystem.objects.get(fq_path=path_file, container=container, status=FileSystem.FILE_ACTIVE)

        if file_exists == False:
            file_exists = check_file_exists(to_path, file_system.file_name, container)

        print('get_total_size file_exists', file_exists)

        if file_system.is_dir == False:
            size += file_system.bytes
        else:
            sub_items = FileSystem.objects.filter(parent=file_system, container=container,
                                                  status=FileSystem.FILE_ACTIVE)
            for item in sub_items:
                size, file_exists = get_total_size_v2(item.fq_path, container, size, to_path, file_exists)
        return (size, file_exists)
    except:
        return (size, file_exists)


def get_account_container(user_id):
    try:
        account = SwiftAccount.objects.get(user_id=user_id)
        container = SwiftContainer.objects.get(account=account)
        return account, container
    except Exception as ex:
        logger.error(ex)
        raise ParseError("Internal Error")


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def md5(str):
    # settings.HASH_PATH_SALT
    m = hashlib.md5()
    m.update(str)
    return m.hexdigest()


def check_file_exists(to_path, file_name, container):
    to_fq_path = to_path.fq_path
    if to_path.is_root:
        new_path = to_fq_path + file_name
    else:
        new_path = to_fq_path + "/" + file_name

    try:
        FileSystem.objects.get(fq_path=new_path, container=container, is_dir=False)
        return True
    except:
        return False


class BrowseAlbumView(APIView):
    """
    Album View
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def put(self, request, *args, **kwargs):
        """
        Create album
        ---
        many: true

        serializer: block.serializers.api_document.AlbumSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: album_name
              description: album name
              required: true
              type: string
              paramType: form

            - name: album_type
              description: album type. Possible values is udef-mixed
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        album_name = data.get('album_name', '')
        album_type = data.get('album_type', Album.T_USER_DEFINED_MIXED)
        is_xhr = data.get('is_xhr', None)

        if not album_name:
            raise ParseError("Vui lòng nhập tên album cần tạo")

        if album_type and album_type not in (Album.T_USER_DEFINED_MIXED,):
            raise ParseError("Loại album không hợp lệ")
        elif not album_type:
            album_type = Album.T_USER_DEFINED_MIXED

        album_utils = AlbumUtils(user_id=user_id)
        user_album_object = album_utils.create(album_name=album_name, album_type=album_type)
        response_data = AlbumSerializer(user_album_object)
        return response.success(response_data.data)

    def get(self, request, *args, **kwargs):
        """
        Browse albums
        ---

        serializer: block.serializers.api_document.AlbumSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: query

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: query

            - name: limit
              description: limit  ( default is 20 )
              required: false
              type: string
              paramType: query

            - name: album_type
              description: Default empty will get all albums. Possible values auto-image, auto-video, udef-mixed
              required: false
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.GET
        page = data.get('page', '1')
        limit = data.get('limit', '20')
        album_type = data.get('album_type', None)
        is_xhr = data.get('is_xhr', None)

        try:
            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        album_utils = AlbumUtils(user_id=user_id)
        user_albums = album_utils.list(album_type=album_type)
        paginator = Paginator(user_albums, limit)
        browse_data = paginator.page(page)
        try:
            response_data = AlbumSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = AlbumSerializer([], many=True)

        return response.success({
            "total_files": paginator.count,
            "total_pages": paginator.num_pages,
            "current_page": page,
            "album_info": response_data.data
        })

    def post(self, request, *args, **kwargs):
        """
        Rename album
        ---

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: query

            - name: album_id
              description: album id
              required: true
              type: string
              paramType: query

            - name: new_album_name
              description: new name for this album
              required: true
              type: string
              paramType: query

            - name: force
              description: default is 0. It is 1 if user confirms to merge.
              required: false
              type: int
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        album_id = data.get('album_id', '')
        force = data.get('force', '0') == '1'
        new_album_name = data.get('new_album_name', '')
        is_xhr = data.get('is_xhr', None)

        if not album_id:
            raise ParseError("Vui lòng chọn album cần đổi tên")

        if not new_album_name:
            raise ParseError("Vui lòng nhập tên album mới")

        album_utils = AlbumUtils(user_id=user_id, album_id=album_id)
        user_album_object = album_utils.rename(new_album_name=new_album_name, force=force)
        response_data = AlbumSerializer(user_album_object)
        return response.success({"status": "success"})

    def delete(self, request, *args, **kwargs):
        """
        Delete albums
        ---

        serializer: block.serializers.api_document.AlbumSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: query

            - name: album_id
              description: album id
              required: true
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.GET
        album_id = data.get('album_id', '')
        is_xhr = data.get('is_xhr', None)

        if not album_id:
            raise ParseError("Vui lòng chọn album cần xoá")

        album_utils = AlbumUtils(user_id=user_id, album_id=album_id)
        album_utils.delete()
        return response.success({"status": "success"})


class ItemsAlbumView(APIView):
    """
    Album View
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def put(self, request, *args, **kwargs):
        """
        Add item to album
        ---

        serializer: block.serializers.api_document.AlbumSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: album_id
              description: album id
              required: true
              type: string
              paramType: form

            - name: files
              description: array file id "['fid:123','fid:456']"
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        files = data.get('files', None)
        album_id = data.get('album_id', '')
        is_xhr = data.get('is_xhr', None)

        if not album_id:
            raise ParseError("Vui lòng nhập chọn album")

        try:
            files = json.loads(files)
        except Exception as e:
            return response.fail("Tập tin không hợp lệ")

        album_utils = AlbumUtils(user_id=user_id, album_id=album_id)

        for file_id in files:
            try:
                album_utils.add_item_to_album(filesystem=file_id, check_permission=True)
            except:
                pass
        return response.success({"status": "success"})

    def delete(self, request, *args, **kwargs):
        """
        Remove item in album
        ---

        serializer: block.serializers.api_document.AlbumSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: query

            - name: album_id
              description: album id
              required: true
              type: string
              paramType: query

            - name: files
              description: array file id "['fid:123','fid:456']"
              required: true
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.GET
        files = data.get('files', None)
        album_id = data.get('album_id', '')
        is_xhr = data.get('is_xhr', None)

        if not album_id:
            raise ParseError("Vui lòng nhập chọn album")

        try:
            files = json.loads(files)
        except Exception as e:
            return response.fail("Tập tin không hợp lệ")

        album_utils = AlbumUtils(user_id=user_id, album_id=album_id)
        for file_id in files:
            try:
                album_utils.remove_item_from_album(filesystem=file_id, check_permission=True)
            except:
                pass

        return response.success({"status": "success"})

    def get(self, request, *args, **kwargs):
        """
        Browse albums
        ---

        serializer: block.serializers.api_document.AlbumSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: query

            - name: album_id
              description: album id
              required: true
              type: string
              paramType: query

            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: query

            - name: limit
              description: limit  ( default is 20 )
              required: false
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.GET
        page = data.get('page', '1')
        limit = data.get('limit', '20')
        album_id = data.get('album_id', None)
        is_xhr = data.get('is_xhr', None)

        try:
            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        album_utils = AlbumUtils(user_id=user_id, album_id=album_id)
        files = album_utils.list_items_in_album()
        paginator = Paginator(files, limit)
        browse_data = paginator.page(page)
        try:
            response_data = FileSystemSerializer(browse_data, many=True)
        except PageNotAnInteger:
            raise ParseError("Page is not integer")
        except EmptyPage:
            response_data = FileSystemSerializer([], many=True)

        return response.success({
            "total_files": paginator.count,
            "total_pages": paginator.num_pages,
            "current_page": page,
            "file_info": response_data.data
        })


class CoverAlbumView(APIView):
    """
    Album View
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def put(self, request, *args, **kwargs):
        """
        set cover for album
        ---

        type :
            status:
                required: true
                type: boolean
                description: 

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: album_id
              description: album id
              required: true
              type: string
              paramType: form

            - name: file_id
              description: id of the file
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_id = data.get('file_id', '')
        album_id = data.get('album_id', '')
        is_xhr = data.get('is_xhr', None)

        if not album_id:
            raise ParseError("Vui lòng nhập chọn album")

        album_utils = AlbumUtils(user_id=user_id, album_id=album_id)
        status = album_utils.set_cover(filesystem=file_id, check_permission=True)
        return response.success({"status": status})
