from datetime import datetime, timedelta

import logging

from django.core.paginator import Paginator
from rest_framework.exceptions import ParseError
from rest_framework.views import APIView

from block.authentication import TokenAuthentication
from block.models import FileSystem, SwiftAccount, SwiftContainer, SharingSession
from block.permissions import TokenIsAuthenticated
from block.serializers.sharing import SharingSessionSerializer
from block.serializers.swift import FileSystemSerializer, FileShareSerializer
from block.services.common import response
logger = logging.getLogger(__name__)

class ShareFolderView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request):
        """
        create sharing session
        ---

        serializer: block.serializers.sharing.SharingSessionSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header
            - name: path
              description: path for share
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            path = request.POST['path']
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Parameter missing")

        try:
            user_id = request.auth
            account = SwiftAccount.objects.get(user_id=user_id)
            container = SwiftContainer.objects.get(account=account)
            filesystem = FileSystem.objects.get(fq_path=path, is_dir=True, container=container)
        except Exception as e:
            logger.exception(e)
            raise ParseError("Folder not exists")

        expire_at = datetime.today() + timedelta(1)
        sharingSession = SharingSession(
            container_owner=container,
            file_path=path,
            expire_at=expire_at.strftime('%Y-%m-%d %H:%M:%S')
        )
        sharingSession.save()

        return response.success(SharingSessionSerializer(sharingSession).data)


class GetShareFolderView(APIView):
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        """
        get sharing session
        ---
        serializer: block.serializers.api_document.FileSystemDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: share_session
              description: id of session
              required: true
              type: string
              paramType: form
            - name: share_path
              description: path
              required: true
              type: string
              paramType: form
            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: form

            - name: limit
              description: limit  ( default is 20 )
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            share_session_id = request.POST['share_session']
            share_path = request.POST['share_path']
            page = request.POST.get('page', '1')
            limit = request.POST.get('limit', '20')
        except Exception as e:
            logger.exception(e)
            raise ParseError("Parameter missing")

        try:
            page, limit = int(page), int(limit)
            if page <= 0:
                page = 1
        except:
            raise ParseError("Page is not integer")

        try:
            sharingSession = SharingSession.objects.get(id=share_session_id)
            split_path = sharingSession.file_path.split('/')
            split_share_path = share_path.split('/')

            for index, path in enumerate(split_path):
                if path != split_share_path[index]:
                    raise Exception("")

            fileSystem = FileSystem.objects.get(container_id=sharingSession.container_owner_id, fq_path=share_path, is_dir=True)

        except Exception as e:
            logger.exception(e)
            raise ParseError("Session not exists")

        list_file = FileSystem.objects.order_by("-is_dir", "-user_last_modified").filter(parent=fileSystem)
        paginator = Paginator(list_file, limit)
        try:
            file_data = paginator.page(page)
            response_data = FileShareSerializer(file_data, many=True)
        except Exception as e:
            logger.exception(e)
            response_data = FileShareSerializer([], many=True)

        return response.success({
            "total_files": paginator.count,
            "total_pages": paginator.num_pages,
            "current_page": page,
            "file_info": response_data.data
        })