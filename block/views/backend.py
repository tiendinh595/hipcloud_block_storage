#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from block.services.common import response
from django.apps import apps
get_model = apps.get_model
from block.models.authenticate import AccessToken
from block.models.swift import SwiftContainer
from rest_framework.exceptions import PermissionDenied
from block.serializers.swift import ContainerSerializer
from django.conf import settings
from jose import jwt
import logging

logger = logging.getLogger(__name__)


class ContainerUpdateView(APIView):

    def verify_token(self, payload):
        try:
            json_profile = jwt.decode(payload, settings.JWT_PRIVATE_SIGNATURE , algorithms=['HS256'])
            return json_profile
        except Exception as ex:
            logger.error("Invalid token : " + str(ex) + " " + payload)
            raise PermissionDenied("Invalid token")


    def sync_container(self, json_profile):
        try:
            container = SwiftContainer.objects.get(account__user_id=json_profile['iss'])
            container.max_quota = json_profile['context']['container']['max_quota']
            container.save()
            return container
        except Exception as ex:
            logger.error("Container not found " + str(ex))
            raise PermissionDenied("Invalid token")



    def post(self, request, *args, **kwargs):
        """
        [BACKEND ONLY] Update container info : quota

        Receive: jwt token with embed json data

        {
            "iss": "user_id",
            "iat": 1300819370,
            "exp": 1300819380,
            "mth": "fb/gg/em/mo",
            "tok": "token",
            "sub": "subject_id",
            "context": {
                "container": {
                    "max_quota": 10000000000,
                }
            },
        }

        ---
        parameters:
            - name: payload
              description: token string
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"message" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        data = request.POST
        payload = data.get('payload')
        if not payload:
            raise PermissionDenied("Unauthorized")
        # Verify token & get payload data
        json_data = self.verify_token(payload)
        # Insert to db
        container = self.sync_container(json_data)
        serialized = ContainerSerializer(container)
        return response.success(serialized.data)
