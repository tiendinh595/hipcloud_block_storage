#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import os
import re
import threading
from collections import OrderedDict

import requests
from django.utils import timezone
from django.conf import settings
from googleapiclient.http import MediaIoBaseDownload
from oauth2client.client import OAuth2WebServerFlow
from rest_framework.decorators import renderer_classes
from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.views import APIView

from block.models import SyncSession
from block.serializers.sync import SyncSessionSerializer, SyncStatusSerializer
from block.services.common import response
from rest_framework.exceptions import PermissionDenied , ParseError
from block.authentication import TokenAuthentication, TokenAuthenticationGoogleRedirect
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftContainer
from apiclient import discovery
from oauth2client import client
import logging
import httplib2
import json

from block.services.file import FileSystemPath
from block.tasks.sync_data_v2 import gdrive_check_quota_logic, sync_gdrive_logic, gdrive_check_quota, sync_gdrive, \
    sync_gdrive_parse_files, ggoauth_refresh_token

logger = logging.getLogger(__name__)


class GGDriveAuthLinkView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Get google oauth token
        ---

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_type
              description: Type sync (1 Google Drive; 2 Google Photos)
              required: true
              type: string
              paramType: form

            - name: resync
              description: confirm delete all files (only stop status)
              required: false
              type: boolean
              paramType: form

            - name: redirect_uri
              description: Authorized redirect URIs, empty is "/block/sync/v2/auth_token" link
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"status" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_type = data.get('sync_type', 1)
        resync = data.get('resync', False)
        redirect_uri = data.get('redirect_uri', False)
        if redirect_uri is False:
            redirect_uri = settings.GOOGLE_OAUTH2_REDIRECT_URI

        flow = OAuth2WebServerFlow(client_id=settings.GOOGLE_OAUTH2_CLIENT_ID,
                                   client_secret=settings.GOOGLE_OAUTH2_CLIENT_SECRET,
                                   scope=settings.GOOGLE_OAUTH2_SCOPES,
                                   redirect_uri=redirect_uri,
                                   state=str(sync_type)+'/'+request.META.get('HTTP_AUTHORIZATION')+'/'+str(resync).lower(),
                                   prompt='consent')
        auth_uri = flow.step1_get_authorize_url()

        return response.success({"status":-1, "auth_uri":str(auth_uri)})

from rest_framework.response import Response


class GGDriveGetTokenView(APIView):

    authentication_classes = (TokenAuthenticationGoogleRedirect,)
    permission_classes = (TokenIsAuthenticated,)
    renderer_classes = [StaticHTMLRenderer]

    def get(self, request, *args, **kwargs):
        data = request.GET
        user_id = request.auth
        code = data.get('code')
        state = data.get('state').split("/")
        sync_type = int(state[0])
        resync = str(state[-1])
        data_html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Import files to Upbox</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"></head><body><a href="upbox://{}" style="display: block;width: 120px;height: 40px;line-height: 40px;background: #309ffd;background: -moz-linear-gradient(left,  #309ffd 0%, #067bff 100%);background: -webkit-linear-gradient(left,  #309ffd 0%,#067bff 100%);background: linear-gradient(to right,  #309ffd 0%,#067bff 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#309ffd\', endColorstr=\'#067bff\',GradientType=1 );color: #ffffff;margin: 0 auto;text-align: center;text-decoration: none; border-radius: 25px;font-size: 18px;">Accept</a></body></html>'
        logger.info('code: '+code+' - sync_type: '+str(sync_type)+' - resync: '+resync)
        try:

            container = SwiftContainer.objects.get(account__user_id=user_id)

            flow = OAuth2WebServerFlow(client_id=settings.GOOGLE_OAUTH2_CLIENT_ID,
                                       client_secret=settings.GOOGLE_OAUTH2_CLIENT_SECRET,
                                       scope=settings.GOOGLE_OAUTH2_SCOPES,
                                       redirect_uri=settings.GOOGLE_OAUTH2_REDIRECT_URI,
                                       prompt='consent')
            credentials = flow.step2_exchange(code)

            logger.info('access_token: ' + credentials.access_token)
            logger.info('refresh_token: '+credentials.refresh_token)

            # Check tooken
            try:
                req = ggoauth_refresh_token(credentials.refresh_token)
                google_token = req['access_token']
                google_refresh_token = credentials.refresh_token
                credentials = client.AccessTokenCredentials(google_token, 'my-user-agent/1.0')
                http = httplib2.Http()
                http = credentials.authorize(http)
                gphotos_user = discovery.build('oauth2', version='v2', http=http)
                user_info = gphotos_user.userinfo().get().execute()
                print 'email: ' + user_info['email']
            except Exception as ex:
                logger.error(ex)
                data = data_html.format('')
                return Response(data)

            # check SyncSession
            try:
                sync_session = SyncSession.objects.get(container__id=container.id,
                                                       sync_type=sync_type,
                                                       email_sync=user_info['email'])
            except:
                sync_session = None

            if sync_session != None:
                if sync_session.status == SyncSession.CHECK_QUOTA or (sync_session.status == SyncSession.CANCEL and str(
                        resync).lower() != 'true') or sync_session.status == SyncSession.UNLINKED or sync_session.status == SyncSession.LINKED or (sync_session.status == SyncSession.SUCCESS and str(resync).lower() != 'true'):
                    data = data_html.format(sync_session.id)
                    return Response(data)

            # update or create SyncSession
            try:
                if sync_session != None:
                    sync_session.token = google_token
                    sync_session.refresh_token = google_refresh_token
                    if sync_session.status == SyncSession.CANCEL or sync_session.status == SyncSession.SUCCESS:
                        sync_session.status = SyncSession.GET_OAUTH
                    sync_session.save()
                else:
                    sync_session = SyncSession(
                        container=container,
                        token=google_token,
                        refresh_token=google_refresh_token,
                        status=SyncSession.GET_OAUTH,
                        sync_type=sync_type,
                        total_size=0,
                        email_sync=user_info['email']
                    )
                sync_session.save()
                logger.info("sync_session.id: " + sync_session.id)
            except Exception as ex:
                logger.error(ex)
                data = data_html.format('')
                return Response(data)
            data = data_html.format(sync_session.id)
            return Response(data)
            # return response.success({"status":"success", "sync_session_id": sync_session.id})
        except Exception as ex:
            logger.error(ex)
            data = data_html.format('')
            return Response(data)

class GGDriveCheckQuotaView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Check quota for sync google drive
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        # check SyncSession
        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(id = sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_DRIVE)
        except:
            return response.fail("Session not found")

        if sync_session.status == SyncSession.CHECK_QUOTA or sync_session.status == SyncSession.UNLINKED or sync_session.status == SyncSession.LINKED or sync_session.status == SyncSession.SUCCESS:
            response_data = SyncStatusSerializer([sync_session], many=True)
            return response.success(response_data.data[0])

        if sync_session.status == SyncSession.CANCEL or sync_session.status == SyncSession.SUCCESS:
            try:
                file_path = FileSystemPath(SyncSession.FOLDER_GOOGLE_DRIVE+"/"+sync_session.email_sync)
                file_path.init_user_container(user_id).sync_path()
                file_path.delete()
                file_path.delete_permanent()
            except Exception as ex:
                logger.error(ex)
                pass

        # Check tooken
        req = ggoauth_refresh_token(sync_session.refresh_token)
        if 'access_token' not in req:
            return response.fail("Refresh token is not valid")

        google_token = req['access_token']

        # update or create SyncSession
        try:
            sync_session.status = SyncSession.CHECK_QUOTA
            sync_session.token = google_token
            sync_session.save()
            logger.info("sync_session.id: " + sync_session.id)
        except Exception as ex:
            logger.error(ex)
            return response.fail("Session not updated")

        # test logic check quota
        # threading.Thread(
        #     target=gdrive_check_quota_logic,
        #     args=(sync_session.id, container.id, request.META.get('HTTP_AUTHORIZATION'), user_id)
        # ).start()
        # return response.success({"status": "success", "sync_session_id": sync_session.id})

        # Spawn sync tasks
        job = gdrive_check_quota.apply_async(([sync_session.id, container.id, request.META.get('HTTP_AUTHORIZATION'), user_id]))
        # task = AsyncResult(job.id)
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class GGDriveSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Start sync google drive
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_DRIVE)
        except:
            return response.fail("Session not found")

        if sync_session.status != SyncSession.UNLINKED:
            response_data = SyncStatusSerializer([sync_session], many=True)
            return response.success(response_data.data[0])

        # update sync status
        sync_session.status = SyncSession.LINKED
        sync_session.save()

        # test logic sync
        # threading.Thread(
        #     target=sync_gdrive_logic,
        #     args=(sync_session.id, sync_session.container.id, request.META.get('HTTP_AUTHORIZATION'), user_id)
        # ).start()
        # return response.success({"status": "success", "sync_session_id": sync_session.id})

        # Spawn sync tasks
        job = sync_gdrive.apply_async(([sync_session.id, sync_session.container.id, request.META.get('HTTP_AUTHORIZATION'), user_id]))
        # task = AsyncResult(job.id)
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class GGDriveStopSyncCommandView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Stop sync google drive
        ---

        many: false

        serializer: block.serializers.api_document.SyncStatusDocumentSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_DRIVE)
        except:
            return response.fail("Session not found")

        if sync_session.status != SyncSession.LINKED:
            return response.fail("Sync not running")

        # update stop status
        sync_session.status = SyncSession.CANCEL
        sync_session.save()
        return response.success({"status": -1, "email":sync_session.email_sync, "sync_session_id": sync_session.id})

class GGDriveSyncCommandStatusView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    # @silk_profile(name='Browse file')
    def post(self, request, *args, **kwargs):
        """
        Get status sync google drive
        ---

        serializer: block.serializers.sync.SyncSessionSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: sync_session_id
              description: Id session task
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        sync_session_id = data.get("sync_session_id", None)

        try:
            container = SwiftContainer.objects.get(account__user_id=user_id)
            sync_session = SyncSession.objects.get(pk=sync_session_id, container=container, sync_type=SyncSession.SYNC_GOOGLE_DRIVE)
        except:
            return response.fail("Session not found")

        response_data = SyncSessionSerializer([sync_session], many=True)
        return response.success(response_data.data[0])