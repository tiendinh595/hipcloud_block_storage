#!/usr/bin/env python
# -*- coding: utf-8 -*-
from block.serializers.downloader import FileSystemDownloaderSerializer, FileSystemShareDownloaderSerializer
from block.serializers.swift import FileSystemSerializer
from block.models.swift import FileSystem

from rest_framework.exceptions import PermissionDenied , ParseError

from block.authentication import TokenAuthentication
from block.permissions import TokenIsAuthenticated
from block.models.swift import SwiftContainer
from block.services.common import response
from rest_framework.views import APIView
import logging

logger = logging.getLogger(__name__)

class DownloadFileUrlView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)


    def post(self, request, *args, **kwargs):
        """
        Get download file url
        ---

        serializer: block.serializers.api_document.FileDocumentSerializer

        parameters_strategy: replace

        many: false

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: file_id
              description: id of the file
              required: false
              type: string
              paramType: form

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "detail" : "Bad Request – Your request sucks"}'
            - code: 401
              message: '{"error" : 401 , "detail" : "Unauthorized – Your API key is wrong"}'
            - code: 403
              message: '{"error" : 403 , "detail" : "Forbidden – The kitten requested is hidden for administrators only"}'
            - code: 405
              message: '{"error" : 405 , "detail" : "Method Not Allowed – You tried to access a kitten with an invalid method"}'
            - code: 410
              message: '{"error" : 410 , "detail" : "Gone – The kitten requested has been removed from our servers"}'
            - code: 429
              message: '{"error" : 429 , "detail" : "Too Many Requests – You’re requesting too many kittens! Slow down!"}'
            - code: 500
              message: '{"error" : 500 , "detail" : "Internal Error – We had a problem with our server. Try again later."}'
            - code: 503
              message: 'HTML Service Unavailable – We’re temporarily offline for maintenance. Please try again later.'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_id = data.get('file_id', None)
        is_xhr = data.get('is_xhr', None)
        try:
            container = SwiftContainer.objects.get(account__user_id = user_id)
            file_to_share = FileSystem.objects.get(id = file_id, container = container,status = FileSystem.FILE_ACTIVE)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("File not found")
        response_data = FileSystemDownloaderSerializer(file_to_share)
        return response.success(response_data.data)


class ShareFileUrlView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)


    def post(self, request, *args, **kwargs):
        """
        Get share file url
        ---

        serializer: block.serializers.api_document.FileDocumentSerializer

        parameters_strategy: replace

        many: false

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: file_id
              description: id of the file
              required: false
              type: string
              paramType: form

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "detail" : "Bad Request – Your request sucks"}'
            - code: 401
              message: '{"error" : 401 , "detail" : "Unauthorized – Your API key is wrong"}'
            - code: 403
              message: '{"error" : 403 , "detail" : "Forbidden – The kitten requested is hidden for administrators only"}'
            - code: 405
              message: '{"error" : 405 , "detail" : "Method Not Allowed – You tried to access a kitten with an invalid method"}'
            - code: 410
              message: '{"error" : 410 , "detail" : "Gone – The kitten requested has been removed from our servers"}'
            - code: 429
              message: '{"error" : 429 , "detail" : "Too Many Requests – You’re requesting too many kittens! Slow down!"}'
            - code: 500
              message: '{"error" : 500 , "detail" : "Internal Error – We had a problem with our server. Try again later."}'
            - code: 503
              message: 'HTML Service Unavailable – We’re temporarily offline for maintenance. Please try again later.'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        data = request.POST
        file_id = data.get('file_id', None)
        is_xhr = data.get('is_xhr', None)
        try:
            container = SwiftContainer.objects.get(account__user_id = user_id)
            file_to_share = FileSystem.objects.get(id = file_id, container = container,
                                                   status = FileSystem.FILE_ACTIVE)
            response_data = FileSystemShareDownloaderSerializer(file_to_share)
            return response.success(response_data.data)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("File not found")
