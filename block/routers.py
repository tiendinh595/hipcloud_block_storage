

class BlockRouter(object):

    def route_for_task(self, task, args=None, kwargs=None):
        if task == 'block.tasks.generate_preview_pdf':
            return {'exchange': 'preview',
                    'exchange_type': 'topic',
                    'routing_key': 'preview.generate.pdfs'}
        elif task == 'block.tasks.generate_preview_word':
            return {'exchange': 'preview',
                    'exchange_type': 'topic',
                    'routing_key': 'preview.generate.words'}
        elif task == 'block.tasks.generate_preview_excel':
            return {'exchange': 'preview',
                    'exchange_type': 'topic',
                    'routing_key': 'preview.generate.excels'}
        elif task == 'block.tasks.generate_preview_video':
            return {'exchange': 'preview',
                    'exchange_type': 'topic',
                    'routing_key': 'preview.generate.videos'}
        elif task == 'block.tasks.generate_preview_image':
            return {'exchange': 'preview',
                    'exchange_type': 'topic',
                    'routing_key': 'preview.generate.images'}
        elif task == 'block.tasks.upload_file':
            return {'exchange': 'swift',
                    'exchange_type': 'topic',
                    'routing_key': 'swift.upload.files'}
        return None
