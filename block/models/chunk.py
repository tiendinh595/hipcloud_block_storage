#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from block.models.swift import FileSystem
from block.models.common import BaseModel
import uuid
import logging

logger = logging.getLogger(__name__)

def generate_upload_id():
    return "upid" + uuid.uuid4().hex

def generate_chunk_id():
    return "chk" + uuid.uuid4().hex

class UploadSession(BaseModel):
    """
        Base chunk upload model. This model is abstract (doesn't create a table in the database).
        Inherit from this model to implement your own.
    """
    INIT = 0
    FAILED = 1
    UPLOADING = 2
    RECEIVED = 3
    COMPLETED = 4
    STATUS_TYPES = (
        (COMPLETED, 4),
        (RECEIVED, 3),
        (UPLOADING, 2),
        (FAILED, 1),
        (INIT, 0)
    )

    id = models.CharField(max_length=54, primary_key=True, editable=False, default=generate_upload_id)
    user_id = models.CharField(max_length=40, null= False, blank=False , db_index=True)
    file_name = models.CharField(max_length=255)
    filesystem = models.ForeignKey(FileSystem, blank=True, null=True, db_constraint=False) # Indicate update newer version for the file
    destination = models.TextField()
    current_chunk = models.IntegerField(null=True, blank=True, default=0)
    total_chunks = models.IntegerField(null=True, blank=True)
    reported_total_size = models.BigIntegerField(default=0)
    expiry_at = models.DateTimeField(blank=True, null=True)
    user_ip = models.CharField(max_length=40, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_TYPES,null=True, blank=True, default=1)

    def __eq__(self, other):
        if other.file_name != self.file_name:
            logger.error("File name is not match")

        return isinstance(other, self.__class__) \
               and other.reported_total_size == self.reported_total_size \
               and other.total_chunks == self.total_chunks \
               and other.file_name == self.file_name \
               and other.destination == self.destination

    def __ne__(self, other):
        if other.file_name != self.file_name:
            logger.error("File name is not match")

        return not isinstance(other, self.__class__) \
               or other.reported_total_size != self.reported_total_size \
               or other.total_chunks != self.total_chunks \
               or other.file_name != self.file_name \
               or other.destination != self.destination

    def __unicode__(self):
        return '{id} | {user_id} | {file_name} | FS : {filesystem} | DES : {destination}' \
               ' | TC : {total_chunks} | SZ : {reported_total_size} | {expiry_at} | {status}'.format(id=self.id,file_name=self.file_name,status=self.status,
                                                                                           destination=self.destination,total_chunks=self.total_chunks,
                                                                                           filesystem=self.filesystem,user_id=self.user_id,
                                                                                           reported_total_size=self.reported_total_size,
                                                                                           expiry_at=self.expiry_at)

    class Meta:
        db_table = 'upload_session'

class UploadProgress(BaseModel):
    """
        Uploading progress by each chunk
    """
    INIT = 0
    FAILED = 1
    UPLOADING = 2
    RECEIVED = 3
    COMPLETED = 4
    STATUS_TYPES = (
        (INIT, 0),
        (FAILED, 1),
        (UPLOADING, 2),
        (RECEIVED, 3),
        (COMPLETED, 4)
    )

    id = models.CharField(max_length=54, primary_key=True, editable=False, default=generate_chunk_id)
    local_file = models.FileField(max_length=255, null=True, blank=True)
    instance_id = models.CharField(max_length=40, null=True, blank=True)
    session = models.ForeignKey(UploadSession, blank=True, null=True, db_index=True, db_constraint=False)
    chunk = models.IntegerField(blank=True, null=True)
    file_hash = models.CharField(max_length=40, blank=True, null=True)  # Hash of file
    offset = models.BigIntegerField(default = 0)
    completed_at = models.DateTimeField(null=True, blank=True)
    user_ip = models.CharField(max_length=40, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_TYPES, null=True, blank=True, default=1)

    class Meta:
        db_table = 'upload_progress'
