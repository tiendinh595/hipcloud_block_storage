#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
import uuid

def generate_container_id():
    return "upbox_"+str(uuid.uuid4().hex)[6:]

def generate_id():
    return str(uuid.uuid4().hex)

class BaseModel(models.Model):

    created_at = models.DateTimeField(blank=True, null = True, auto_now_add = True)
    updated_at = models.DateTimeField(blank=True, null = True, auto_now = True)

    class Meta:
        abstract = True
