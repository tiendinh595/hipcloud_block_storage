#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import hashlib
from common import generate_id , BaseModel
from block.models.swift import SwiftContainer
from django.db import models

class DropboxAccount(BaseModel):
    """
        Store dropbox account
        Who linked with this drobox account ?
        What dropbox account info about ?
    """
    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    user_id = models.CharField(max_length=40, null=False, blank=False)
    account_id = models.CharField(max_length=45, null=True, blank=True)
    display_name = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=40, null=True, blank=True)
    email_verified = models.BooleanField(default=False)
    locale = models.CharField(max_length=40, null=True, blank=True)
    referral_link = models.CharField(max_length=40, null=True, blank=True)
    country = models.CharField(max_length=40, null=True, blank=True)
    team = models.CharField(max_length=64, null=True, blank=True)
    team_member_id = models.CharField(max_length=64, null=True, blank=True)
    disabled = models.BooleanField(default=False)

    class Meta:
        db_table = 'block_dropbox_account'


class DropboxSync(BaseModel):
    """
        Sync file from user dropbox
        What is the current sync session token ?
        What container to save to ?
    """
    LINKED = 1
    UNLINKED = 0
    SYNC_STATUS = (
        (LINKED, 1),
        (UNLINKED, 0)
    )

    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    user_id = models.CharField(max_length=40, null=True, blank=True)
    dropbox_account = models.ForeignKey(DropboxAccount, null=True, blank=True, db_constraint=False)
    container = models.ForeignKey(SwiftContainer, null=True, blank=True, db_constraint=False)
    dropbox_token = models.CharField(max_length=65, null=True, blank=True)
    status = models.IntegerField(null=True, blank=True, choices=SYNC_STATUS, default=UNLINKED)

    class Meta:
        db_table = 'block_dropbox_sync'

class DropboxSyncCommand(BaseModel):
    """
        Sync file from user dropbox
        What directory want to sync to ?
        What directory with to sync ?
    """
    SYNCED = 2
    SYNCING = 1
    INIT = 0
    SYNC_COMMAND_STATUS = (
        (SYNCED, 2),
        (SYNCING, 1),
        (INIT, 0)
    )

    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    container = models.ForeignKey(SwiftContainer, null=True, blank=True, db_constraint=False)
    paths_to_sync = models.TextField(null=True, blank=True)
    save_path = models.TextField(null=True, blank=True)
    status = models.IntegerField(null=True, blank=True, choices=SYNC_COMMAND_STATUS, default=INIT)

    class Meta:
        db_table = 'block_dropbox_command_sync'





