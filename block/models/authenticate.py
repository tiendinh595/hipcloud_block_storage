#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from common import BaseModel
import uuid

def generate_access_token():
    return "btk:" + uuid.uuid4().hex

class AccessToken(models.Model):
    """
        Base class access token storing for each user
    """
    id = models.CharField(max_length=65, primary_key=True)
    user_id = models.CharField(max_length=36, null= False, blank=False , db_index=True)
    type = models.CharField(max_length=4, blank=True, null=True)
    subject_id = models.CharField(max_length=40, blank=True, null=True)
    last_request_at = models.DateTimeField(blank=True, null=True, auto_now=True)
    issued_at = models.IntegerField(blank=True, null=True, default = 0)
    expiry_at = models.IntegerField(blank=True, null=True, default = 0)

    class Meta:
        db_table = "block_access_token"
