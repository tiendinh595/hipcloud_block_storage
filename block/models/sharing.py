#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import hashlib

from block.models import FileSystem, SwiftContainer
from common import generate_id , BaseModel
from django.db import models
from django.utils import timezone
from django.conf import settings
import uuid

class SharingSession(BaseModel):
    """
        Sharing session
    """
    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    container_owner = models.ForeignKey(SwiftContainer, blank=True, null=True)
    file_path = models.TextField()
    expire_at = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(null=True, blank=True, default=1)

    class Meta:
        db_table = 'block_sharing_session'
        index_together = (
            ('container_owner',),
        )