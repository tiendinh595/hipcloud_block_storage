import os
import hashlib
from common import generate_id , BaseModel
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.conf import settings
import uuid


def generate_file_id():
    return 'fid:'+ generate_id()

class UserActivity(models.Model):
    """
        User's Activity
    """

    id = models.CharField(max_length=40, primary_key=True, default=generate_file_id)
    type = models.CharField(max_length=10, blank=True, null=True)
    user_id = models.CharField(max_length=40, null=False, blank=False)
    affected_user_id = models.CharField(max_length=40, null=False, blank=False)
    subject = models.CharField(max_length=40, null=True, blank=True)
    subject_params = models.CharField(max_length=4000, null=True, blank=True)
    message = models.CharField(max_length=40, null=True, blank=True)
    message_params = models.CharField(max_length=4000, null=True, blank=True)
    link = models.CharField(max_length=4000, null=True, blank=True)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)

    class Meta:
        db_table = 'block_activity'
