from django.db import models

from block.models.swift import SwiftContainer
from common import BaseModel, generate_id


class SyncSession(BaseModel):
    """
        Session sync file from user
    """
    SYNC_DROPBOX = 0
    SYNC_GOOGLE_DRIVE = 1
    SYNC_GOOGLE_PHOTOS = 2
    FOLDER_IMPORT = 'Import Form'
    FOLDER_DROPBOX = FOLDER_IMPORT+'/Dropbox'
    FOLDER_GOOGLE_DRIVE = FOLDER_IMPORT+'/Google Drive'
    FOLDER_GOOGLE_PHOTOS = FOLDER_IMPORT='/Google Photos'
    SYNC_TYPE = (
        (SYNC_DROPBOX, 'Dropbox'),
        (SYNC_GOOGLE_DRIVE, 'Google Drive'),
        (SYNC_GOOGLE_PHOTOS, 'Google Photos'),
    )
    ERROR = 8
    CANCEL = 7
    GET_OAUTH = 6
    QUOTA_EXCEED = 5
    TOKEN_INVALID = 4
    SUCCESS = 3
    CHECK_QUOTA = 2
    LINKED = 1
    UNLINKED = 0
    DEFAULT = -1
    SYNC_STATUS = (
        (ERROR, 'Sync error'),
        (CANCEL, 'Sync stop'),
        (GET_OAUTH, 'Get Google Oauth'),
        (QUOTA_EXCEED, 'Quota exceed'),
        (TOKEN_INVALID, 'Token invalid'),
        (SUCCESS, 'Sync success'),
        (CHECK_QUOTA, 'Check quota process'),
        (LINKED, 'Sync process'),
        (UNLINKED, 'Pending'),
        (DEFAULT, 'Default')
    )

    CHECK_QUOTA_PENDING = 'pending'
    CHECK_QUOTA_SUCCESS = 'success'
    CHECK_QUOTA_FAILURE = 'failure'

    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    container = models.ForeignKey(SwiftContainer, null=True, blank=True, db_constraint=False)
    email_sync = models.CharField(max_length=200, null=True, blank=True)
    token = models.CharField(max_length=129, null=True, blank=True)
    refresh_token = models.CharField(max_length=200, null=True, blank=True)
    expires_in = models.IntegerField(null=True, blank=True)
    total_size = models.BigIntegerField(null=True, blank=True, default=0)
    sync_type = models.IntegerField(choices=SYNC_TYPE)
    status = models.IntegerField(null=True, blank=True, choices=SYNC_STATUS, default=UNLINKED)
    sync_value = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'block_sync_session'