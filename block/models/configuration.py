#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import hashlib
from common import generate_id , BaseModel
from django.db import models
from django.utils import timezone
from django.conf import settings
import uuid


class SettingConfiguration(BaseModel):
    """
        Configuration for block application
    """
    INT_TYPE = 'int'
    STR_TYPE = 'str'
    JSON_TYPE = 'json'

    VALUE_TYPE_CHOICE = (
        ('int',INT_TYPE),
        ('str', STR_TYPE),
        ('json', JSON_TYPE),
    )

    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    app_name = models.CharField(max_length=40, null=False, blank=False)
    setting_name = models.CharField(max_length=40, null=False, blank=False, db_index=True)
    setting_value = models.TextField(blank=True, null=True)
    value_type = models.CharField(max_length=4,blank=True, null=True, choices=VALUE_TYPE_CHOICE)
    status = models.IntegerField(null=True, blank=True, default=1)

    class Meta:
        db_table = 'block_settings'
