#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import hashlib
from jsonfield import JSONField
from common import generate_id , BaseModel, generate_container_id
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.conf import settings
import uuid
import string
import random


def generate_file_id():
    return 'fid:'+ generate_id()

def generate_album_id():
    return 'aid:'+ generate_id()

def pw_gen():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(32))

class SwiftProject(BaseModel):
    """
        Swift project
    """
    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    project_id = models.CharField(max_length=40, blank=False, null=False, db_index=True)
    project_name = models.CharField(max_length=40, null=False, blank=False)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'block_swift_project'

class SwiftDomain(BaseModel):
    """
        Swift domain id
    """
    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    domain_id = models.CharField(max_length=40, blank=False, null=False, db_index=True)
    name = models.CharField(max_length=40, null=False, blank=False)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'block_swift_domain'

class SwiftAccount(BaseModel):
    """
        Internal mapping between swift key and user id
        Inherthroughit from this model to implement your own.
    """

    NOT_FOUND = 0
    ACTIVE = 1
    DEACTIVATE = 2
    STATUS_TYPES = (
        (NOT_FOUND, 0),
        (ACTIVE, 1),
        (DEACTIVATE, 2)
    )

    id = models.CharField(max_length = 40, primary_key=True, default = generate_id)
    user_id = models.CharField(max_length = 40, null  = False, blank = False , db_index = True)
    username = models.CharField(max_length = 36, null = False, blank = False , db_index = True)
    password = models.CharField(max_length = 150, null = False, blank = False, default = pw_gen)
    access_key = models.CharField(max_length = 34, null = True, blank = True)
    secret_key = models.CharField(max_length=34, null=True, blank=True)
    description = models.CharField(max_length = 100)
    project_name = models.CharField(max_length=40, null=True, blank=True)
    project_id = models.CharField(max_length=40, blank=True, null=True)
    domain_id = models.CharField(max_length=40, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_TYPES, null = True, blank = True, default = ACTIVE)

    class Meta:
        db_table = 'block_swift_account'

# Maintain container quota
# Maintain current quota status
# Info for connecting to container
class SwiftContainer(BaseModel):
    """
        Container name mapping with swift account
    """
    DEFAULT = 0
    SWIFT_RADOSGW = 1
    S3 = 2
    CONTAINER_TYPES = (
        (DEFAULT, 0),
        (SWIFT_RADOSGW, 1),
        (S3, 2)
    )

    ACTIVE = 1
    DEACTIVATE = 0
    CONTAINER_STATUS = (
        (ACTIVE, 1),
        (DEACTIVATE, 0)
    )

    id = models.CharField(max_length=40, primary_key=True, default=generate_container_id)
    account = models.ForeignKey(SwiftAccount, blank=True, null=True, db_constraint=False)
    type = models.IntegerField(choices=CONTAINER_TYPES, blank=True, null=True, default=DEFAULT)
    name = models.CharField(max_length=40, null=False, blank=False, unique=True)
    path = models.CharField(max_length=50, null=True, blank=True)
    temp_url_secret_key_1 = models.CharField(max_length=100, null=True, blank=True, default=pw_gen)
    temp_url_secret_key_2 = models.CharField(max_length=100, null=True, blank=True, default=pw_gen)
    max_quota = models.BigIntegerField(null=True, blank=True, default=settings.DEFAULT_QUOTA_CONTAINER)
    quota_count = models.BigIntegerField(null=True, blank=True, default=0)
    status = models.IntegerField(choices=CONTAINER_STATUS, null=True, blank=True, default=1)

    class Meta:
        db_table = 'block_swift_container'

# Maintain structural filesystem
# Maintain uniqueness of filename
# File type detection
# Url access
class FileSystem(BaseModel):
    """
        Contain user filesystem storage
        Folders
        Files
        Default is /
        Disallow \ / : ? * " |
    """
    ALBUM_FOLDER_TYPE = 5
    SHARED_FOLDER_TYPE = 4
    TEAM_FOLDER_TYPE = 3
    FOLDER_TYPE = 2
    FILE_TYPE = 1
    # Permission
    PERMISSION_READ = 1
    PERMISSION_UPDATE = 2
    PERMISSION_CREATE = 4
    PERMISSION_DELETE = 8
    PERMISSION_SHARE = 16
    PERMISSION_ALL = 31

    PERMISSION_TYPES = (
        (PERMISSION_READ, 1),
        (PERMISSION_UPDATE, 2),
        (PERMISSION_CREATE, 4),
        (PERMISSION_DELETE, 8),
        (PERMISSION_SHARE, 16),
        (PERMISSION_ALL, 31),
    )

    FILE_SYNC_FAIL = 6
    FILE_SYNCING = 5
    FILE_CLEARED = 4
    FILE_DELETED = 3
    FILE_ACTIVE = 2
    FILE_UPLOADING = 1
    FILE_INIT = 0
    FILE_STATUS = (
        (FILE_DELETED,3),
        (FILE_ACTIVE, 2),
        (FILE_UPLOADING, 1),
        (FILE_INIT, 0)
    )

    DEFAULT_KIND = 'default'
    IMAGE_KIND = 'image'
    AUDIO_KIND = 'audio'
    VIDEO_KIND = 'video'
    FILE_KIND = (
        (DEFAULT_KIND, 'default'),
        (IMAGE_KIND, 'image'),
        (AUDIO_KIND, 'audio'),
        (VIDEO_KIND, 'video')
    )

    FILE_TYPES = (
        (FILE_TYPE, 1),
        (FOLDER_TYPE, 2),
        (TEAM_FOLDER_TYPE , 3),
        (SHARED_FOLDER_TYPE, 4),
        (ALBUM_FOLDER_TYPE, 5)
    )

    id = models.CharField(max_length=40, primary_key=True, default=generate_file_id)
    parent = models.ForeignKey("self", blank=True, null=True , db_constraint=False)
    file_name = models.CharField(max_length=261, null=False, blank=False)                       # Name of the file
    container = models.ForeignKey(SwiftContainer, null=True, blank=True, db_constraint=False)
    type = models.IntegerField(blank=True, null=True, choices=FILE_TYPES)
    kind = models.CharField(max_length=40, blank=True, null=True)                               # image, document, file, folder , archive, shared folder, video
    extension = models.CharField(max_length=40, blank=True, null=True)
    icon = models.CharField(max_length=10, blank=True, null=True)
    direct_blockserver_link = models.TextField(blank=True, null=True)
    fq_path_reference = models.CharField(max_length=36, null=True, blank=True)                  # Hash of md5(parent path)
    fq_path = models.TextField(blank=True, null=True)
    swift_href = models.TextField(null=False, blank=False)                                      # Url access store in swift /container/object
    is_dir = models.BooleanField(default=False)
    in_queue = models.BooleanField(default=False)
    is_starred = models.BooleanField(default=False)
    color = models.CharField(max_length=7, blank=True, null=True)
    bytes = models.BigIntegerField(null=True, blank=True, default=0)
    user_last_modified = models.DateTimeField(default=timezone.now)
    thumbnail_url_tmpl = models.TextField(blank=True, null=True)
    large_thumbnail_url_tmpl = models.TextField(blank=True, null=True)
    album_large_thumbnail_url_tmpl = models.TextField(blank=True, null=True)
    current_version = models.CharField(max_length=40, blank=True, null=True)                    # Current version will be created first
    file_count = models.IntegerField(blank=True,null=True, default=0)
    count = models.IntegerField(blank=True,null=True, default=1)
    permissions = models.IntegerField(blank=True,null=True, choices=PERMISSION_TYPES,default=PERMISSION_ALL)
    status = models.IntegerField(choices=FILE_STATUS, null = True, blank = True, default = 1)
    image_info = JSONField(null=True, default=None, blank=True)

    class Meta:
        db_table = 'block_filesystem'
        unique_together = ("container", "fq_path_reference")
        index_together = [
            ["parent", "file_name"],                # List item in folder
            ["container", "kind"],                  # Sort by kind
            ["container", "extension"],             # Sort by extension
            ["container", "bytes"],                 # Sort by size
            ["container", "user_last_modified"],    # Storage last modified
        ]

    def index(self):
        if self.status == self.FILE_CLEARED:
            return settings.ES.delete(index=settings.ELASTICSEARCH_INDEX, doc_type=settings.ELASTICSEARCH_DOCTYPE,id=self.id)
        else:
            return settings.ES.index(index = settings.ELASTICSEARCH_INDEX, doc_type = settings.ELASTICSEARCH_DOCTYPE,
                                     id = self.id,
                                     body = {
                                                 "fid": self.id,
                                                 "type": self.type,
                                                 "container": self.container.id,
                                                 "kind": self.kind,
                                                 "extension": self.extension,
                                                 "icon": self.icon,
                                                 "fq_path": self.fq_path,
                                                 "file_name": self.file_name,
                                                 "is_dir": self.is_dir,
                                                 "thumbnail_url_tmpl": self.thumbnail_url_tmpl,
                                                 "large_thumbnail_url_tmpl": self.large_thumbnail_url_tmpl,
                                                 "album_large_thumbnail_url_tmpl": self.album_large_thumbnail_url_tmpl,
                                                 "status": self.status
                                            }
                                    )

    def delete_index(self):
        return settings.ES.delete(index=settings.ELASTICSEARCH_INDEX, doc_type=settings.ELASTICSEARCH_DOCTYPE, id=self.id)

    def save(self, *args, **kwargs):
        super(FileSystem, self).save(*args, **kwargs)

    def __str__(self):
        if self.parent:
            return "( %s | %s | %s | %s | %s | %d | %d )" % (self.id, self.parent.id, self.fq_path , self.file_name,
                                                             self.fq_path_reference, self.type, self.status)
        else:
            return "( %s | %s | %s | %s | %s | %d | %d )" % (self.id, "/", self.fq_path, self.file_name,
                                                             self.fq_path_reference, self.type, self.status)


class FileSystemSync(FileSystem):
    """
    Model for searching data that's built on top of FileSystem model
    """
    class Meta:
        proxy = True

    def sync_dropbox(path_list):
        pass


class Album(BaseModel):
    ALBUM_INIT = 0
    ALBUM_ACTIVE = 2
    ALBUM_DELETED = 3
    ALBUM_STATUSES = (
        (ALBUM_INIT, 'Album was newly created'),
        (ALBUM_ACTIVE, 'Album is activated'),
        (ALBUM_DELETED, 'Album was mark as deleted'),
    )

    T_AUTO_PHOTO = 'auto-image'
    T_AUTO_VIDEO = 'auto-video'
    T_USER_DEFINED_MIXED = 'udef-mixed'
    ALBUM_TYPES = (
        (T_AUTO_PHOTO, 'Image Auto Album'),
        (T_AUTO_VIDEO, 'Video Auto Album'),
        (T_USER_DEFINED_MIXED, 'Mixed Photo & Video User defined Album'),
    )

    # Permission
    PERMISSION_READ = 1
    PERMISSION_UPDATE = 2
    PERMISSION_CREATE = 4
    PERMISSION_DELETE = 8
    PERMISSION_SHARE = 16
    PERMISSION_ALL = 31

    PERMISSION_TYPES = (
        (PERMISSION_READ, 1),
        (PERMISSION_UPDATE, 2),
        (PERMISSION_CREATE, 4),
        (PERMISSION_DELETE, 8),
        (PERMISSION_SHARE, 16),
        (PERMISSION_ALL, 31),
    )

    id = models.CharField(max_length=40, primary_key=True, default=generate_album_id)
    container = models.ForeignKey(SwiftContainer, null=True, blank=True, db_constraint=False)
    name = models.CharField(max_length=40, null=False, blank=False)
    type = models.CharField(max_length=16, choices=ALBUM_TYPES)
    last_added_item = models.ForeignKey(FileSystem, null=True, default=None, db_constraint=False)
    cover_thumbnail_url_tmpl = models.TextField(blank=True, null=True)
    file_count = models.IntegerField(blank=True,null=True, default=0)
    permissions = models.IntegerField(blank=True, null=True, choices=PERMISSION_TYPES, default=PERMISSION_ALL)
    status = models.IntegerField(choices=ALBUM_STATUSES, null=False, blank=False, default=ALBUM_INIT)

    class Meta:
        db_table = 'block_album'
        index_together = [
            ["container", "status"],                # List album
        ]
        unique_together = ('container', 'name')


class FileAlbum(BaseModel):
    id = models.CharField(max_length=40, primary_key=True, default=generate_id)
    album = models.ForeignKey(Album, null=False, db_constraint=False)
    filesystem = models.ForeignKey(FileSystem, null=False, db_constraint=False, db_index=True)

    class Meta:
        db_table = 'block_file_album'
        index_together = [
            ["album", "created_at"],                # List item in album
        ]
        unique_together = ('album', 'filesystem')
