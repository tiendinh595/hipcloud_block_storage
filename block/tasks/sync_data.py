#!/usr/bin/env python
# -*- coding: utf-8 -*-
from celery import shared_task
from block.models.swift import SwiftContainer, FileSystem
from block.services.swift.files import SwiftStorage
from block.services.common import hash_path , get_file_kind
from block.services.thumbnails import generate_thumbnail
from django.conf import settings
from django.db.models import F
from apiclient import discovery, errors
from oauth2client import client
from block.models.swift import SwiftAccount
from django.conf import settings
from collections import OrderedDict
from django.utils import timezone
import itertools
import httplib2
import dropbox
import logging
import json
import os

logger = logging.getLogger(__name__)

GOOGLE_DRIVE_ROOT_FOLDER = "root"


def sync_folders(dropbox_root , container, dbox, dropbox_path):
    logger.info("Sync folders from " + dropbox_path + " to " + dropbox_root.fq_path)
    path_data = dbox.files_list_folder(dropbox_path, recursive=True)
    while True:
        # Build folder keys
        folders = { folder_metadata.path_display.replace(dropbox_path, "") : folder_metadata.name
                    for folder_metadata in path_data.entries
                    if not hasattr(folder_metadata, 'client_modified') }
        logger.debug("List folders from dropbox " + str(folders))
        # Create folders
        folder_tree = OrderedDict(sorted(folders.items(), key=lambda t: len(t[0].split("/"))))
        for folder in folder_tree.items():
            logger.debug("Create folder " + str(folder))
            folder_path = folder[0]
            folder_name = folder[1]
            if len(folder_path.split("/")) > 2:
                folder_path = dropbox_root.fq_path + folder[0]
                try:
                    parent_folder_path = '/'.join(folder_path.split("/")[:-1])
                    parent_folder = FileSystem.objects.get(fq_path_reference=hash_path(parent_folder_path))
                    try:
                        folder_to_save = FileSystem.objects.get(fq_path_reference=hash_path(folder_path))
                        folder_to_save.parent = parent_folder
                        if folder_to_save.status != FileSystem.FILE_ACTIVE:
                            folder_to_save.status = FileSystem.FILE_ACTIVE
                            folder_to_save.save()
                    except:
                        folder_to_save = FileSystem(
                            parent=parent_folder,
                            container=container,
                            type=FileSystem.FOLDER_TYPE,
                            kind='folder',
                            icon='default',
                            fq_path_reference=hash_path(folder_path),
                            fq_path=folder_path,
                            file_name=folder_name,
                            is_dir=True,
                            bytes=0,
                            user_last_modified=timezone.now(),
                            status=FileSystem.FILE_ACTIVE
                        )
                        folder_to_save.save()
                    logger.warn("Sync folder after sub folder : ", folder_to_save.fq_path)
                except Exception as ex:
                    logger.error(ex)
            else:
                # Folder under root level
                folder_path = dropbox_root.fq_path + folder[0]
                try:
                    folder_to_save = FileSystem.objects.get(fq_path_reference=hash_path(folder_path))
                    if folder_to_save.status != FileSystem.FILE_ACTIVE:
                        folder_to_save.status = FileSystem.FILE_ACTIVE
                        folder_to_save.save()
                except:
                    folder_to_save = FileSystem(
                        parent=dropbox_root,
                        container=container,
                        type=FileSystem.FOLDER_TYPE,
                        kind='folder',
                        icon='default',
                        fq_path_reference=hash_path(folder_path),
                        fq_path=folder_path,
                        file_name=folder_name,
                        is_dir=True,
                        bytes=0,
                        user_last_modified=timezone.now(),
                        status=FileSystem.FILE_ACTIVE
                    )
                    logger.warn("Sync folder after root : ", folder_to_save.fq_path)
                    folder_to_save.save()

        if not path_data.has_more:
            break
        else:
            path_data = dbox.files_list_folder_continue(path_data.cursor)

def sync_files(dropbox_root , container, dbox, dropbox_path, storage):
    logger.info("Sync files from " + dropbox_path + " to " + dropbox_root.fq_path)
    path_data = dbox.files_list_folder(dropbox_path, recursive=True)
    # Init folder for storing dropbox download files
    local_file_save_path = settings.SITE_ROOT + settings.MEDIA_URL + container.id + "/" + timezone.now().strftime("%Y-%m-%d")
    if not os.path.exists(local_file_save_path):
        os.makedirs(local_file_save_path)

    while True:
        # Create files
        files = {folder_metadata.path_display.replace(dropbox_path, ""): { "name" : folder_metadata.name, "size" : folder_metadata.size }
                 for folder_metadata in path_data.entries[1:]
                 if hasattr(folder_metadata, 'client_modified')}
        # Create files
        file_tree = OrderedDict(sorted(files.items(), key=lambda t: len(t[0].split("/"))))
        total_size = 0
        logger.debug("Files to sync ")
        logger.debug(file_tree)
        for data_file in file_tree.items():
            logger.debug("Syncing file  " + str(data_file))
            dropbox_file_path = data_file[0]
            file_name = data_file[1]['name']
            file_size = data_file[1]['size']
            file_extension = file_name.split(".")[-1]
            kind = get_file_kind(file_extension)
            if len(dropbox_file_path.split("/")) > 2:
                file_path = dropbox_root.fq_path + dropbox_file_path
                try:
                    parent_folder_path = '/'.join(file_path.split("/")[:-1])
                    logger.debug("Find parent folder : " + parent_folder_path)
                    parent_folder = FileSystem.objects.get(fq_path_reference=hash_path(parent_folder_path))
                    try:
                        file_to_download = FileSystem.objects.get(fq_path_reference=hash_path(file_path))
                        file_to_download.extension = file_extension
                        file_to_download.bytes = file_size
                        if file_to_download.status != FileSystem.FILE_ACTIVE:
                            file_to_download.status = FileSystem.FILE_ACTIVE
                            file_to_download.save()
                    except:
                        file_to_download = FileSystem(
                            parent=parent_folder,
                            container=container,
                            extension=file_extension,
                            type=FileSystem.FILE_TYPE,
                            kind=kind,
                            icon='default',
                            fq_path_reference=hash_path(file_path),
                            fq_path=file_path,
                            file_name=file_name,
                            is_dir=False,
                            bytes=file_size,
                            user_last_modified=timezone.now(),
                            status=FileSystem.FILE_SYNCING
                        )
                        file_to_download.save()
                except Exception as ex:
                    logger.error(ex)
            else:
                file_path = dropbox_root.fq_path + dropbox_file_path
                try:
                    file_to_download = FileSystem.objects.get(fq_path_reference=hash_path(file_path))
                    file_to_download.extension = file_extension
                    file_to_download.bytes = file_size
                    if file_to_download.status != FileSystem.FILE_ACTIVE:
                        file_to_download.status = FileSystem.FILE_ACTIVE
                        file_to_download.save()
                except:
                    file_to_download = FileSystem(
                        parent=dropbox_root,
                        container=container,
                        type=FileSystem.FILE_TYPE,
                        extension=file_extension,
                        kind=kind,
                        icon='default',
                        fq_path_reference=hash_path(file_path),
                        fq_path=file_path,
                        file_name=file_name,
                        is_dir=False,
                        bytes=file_size,
                        user_last_modified=timezone.now(),
                        status=FileSystem.FILE_SYNCING
                    )
                    file_to_download.save()

            logger.error("File to download " + str(file_to_download.__dict__))
            logger.error("Start download file   "  + dropbox_file_path)
            # Start download file
            try:
                full_local_file_save_path = local_file_save_path + "/" + file_name
                logger.error("Save to " +  full_local_file_save_path)
                dbox.files_download_to_file(full_local_file_save_path,   dropbox_file_path)
                # Generate thumbnail
                if file_to_download.kind == FileSystem.IMAGE_KIND:
                    local_thumbnails_paths, track_exif = generate_thumbnail(file_name, full_local_file_save_path)
                    if local_thumbnails_paths:
                        try:
                            small_thumb = local_thumbnails_paths[0]
                            storage.upload_single_file(small_thumb[0], small_thumb[1])
                            large_thumb = local_thumbnails_paths[1]
                            storage.upload_single_file(large_thumb[0], large_thumb[1])
                            album_large_thumb = local_thumbnails_paths[2]
                            storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])

                            file_to_download.thumbnail_url_tmpl = small_thumb[1]
                            file_to_download.large_thumbnail_url_tmpl = large_thumb[1]
                            file_to_download.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                        except:
                            pass

                is_uploaded = storage.upload_single_file(full_local_file_save_path, file_to_download.id)
                if is_uploaded:
                    total_size = total_size + file_size
                    file_to_download.status = FileSystem.FILE_ACTIVE
                else:
                    file_to_download.status = FileSystem.FILE_SYNC_FAIL
                file_to_download.save()
            except Exception as ex:
                logger.error(ex)
                file_to_download.delete()
        SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + total_size)
        if not path_data.has_more:
            break
        else:
            path_data = dbox.files_list_folder_continue(path_data.cursor)


def sync_dropbox_logic(dropbox_token, dropbox_list_paths, target_path, container_id, account_id):
    dbox = dropbox.Dropbox(dropbox_token)
    try:
        account = SwiftAccount.objects.get(id=account_id)
        container = SwiftContainer.objects.get(pk=container_id)
        if target_path == "/":
            target_path_filesystem = None
            dropbox_path_root = "/Dropbox"
        else:
            target_path_filesystem = FileSystem.objects.get(container=container,
                                                            fq_path_reference=hash_path(target_path))
            dropbox_path_root = target_path_filesystem.fq_path + "/Dropbox"
        # Init dropbox folder
        try:
            dropbox_folder = FileSystem.objects.get(container=container, fq_path_reference=hash_path(dropbox_path_root))
            if dropbox_folder.status != FileSystem.FILE_ACTIVE:
                dropbox_folder.status = FileSystem.FILE_ACTIVE
                dropbox_folder.save()
        except:
            dropbox_folder = FileSystem(
                parent=target_path_filesystem,
                container=container,
                type=FileSystem.FOLDER_TYPE,
                kind='folder',
                icon='folder_1',
                fq_path_reference=hash_path(dropbox_path_root),
                fq_path=dropbox_path_root,
                file_name="Dropbox",
                is_dir=True,
                bytes=0,
                user_last_modified=timezone.now(),
                status=FileSystem.FILE_ACTIVE
            )
            dropbox_folder.save()
    except Exception as ex:
        logger.error(ex)
        return

    # Init all paths to download folders
    for dropbox_path in dropbox_list_paths:
        logger.info("Sync folder "+ dropbox_path)
        sync_folders(dropbox_folder, container, dbox, dropbox_path)

    # Init swift storage connection
    storage = SwiftStorage(account, container)
    # Create & download & upload to swift

    # Download and store files
    for dropbox_path in dropbox_list_paths:
        sync_files(dropbox_folder, container, dbox, dropbox_path, storage)


@shared_task(queue='block.swift.dbox.sync')
def sync_dropbox(dropbox_token, dropbox_list_paths, target_path, container_id, account_id):
    sync_dropbox_logic(dropbox_token, dropbox_list_paths, target_path, container_id, account_id)


def validate_google_drive_token(google_token):
    credentials = client.AccessTokenCredentials(google_token, 'my-user-agent/1.0')
    try:
        http = httplib2.Http()
        http = credentials.authorize(http)
        service = discovery.build('drive', 'v2', http=http)
        file = service.files().get(fileId=GOOGLE_DRIVE_ROOT_FOLDER).execute()
    except:
        return False
    return True


def download_ggdrive_file(http, service, file_id, local_fd):
    request = service.files().get_media(fileId=file_id)
    media_request = http.MediaIoBaseDownload(local_fd, request)

    while True:
        try:
            download_progress, done = media_request.next_chunk()
        except errors.HttpError, error:
            print 'An error occurred: %s' % error
            return
        if download_progress:
            print 'Download Progress: %d%%' % int(download_progress.progress() * 100)
        if done:
            print 'Download Complete'
            return

def sync_google_drive_logicsync_files(dropbox_root , container, google_token, dropbox_path, storage):
    credentials = client.AccessTokenCredentials(google_token, 'my-user-agent/1.0')
    http = httplib2.Http()
    http = credentials.authorize(http)
    service = discovery.build('drive', 'v2', http=http)

    # Init local temp file
    local_file_save_path = settings.SITE_ROOT + settings.MEDIA_URL + container.id + "/" + timezone.now().strftime("%Y-%m-%d")
    if not os.path.exists(local_file_save_path):
        os.makedirs(local_file_save_path)

    page_token = None
    while True:
        try:
            param = {}
            if page_token:
                param['pageToken'] = page_token
                param['field'] = "items,etag,kind,nextLink,nextPageToken,selfLink"
            children = service.children().list(folderId="root", **param).execute()

            for child in children.get('items', []):
                try:
                    file = service.files().get(fileId=child['id']).execute()
                    # Download file
                    download_ggdrive_file(http, service, file['id'], local_fd)

                    # print 'Id: %s' % file['id']
                    # print 'Title: %s' % file['title']
                    # print 'mimeType: %s' % file['mimeType']
                    # print 'fileSize: %s' % file.get('fileSize', 0)
                    # print 'fileExtension: %s' % file.get('fileExtension', '')
                    # print 'fileExtension: %s' % file.get('fileExtension', '')
                    # print  json.dumps(file)
                    # print '========================'
                except errors.HttpError, error:
                    print 'An error occurred: %s' % error

            page_token = children.get('nextPageToken')
            if not page_token:
                break
        except errors.HttpError, error:
            logger.error('An error occurred: %s' % error)
            break

    # results = service.files().list(pageSize=100, orderBy="folder",fields="nextPageToken, files(id, name, parents, mimeType, size, webContentLink, imageMediaMetadata)").execute()
    # items = results.get('files', [])
    # if not items:
    #     print('No files found.')
    # else:
    #     print('Files:')
    #     for item in items:
    #         print json.dumps(item)

    # Download file
    # file_id = '0BwwA4oUTeiV1UVNwOHItT0xfa2M'
    # request = drive_service.files().get_media(fileId=file_id)
    # fh = io.BytesIO()
    # downloader = MediaIoBaseDownload(fh, request)
    # done = False
    # while done is False:
    #     status, done = downloader.next_chunk()
    #     print "Download %d%%." % int(status.progress() * 100)


@shared_task()
def sync_google_drive(google_token):
    sync_google_drive_logic(google_token)
