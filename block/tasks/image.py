#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
import json
import logging
from celery import shared_task, Task

from block.services.swift.files import SwiftStorage
from block.tasks.tracking import track_direct
from django.utils import timezone
from block.models import FileSystem, UploadSession
from block.services.thumbnails import generate_thumbnail
from block.services.album import AlbumUtils
from block.views.radosgw import get_account_container

logger = logging.getLogger(__name__)


@shared_task
def generate_thumb(user_id, file_id, full_path = None):
    account, container = get_account_container(user_id)
    storage = SwiftStorage(account, container)

    filesystem = FileSystem.objects.get(id=file_id)

    filesystem.in_queue = False
    filesystem.save()

    try:
        if full_path == None:
            full_path = storage.get_file_from_storage(filesystem.id, filesystem.file_name,
                                                  user_id + "_temp/" + timezone.now().strftime("%Y-%m-%d"), container)

        if full_path == '':
            logger.info("download fail")
            local_thumbnails_paths = []
        else:
            logger.info("start gen file: "+full_path)
            local_thumbnails_paths, track_exif = generate_thumbnail(filesystem.file_name, full_path)
            filesystem.image_info = track_exif
    except Exception as ex:
        logger.exception(ex)
        print('---------retry download file----------')
        raise generate_thumb.retry(countdown=1, exc=ex, max_retries=3)
    print('local_thumbnails_paths: ')
    print(local_thumbnails_paths)
    # try:
    #     track_exif['fid'] = filesystem.id
    #     track_exif['FileSize'] = filesystem.bytes
    #     if track_exif:
    #         track_exif['exif'] = True
    #         track_direct("media_upload", json.dumps(track_exif), "image")
    #     else:
    #         track_exif['exif'] = False
    #         track_direct("media_upload", json.dumps(track_exif), "image")
    # except Exception as ex:
    #     pass
        # logger.exception(ex)
        # raise generate_thumb.retry(countdown=1, exc=ex, max_retries=2)
    print('track_exif----: ')
    print(track_exif)
    if local_thumbnails_paths:
        print('---build thumb success---')
        try:
            small_thumb = local_thumbnails_paths[0]
            storage.upload_single_file(str_unicode(small_thumb[0]), str_unicode(small_thumb[1]))
            large_thumb = local_thumbnails_paths[1]
            storage.upload_single_file(str_unicode(large_thumb[0]), str_unicode(large_thumb[1]))
            album_large_thumb = local_thumbnails_paths[2]
            storage.upload_single_file(str_unicode(album_large_thumb[0]), str_unicode(album_large_thumb[1]))

            filesystem.thumbnail_url_tmpl = str_unicode(small_thumb[1])
            filesystem.large_thumbnail_url_tmpl = str_unicode(large_thumb[1])
            filesystem.album_large_thumbnail_url_tmpl = str_unicode(album_large_thumb[1])
            filesystem.save()
        except Exception as ex:
            print('---------retry update thumb----------')
            logger.exception(ex)
            raise generate_thumb.retry(countdown=1, exc=ex, max_retries=2)
    else:
        print('----build thumb fail----')
    # add image to album
    try:
        if filesystem.kind == FileSystem.IMAGE_KIND:
            album_utils = AlbumUtils(container_id=container)
            auto_photo_album = album_utils.get_auto_photo_album()
            if isinstance(filesystem.image_info, dict):
                try:
                    if int(filesystem.image_info['width']) >= 800 and int(filesystem.image_info['height']) >= 800:
                        album_utils._album = auto_photo_album
                        album_utils.add_item_to_album(filesystem=filesystem, check_permission=False)
                except KeyError:
                    pass
                except ValueError:
                    pass
    except Exception as ex:
        pass
        # logger.exception(ex)
        # raise generate_thumb.retry(countdown=1, exc=ex, max_retries=2)

    logger.info("end gen file: " + filesystem.file_name)
    # filesystem.user_last_modified = timezone.now()
    filesystem.save()
    filesystem.index()
    return True


def str_unicode(str):
    str = str.encode("utf-8")
    return str