#!/usr/bin/env python
# -*- coding: utf-8 -*-
from celery import shared_task,current_task
from django.conf import settings
import requests
import logging

logger = logging.getLogger(__name__)


@shared_task
def track(event_name, tracking_data, sub_event_name = ""):
    try:
        response = requests.post(settings.ANALYTIC_HOST, data={
            "action": event_name,
            "subaction": sub_event_name,
            "data": tracking_data
        },timeout=(1.5, 1.5))
        if response.text == "{\"status\":true}":
            return True
        else:
            return False
    except Exception as ex:
        logger.error(ex)
        return False


def track_direct(event_name, tracking_data, sub_event_name = ""):
    try:
        response = requests.post(settings.ANALYTIC_HOST, data={
            "action": event_name,
            "subaction": sub_event_name,
            "data": tracking_data
        },timeout=(1.5, 1.5))
        if response.text == "{\"status\":true}":
            return True
        else:
            return False
    except Exception as ex:
        logger.error(ex)
        return False