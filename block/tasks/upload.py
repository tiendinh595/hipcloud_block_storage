#!/usr/bin/env python
# -*- coding: utf-8 -*-
from celery import shared_task,current_task
from django.core.cache import cache

# python manage.py celery worker --loglevel=DEBUG --settings=hipcloud_block_storage.settings.prod

@shared_task(queue='block.swift.upload')
def upload_to_swift(local_path_url):
    return local_path_url