import base64
import io
import json
import logging
import os
import re
from collections import OrderedDict

import httplib2
import requests
import time
from celery import shared_task
from dateutil.parser import parse
from django.conf import settings

import dropbox
from django.db.models import F
from django.utils import timezone
from googleapiclient import discovery
from googleapiclient.http import MediaIoBaseDownload
from jose import jwt
from oauth2client import client

from block.models import AccessToken
from block.models import FileSystem
from block.models import SwiftAccount
from block.models import SwiftContainer
from block.models.sync import SyncSession
from block.services.common import hash_path, get_file_kind
from block.services.swift.files import SwiftStorage
from block.services.thumbnails import generate_thumbnail
from apiclient import errors
logger = logging.getLogger(__name__)


def push_notification(sync_session, notification, access_token, user_id):
    logger.info('--- push notification')
    try:
        sync_value = json.loads(sync_session.sync_value)
    except:
        sync_value = {
            "total_size": 0,
            "total_files": 0,
            "current_file_path": '',
            "sync_success_files": 0,
            "sync_success_size": 0
        }
    try:
        access_token = AccessToken.objects.get(id = access_token)
        notification = json.dumps({
            "notification": {
                "title": notification[0],
                "body": notification[1]
            },
            "data": {
                "status": sync_session.status,
                "sync_type": sync_session.sync_type,
                "total_size": sync_value['total_size'],
                "total_files": sync_value['total_files'],
                "current_file_path": sync_value['current_file_path'],
                "sync_success_files": sync_value['sync_success_files'],
                "sync_success_size": sync_value['sync_success_size']
            },
            "priority": "high"
        })
        logger.info(notification)
        token_data = {'iss': access_token.user_id, 'iat': access_token.issued_at,
                      'exp': access_token.expiry_at, 'mth': 'mo', 'tok': access_token.id, 'sub': access_token.subject_id,
                      'context': notification, 'admin': False}

        token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')
        r = requests.post(settings.CAS_PUSH_NOTIFICATION_URL, data={'payload': token})
        logger.info(r.content)
    except Exception as ex:
        logger.error(ex)

def get_increasing_file(filename, index):
    extension = filename.split(".")
    if len(extension) > 1:
        extension = "." +extension[-1]
    else:
        extension = ''
    filename_only = filename.replace(extension, "")
    return filename_only + "(" + str(index) + ")" + extension

# Dropbox
def sync_dropbox_parse_files(dbox, dropbox_path = ''):
    logger.info('Parse Dropbox file list')
    folders = {}
    files = {}
    path_data = dbox.files_list_folder(dropbox_path, recursive=True)
    while True:
        for item in path_data.entries:
            if not hasattr(item, 'client_modified'):
                folders[item.path_display] = {
                    'path': item.path_display,
                    'name': item.name,
                    'type': 'folder'
                }
            else:
                files[item.path_display] = {
                    'path': item.path_display,
                    'name': item.name,
                    'type': 'file',
                    'size': item.size,
                    'rev': item.rev,
                }
        if not path_data.has_more:
            break
        else:
            path_data = dbox.files_list_folder_continue(path_data.cursor)

    folders = OrderedDict(sorted(folders.items(), key=lambda t: len(t[0].split("/"))))
    files = OrderedDict(sorted(files.items(), key=lambda t: len(t[0].split("/"))))
    logger.info('Folders: '+str(folders))
    logger.info('Files: '+str(files))
    return folders, files

def sum_dropbox_files(dbox, dropbox_path = ''):
    folders, files = sync_dropbox_parse_files(dbox, dropbox_path)
    total_size = 0
    total_files = 0

    for key, item in files.items():
        total_size += item['size']
        total_files += 1

    return total_size, total_files

def sync_dropbox_folders(dropbox_root , container, folders):
    for key, folder in folders.items():
        logger.info("Create folder: " + str(folder))
        folder_path = folder['path']
        folder_name = folder['name']
        if len(folder_path.split("/")) > 2:
            folder_path = dropbox_root.fq_path + folder['path']
            logger.info("Create folder_path: " + str(folder_path))
            try:
                parent_folder_path = '/'.join(folder_path.split("/")[:-1])
                parent_folder = FileSystem.objects.get(fq_path_reference=hash_path(parent_folder_path), container=container)
                try:
                    folder_to_save = FileSystem.objects.get(fq_path_reference=hash_path(folder_path), container=container)
                    folder_to_save.parent = parent_folder
                    if folder_to_save.status != FileSystem.FILE_ACTIVE:
                        folder_to_save.status = FileSystem.FILE_ACTIVE
                        folder_to_save.save()
                except:
                    folder_to_save = FileSystem(
                        parent=parent_folder,
                        container=container,
                        type=FileSystem.FOLDER_TYPE,
                        kind='folder',
                        icon='default',
                        fq_path_reference=hash_path(folder_path),
                        fq_path=folder_path,
                        file_name=folder_name,
                        is_dir=True,
                        bytes=0,
                        user_last_modified=timezone.now(),
                        status=FileSystem.FILE_ACTIVE
                    )
                    folder_to_save.save()
                logger.info("Sync folder after sub folder: " + str(folder_to_save.fq_path))
            except Exception as ex:
                logger.error(ex)
        else:
            # Folder under root level
            folder_path = dropbox_root.fq_path + folder['path']
            logger.info("Create folder_path: " + str(folder_path))
            try:
                folder_to_save = FileSystem.objects.get(fq_path_reference=hash_path(folder_path), container=container)
                if folder_to_save.status != FileSystem.FILE_ACTIVE:
                    folder_to_save.status = FileSystem.FILE_ACTIVE
                    folder_to_save.save()
            except:
                folder_to_save = FileSystem(
                    parent=dropbox_root,
                    container=container,
                    type=FileSystem.FOLDER_TYPE,
                    kind='folder',
                    icon='default',
                    fq_path_reference=hash_path(folder_path),
                    fq_path=folder_path,
                    file_name=folder_name,
                    is_dir=True,
                    bytes=0,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_ACTIVE
                )
                logger.info("Sync folder after root: " + str(folder_to_save.fq_path))
                folder_to_save.save()

def sync_dropbox_files(dbox, dropbox_root, sync_session, container, storage, files, access_token, user_id):
    try:
        sync_value = json.loads(sync_session.sync_value)
    except Exception as ex:
        logger.info(ex.message)
    logger.info('Sync session: '+sync_session.id)
    # Init folder for storing dropbox download files
    local_file_save_path = settings.SITE_ROOT + settings.MEDIA_URL + container.id + "/" + timezone.now().strftime("%Y-%m-%d")
    if not os.path.exists(local_file_save_path):
        os.makedirs(local_file_save_path)

    for key, data_file in files.items():
        dropbox_file_path = data_file['path']
        file_name = data_file['name']
        file_size = data_file['size']

        sync_session = SyncSession.objects.get(id=sync_session.id)
        try:
            sync_value = json.loads(sync_session.sync_value)
        except Exception as ex:
            logger.info(ex.message)
        logger.info("Sync status: " + str(sync_session.status))
        if sync_session.status == SyncSession.CANCEL:
            pending_size = sync_value['total_size'] - sync_value['sync_success_size']
            sync_session.total_size -= pending_size
            sync_value['total_size'] -= pending_size
            try:
                sync_session.sync_value = json.dumps(sync_value)
                sync_session.save()
                logger.info("SyncSession: " + str(sync_session.__dict__))
            except:
                pass
            SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - pending_size)
            logger.info("Sync is stop")
            push_notification(sync_session, ('Alert from Upbox', 'Import your Dropbox canceled, keep touch inside...'), access_token, user_id)
            return True
        else:
            logger.info("Syncing file: " + str(data_file))

            # Update status sync session
            sync_value['current_file_path'] = dropbox_file_path
            try:
                sync_session.sync_value = json.dumps(sync_value)
                sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                logger.info("SyncSession: " + str(sync_session.__dict__))
            except:
                pass

            file_extension = file_name.split(".")

            if len(file_extension) > 1:
                file_extension = str(file_extension[-1]).lower()
            else:
                file_extension = ''
            kind = get_file_kind(file_extension)

            file_path = dropbox_root.fq_path + dropbox_file_path

            if len(dropbox_file_path.split("/")) > 2:
                parent_folder_path = '/'.join(file_path.split("/")[:-1])
                logger.info("Find parent folder: " + parent_folder_path)
                parent_folder = FileSystem.objects.get(fq_path_reference=hash_path(parent_folder_path), container=container)
            else:
                parent_folder = dropbox_root

            try:
                file_to_download = FileSystem.objects.get(fq_path_reference=hash_path(file_path),
                                                          container=container)
                file_to_download.extension = file_extension
                file_to_download.kind = kind
                file_to_download.bytes = file_size
                file_to_download.status = FileSystem.FILE_SYNCING
                file_to_download.user_last_modified = timezone.now()
                file_to_download.save()
            except:
                file_to_download = FileSystem(
                    parent=parent_folder,
                    container=container,
                    extension=file_extension,
                    type=FileSystem.FILE_TYPE,
                    kind=kind,
                    icon='default',
                    fq_path_reference=hash_path(file_path),
                    fq_path=file_path,
                    file_name=file_name,
                    is_dir=False,
                    bytes=file_size,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_SYNCING
                )
                file_to_download.save()

            logger.info("Link download: " + storage.generate_download_url(file_to_download.id))
            logger.info("File to download: " + str(file_to_download.__dict__))
            logger.info("Start download file: " + dropbox_file_path)
            # Start download file
            try:
                full_local_file_save_path = local_file_save_path + "/" + file_name
                logger.info("Save to: " + full_local_file_save_path)
                dbox.files_download_to_file(full_local_file_save_path, dropbox_file_path, data_file['rev'])
                # Generate thumbnail
                if file_to_download.kind == FileSystem.IMAGE_KIND:
                    try:
                        local_thumbnails_paths, track_exif = generate_thumbnail(file_name, full_local_file_save_path)
                        if local_thumbnails_paths:
                            small_thumb = local_thumbnails_paths[0]
                            storage.upload_single_file(small_thumb[0], small_thumb[1])
                            large_thumb = local_thumbnails_paths[1]
                            storage.upload_single_file(large_thumb[0], large_thumb[1])
                            album_large_thumb = local_thumbnails_paths[2]
                            storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])

                            file_to_download.thumbnail_url_tmpl = small_thumb[1]
                            file_to_download.large_thumbnail_url_tmpl = large_thumb[1]
                            file_to_download.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                    except:
                        pass

                logger.info('Upload to S3: ' + full_local_file_save_path)
                is_uploaded = storage.upload_single_file(full_local_file_save_path, file_to_download.id)
                logger.info('Is upload to S3: ' + str(is_uploaded))
                logger.info('Update status file: ' + full_local_file_save_path)
                if is_uploaded:
                    file_to_download.status = FileSystem.FILE_ACTIVE
                    sync_value['sync_success_files'] += 1
                    sync_value['sync_success_size'] += file_size
                    try:
                        sync_session.sync_value = json.dumps(sync_value)
                        sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                        logger.info("SyncSession: " + str(sync_session.__dict__))
                    except:
                        pass
                else:
                    file_to_download.status = FileSystem.FILE_SYNC_FAIL
                file_to_download.save()
            except Exception as ex:
                logger.error(ex)
                file_to_download.delete()
                SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - file_size)

    # update sync status
    logger.info('Sync '+sync_session.id+' success')
    sync_session.status = SyncSession.SUCCESS
    sync_session.save()
    push_notification(sync_session, ('Alert from Upbox', 'Import your Dropbox successfully, keep touch inside...'), access_token, user_id)

def sync_dropbox_logic(sync_session_id, container_id, access_token, user_id):
    dropbox_folder = None
    try:
        account = SwiftAccount.objects.get(user_id=user_id)
        logger.error("SwiftAccount: " + str(account.__dict__))
        container = SwiftContainer.objects.get(pk=container_id)
        logger.error("SwiftContainer: " + str(container.__dict__))
        sync_session = SyncSession.objects.get(id=sync_session_id)
        logger.info("SyncSession: " + str(sync_session.__dict__))
        dbox = dropbox.Dropbox(sync_session.token)
        target_path_filesystem = None
        full_path = SyncSession.FOLDER_DROPBOX+"/" + sync_session.email_sync
        dropbox_path_root = ''

        # Init dropbox folder
        for dir in full_path.split('/'):
            dropbox_path_root += '/' + dir
            logger.info("dropbox_path_root: " + dropbox_path_root)
            try:
                dropbox_folder = FileSystem.objects.get(fq_path_reference=hash_path(dropbox_path_root), container=container)
                if dropbox_folder.status != FileSystem.FILE_ACTIVE:
                    dropbox_folder.status = FileSystem.FILE_ACTIVE
                    dropbox_folder.save()
            except:
                dropbox_folder = FileSystem(
                    parent=target_path_filesystem,
                    container=container,
                    type=FileSystem.FOLDER_TYPE,
                    kind='folder',
                    icon='folder_1',
                    fq_path_reference=hash_path(dropbox_path_root),
                    fq_path=dropbox_path_root,
                    file_name=dir,
                    is_dir=True,
                    bytes=0,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_ACTIVE
                )
                dropbox_folder.save()
            target_path_filesystem = dropbox_folder
            logger.info(dropbox_folder.__dict__)
    except Exception as ex:
        logger.error(ex)
        return

    if dropbox_folder != None:
        folders, files = sync_dropbox_parse_files(dbox, '')
        sync_dropbox_folders(dropbox_folder, container, folders)
        # Init swift storage connection
        # if settings.DEBUG:
        #     container_test_id = container_id.split('_test_')[0]
        #     container_test = SwiftContainer.objects.get(pk=container_test_id)
        #     storage = SwiftStorage(account, container_test)
        # else:
        storage = SwiftStorage(account, container)
        logger.info('Storage: ' + str(storage.__dict__))

        # Create & download & upload to swift
        # Download and store files
        sync_dropbox_files(dbox, dropbox_folder, sync_session, container, storage, files, access_token, user_id)

def dropbox_check_quota_logic(sync_session_id, container_id, access_token, user_id):
    try:
        container = SwiftContainer.objects.get(pk=container_id)
        sync_session = SyncSession.objects.get(id=sync_session_id)
    except Exception as ex:
        logger.error(ex)
        return

    try:

        dbox = dropbox.Dropbox(sync_session.token)

        size_sum, total_files = sum_dropbox_files(dbox, '')

        total_size = (size_sum + container.quota_count)
        logger.info("total_files: " + str(total_files))
        logger.info("size_sum : " + str(size_sum))
        logger.info("max_quota: " + str(container.max_quota))
        logger.info("size_sum + quota_count: " + str(total_size))

        sync_value = {
            'total_files': total_files,
            'total_size': size_sum,
            'sync_success_files': 0,
            'sync_success_size': 0,
            'current_file_path': ''
        }
        sync_session.sync_value = json.dumps(sync_value)
        sync_session.total_size = size_sum
        if total_size <= container.max_quota:
            sync_session.status = SyncSession.UNLINKED
            SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + size_sum)
        else:
            sync_session.status = SyncSession.QUOTA_EXCEED
        sync_session.save()
        logger.info("SyncSession: " + str(sync_session.__dict__))
        push_notification(sync_session, ('Alert from Upbox', 'Check quota import your Dropbox successfully, keep touch inside...'), access_token, user_id)
        if sync_session.status == SyncSession.UNLINKED:
            return 'success'
        else:
            return 'error'
    except:
        return 'error'

# Google Drive
def ggoauth_refresh_token(refresh_token):
    req = requests.post('https://accounts.google.com/o/oauth2/token',
                  data={
                      'grant_type': 'refresh_token',
                      'redirect_uri': settings.GOOGLE_OAUTH2_REDIRECT_URI,
                      'refresh_token': refresh_token,
                      'client_id': settings.GOOGLE_OAUTH2_CLIENT_ID,
                      'client_secret': settings.GOOGLE_OAUTH2_CLIENT_SECRET
                  },
                  headers={
                      'content-type': 'application/x-www-form-urlencoded'
                  })
    logger.info('------------')
    logger.info('refresh_token: '+refresh_token)
    logger.info(req.text)
    logger.info('------------')
    return json.loads(req.text)

def get_service_oauth(refresh_token):
    req = ggoauth_refresh_token(refresh_token)
    token = ''
    service = None
    expires_in = int(time.time())
    if 'access_token' in req:
        token = req['access_token']
    if 'expires_in' in req:
        # expires_in += 120
        expires_in += req['expires_in']
    try:
        credentials = client.AccessTokenCredentials(token, 'my-user-agent/1.0')
        http = httplib2.Http()
        http = credentials.authorize(http)
        service = discovery.build('drive', 'v3', http=http)
    except Exception as ex:
        logger.error(ex)
    logger.info('new token: ' + token)
    logger.info('expires_in: ' + str(expires_in))
    return token, expires_in, service

def sum_gdrive_files(service, container_id):
    folders, files = sync_gdrive_parse_files(service, container_id)
    total_size = 0
    total_files = 0

    for key, item in files.items():
        total_size += item['size']
        total_files += 1

    return total_size, total_files

def sync_gdrive_parse_files(service, container_id):
    local_file_save_path = settings.SITE_ROOT + settings.MEDIA_URL + container_id + "/" + timezone.now().strftime("%Y-%m-%d")
    if not os.path.exists(local_file_save_path):
        os.makedirs(local_file_save_path)

    page_token = None
    list = {}
    while True:
        try:
            param = {
                'pageSize': 20,
                'fields': 'nextPageToken, files(id, name, mimeType, size, ownedByMe, parents, explicitlyTrashed)'
            }
            if page_token:
                param['pageToken'] = page_token
            result = service.files().list(**param).execute()
            for item in result.get('files', []):
                # if item['name'] == 'admin, adv, sub, pub.rar':
                #     logger.info('Skip add to list: ' + item['name'])
                #     continue
                if item['ownedByMe'] and item['explicitlyTrashed'] == False:
                    size = 0
                    export_mimeType = ''
                    if item['mimeType'] == 'application/vnd.google-apps.folder':
                        type = 'folder'
                    else:
                        type = 'file'
                        if item['mimeType'] == 'application/vnd.google-apps.spreadsheet':
                            item['name'] += '.xlsx'
                            export_mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        elif item['mimeType'] == 'application/vnd.google-apps.document':
                            item['name'] += '.docx'
                            export_mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        elif item['mimeType'] == 'application/vnd.google-apps.presentation':
                            item['name'] += '.pptx'
                            export_mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                        elif item['mimeType'] == 'application/vnd.google-apps.drawing':
                            item['name'] += '.jpg'
                            export_mimeType = 'image/jpeg'
                        elif re.search(r'.*google-apps.*', item['mimeType']):
                            continue
                        if export_mimeType != '':
                            content = service.files().export(fileId=item['id'],
                                                             mimeType=export_mimeType).execute()
                            full_local_file_save_path = local_file_save_path + "/" + item['name']
                            with open(full_local_file_save_path, 'wb') as w:
                                w.write(content)
                                size = int(len(content))
                        elif 'size' in item:
                            size = int(item['size'])
                    list[item['id']] = {
                        'id': item['id'],
                        'path': item['name'],
                        'name': item['name'],
                        'type': type,
                        'size': size,
                        'mimeType': item['mimeType'],
                        'export_mimeType': export_mimeType,
                        'parent': item['parents'][-1]
                    }
            page_token = result.get('nextPageToken')
            if not page_token:
                break
        except errors.HttpError, error:
            print 'An error occurred: %s' % error
            break
    folders = {}
    files = {}
    path_folders = {}
    #  tree folders
    for key, item in list.items():
        if item['type'] == 'file':
            continue
        path = ''
        if list.has_key(item['parent']):
            path = '/' + list[item['parent']]['path']
        item['path'] = path + '/' + item['path']
        item['path'] = item['path'].replace('//', '/')
        path_folders[item['path']] = item
        folders[item['id']] = item
    # tree files
    for key, item in list.items():
        if item['type'] != 'file':
            continue
        path = ''
        if folders.has_key(item['parent']):
            path = '/' + folders[item['parent']]['path']
        item['path'] = path + '/' + item['path']
        item['path'] = item['path'].replace('//', '/')
        files[item['path']] = item

    folders = OrderedDict(sorted(path_folders.items(), key=lambda t: len(t[0].split("/"))))
    files = OrderedDict(sorted(files.items(), key=lambda t: len(t[0].split("/"))))
    return folders, files

def sync_gdrive_folders(gdrive_folder, sync_session, container, folders):
    for key, folder in folders.items():
        logger.info("Create folder: " + str(folder))
        folder_path = folder['path']
        folder_name = folder['name']
        if len(folder_path.split("/")) > 2:
            folder_path = gdrive_folder.fq_path + folder['path']
            try:
                parent_folder_path = '/'.join(folder_path.split("/")[:-1])
                parent_folder = FileSystem.objects.get(fq_path_reference=hash_path(parent_folder_path), container=container)
                try:
                    folder_to_save = FileSystem.objects.get(fq_path_reference=hash_path(folder_path), container=container)
                    folder_to_save.parent = parent_folder
                    if folder_to_save.status != FileSystem.FILE_ACTIVE:
                        folder_to_save.status = FileSystem.FILE_ACTIVE
                        folder_to_save.save()
                except:
                    folder_to_save = FileSystem(
                        parent=parent_folder,
                        container=container,
                        type=FileSystem.FOLDER_TYPE,
                        kind='folder',
                        icon='default',
                        fq_path_reference=hash_path(folder_path),
                        fq_path=folder_path,
                        file_name=folder_name,
                        is_dir=True,
                        bytes=0,
                        user_last_modified=timezone.now(),
                        status=FileSystem.FILE_ACTIVE
                    )
                    folder_to_save.save()
                logger.info("Sync folder after sub folder: " + str(folder_to_save.fq_path))
            except Exception as ex:
                logger.error(ex)
        else:
            # Folder under root level
            folder_path = gdrive_folder.fq_path + folder['path']
            try:
                folder_to_save = FileSystem.objects.get(fq_path_reference=hash_path(folder_path), container=container)
                if folder_to_save.status != FileSystem.FILE_ACTIVE:
                    folder_to_save.status = FileSystem.FILE_ACTIVE
                    folder_to_save.save()
            except:
                folder_to_save = FileSystem(
                    parent=gdrive_folder,
                    container=container,
                    type=FileSystem.FOLDER_TYPE,
                    kind='folder',
                    icon='default',
                    fq_path_reference=hash_path(folder_path),
                    fq_path=folder_path,
                    file_name=folder_name,
                    is_dir=True,
                    bytes=0,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_ACTIVE
                )
                logger.info("Sync folder after root: " + str(folder_to_save.fq_path))
                folder_to_save.save()

def sync_gdrive_files(service, gdrive_folder, sync_session, container, storage, files, access_token, user_id):
    try:
        sync_value = json.loads(sync_session.sync_value)
    except Exception as ex:
        logger.info(ex.message)
    logger.info('Sync session: '+sync_session.id)
    # Init folder for storing google drive download files
    local_file_save_path = settings.SITE_ROOT + settings.MEDIA_URL + container.id + "/" + timezone.now().strftime("%Y-%m-%d")
    if not os.path.exists(local_file_save_path):
        os.makedirs(local_file_save_path)
    for key, data_file in files.items():
        gdrive_file_path = data_file['path']
        file_name = data_file['name']
        file_size = data_file['size']

        sync_session = SyncSession.objects.get(id=sync_session.id)
        try:
            sync_value = json.loads(sync_session.sync_value)
        except Exception as ex:
            logger.info(ex.message)
        logger.info("Sync status: " + str(sync_session.status))
        if sync_session.status == SyncSession.CANCEL:
            pending_size = sync_value['total_size'] - sync_value['sync_success_size']
            sync_session.total_size -= pending_size
            sync_value['total_size'] -= pending_size
            try:
                sync_session.sync_value = json.dumps(sync_value)
                sync_session.save()
                logger.info("SyncSession: " + str(sync_session.__dict__))
            except:
                pass
            SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - pending_size)
            logger.info("Sync is stop")
            push_notification(sync_session, ('Alert from Upbox', 'Import your Google Drirve canceled, keep touch inside...'), access_token, user_id)
            return True
        else:
            logger.info("-------------------")
            time_expire = int(time.time())+600
            # current_time = int(time.time())+60
            logger.info("Check time refresh token: time_expire: "+str(time_expire)+" - sync_session.expires_in: "+str(sync_session.expires_in))
            if time_expire > sync_session.expires_in:
                logger.info("Update new token")
                token, expires_in, service = get_service_oauth(sync_session.refresh_token)
                if service != None:
                    sync_session.token = token
                    sync_session.expires_in = expires_in
                    sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                else:
                    sync_session.status = SyncSession.TOKEN_INVALID
                    sync_session.save()
                    return

            logger.info("Syncing file: " + str(data_file))

            # Update status sync session
            sync_value['current_file_path'] = gdrive_file_path
            try:
                sync_session.sync_value = json.dumps(sync_value)
                sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                logger.info("SyncSession: " + str(sync_session.__dict__))
            except:
                pass

            file_extension = file_name.split(".")
            if len(file_extension) > 1:
                file_extension = str(file_extension[-1]).lower()
            else:
                file_extension = None
            kind = get_file_kind(file_extension)

            file_path = gdrive_folder.fq_path + gdrive_file_path

            if len(gdrive_file_path.split("/")) > 2:
                parent_folder_path = '/'.join(file_path.split("/")[:-1])
                logger.info("Find parent folder: " + parent_folder_path)
                parent_folder = FileSystem.objects.get(fq_path_reference=hash_path(parent_folder_path), container=container)
            else:
                parent_folder = gdrive_folder

            try:
                file_to_download = FileSystem.objects.get(fq_path_reference=hash_path(file_path),
                                                          container=container)
                file_to_download.extension = file_extension
                file_to_download.kind = kind
                file_to_download.bytes = file_size
                file_to_download.status = FileSystem.FILE_SYNCING
                file_to_download.user_last_modified = timezone.now()
                file_to_download.save()
            except:
                file_to_download = FileSystem(
                    parent=parent_folder,
                    container=container,
                    extension=file_extension,
                    type=FileSystem.FILE_TYPE,
                    kind=kind,
                    icon='default',
                    fq_path_reference=hash_path(file_path),
                    fq_path=file_path,
                    file_name=file_name,
                    is_dir=False,
                    bytes=file_size,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_SYNCING
                )
                file_to_download.save()

            logger.info("Link download: " + storage.generate_download_url(file_to_download.id))
            logger.info("File to download: " + str(file_to_download.__dict__))
            logger.info("Start download file: " + gdrive_file_path)

            # Start download file
            try:
                # check token
                # download file
                full_local_file_save_path = local_file_save_path + "/" + file_name
                logger.info("Save to: " + full_local_file_save_path)
                if data_file['export_mimeType'] != '':
                    if service != None:
                        if not os.path.exists(full_local_file_save_path):
                            content = service.files().export(fileId=data_file['id'],
                                                             mimeType=data_file['export_mimeType']).execute()
                            with open(full_local_file_save_path, 'wb') as w:
                                w.write(content)
                                logger.info('Download: '+full_local_file_save_path+' success')
                                # Generate thumbnail
                                if file_to_download.kind == FileSystem.IMAGE_KIND:
                                    try:
                                        local_thumbnails_paths, track_exif = generate_thumbnail(file_name,
                                                                                                full_local_file_save_path)
                                        if local_thumbnails_paths:
                                            small_thumb = local_thumbnails_paths[0]
                                            storage.upload_single_file(small_thumb[0], small_thumb[1])
                                            large_thumb = local_thumbnails_paths[1]
                                            storage.upload_single_file(large_thumb[0], large_thumb[1])
                                            album_large_thumb = local_thumbnails_paths[2]
                                            storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])

                                            file_to_download.thumbnail_url_tmpl = small_thumb[1]
                                            file_to_download.large_thumbnail_url_tmpl = large_thumb[1]
                                            file_to_download.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                                    except:
                                        pass

                                logger.info('Upload to S3: '+full_local_file_save_path)
                                is_uploaded = storage.upload_single_file(full_local_file_save_path, file_to_download.id)
                                logger.info('Update status file: '+full_local_file_save_path)
                                if is_uploaded:
                                    file_to_download.status = FileSystem.FILE_ACTIVE
                                    sync_value['sync_success_files'] += 1
                                    sync_value['sync_success_size'] += file_size
                                    try:
                                        sync_session.sync_value = json.dumps(sync_value)
                                        sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                                        logger.info("SyncSession: " + str(sync_session.__dict__))
                                    except:
                                        pass
                                else:
                                    file_to_download.status = FileSystem.FILE_SYNC_FAIL
                                logger.info('Status file: ' + full_local_file_save_path+": "+str(file_to_download.status ))
                                file_to_download.save()
                        else:
                            logger.info('File local: ' + full_local_file_save_path + ' exists')
                            # Generate thumbnail
                            if file_to_download.kind == FileSystem.IMAGE_KIND:
                                try:
                                    local_thumbnails_paths, track_exif = generate_thumbnail(file_name,
                                                                                            full_local_file_save_path)
                                    if local_thumbnails_paths:
                                        small_thumb = local_thumbnails_paths[0]
                                        storage.upload_single_file(small_thumb[0], small_thumb[1])
                                        large_thumb = local_thumbnails_paths[1]
                                        storage.upload_single_file(large_thumb[0], large_thumb[1])
                                        album_large_thumb = local_thumbnails_paths[2]
                                        storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])

                                        file_to_download.thumbnail_url_tmpl = small_thumb[1]
                                        file_to_download.large_thumbnail_url_tmpl = large_thumb[1]
                                        file_to_download.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                                except:
                                    pass

                            logger.info('Upload to S3: ' + full_local_file_save_path)
                            is_uploaded = storage.upload_single_file(full_local_file_save_path, file_to_download.id)
                            logger.info('Update status file: ' + full_local_file_save_path)
                            if is_uploaded:
                                file_to_download.status = FileSystem.FILE_ACTIVE
                                sync_value['sync_success_size'] += file_size
                                sync_value['sync_success_files'] += 1
                                try:
                                    sync_session.sync_value = json.dumps(sync_value)
                                    sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                                    logger.info("SyncSession: " + str(sync_session.__dict__))
                                except:
                                    pass
                            else:
                                file_to_download.status = FileSystem.FILE_SYNC_FAIL
                            logger.info('Status file: ' + full_local_file_save_path+": "+str(file_to_download.status ))
                            file_to_download.save()
                    else:
                        logger.error('Token not valid')
                        file_to_download.delete()
                        SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - file_size)
                else:
                    if service != None:
                        request = service.files().get_media(fileId=data_file['id'])
                        fh = io.BytesIO()
                        downloader = MediaIoBaseDownload(fh, request)
                        done = False
                        while done is False:
                            status, done = downloader.next_chunk()
                            print "Download %d%%." % int(status.progress() * 100)
                            if done is True:
                                with open(full_local_file_save_path, 'wb') as w:
                                    w.write(fh.getvalue())
                                    logger.info('Download: '+full_local_file_save_path+' success')
                                    # Generate thumbnail
                                    if file_to_download.kind == FileSystem.IMAGE_KIND:
                                        try:
                                            local_thumbnails_paths, track_exif = generate_thumbnail(file_name, full_local_file_save_path)
                                            if local_thumbnails_paths:
                                                small_thumb = local_thumbnails_paths[0]
                                                storage.upload_single_file(small_thumb[0], small_thumb[1])
                                                large_thumb = local_thumbnails_paths[1]
                                                storage.upload_single_file(large_thumb[0], large_thumb[1])
                                                album_large_thumb = local_thumbnails_paths[2]
                                                storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])

                                                file_to_download.thumbnail_url_tmpl = small_thumb[1]
                                                file_to_download.large_thumbnail_url_tmpl = large_thumb[1]
                                                file_to_download.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                                        except:
                                            pass

                                    logger.info('Upload to S3: '+full_local_file_save_path)
                                    is_uploaded = storage.upload_single_file(full_local_file_save_path, file_to_download.id)
                                    logger.info('Is upload to S3: ' + str(is_uploaded))
                                    logger.info('Update status file: ' + full_local_file_save_path)
                                    if is_uploaded:
                                        file_to_download.status = FileSystem.FILE_ACTIVE
                                        sync_value['sync_success_size'] += file_size
                                        sync_value['sync_success_files'] += 1
                                        try:
                                            sync_session.sync_value = json.dumps(sync_value)
                                            sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                                            logger.info("SyncSession: " + str(sync_session.__dict__))
                                        except:
                                            pass
                                    else:
                                        file_to_download.status = FileSystem.FILE_SYNC_FAIL
                                    logger.info('Status file: ' + full_local_file_save_path+": "+str(file_to_download.status ))
                                    file_to_download.save()
                    else:
                        logger.error('Token not valid')
                        file_to_download.delete()
                        SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - file_size)
            except Exception as ex:
                logger.error(ex)
                logger.info('Download file: '+file_name+' error')
                file_to_download.delete()
                SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - file_size)

    # update sync status
    logger.info('Sync '+sync_session.id+' success')
    sync_session.status = SyncSession.SUCCESS
    sync_session.save()
    push_notification(sync_session, ('Alert from Upbox', 'Import your Google Drive successfully, keep touch inside...'), access_token, user_id)

def sync_gdrive_logic(sync_session_id, container_id, access_token, user_id):
    gdrive_folder = None
    try:
        account = SwiftAccount.objects.get(user_id=user_id)
        container = SwiftContainer.objects.get(pk=container_id)
        sync_session = SyncSession.objects.get(id=sync_session_id)
        logger.info("SyncSession: " + str(sync_session.__dict__))
        target_path_filesystem = None
        full_path = SyncSession.FOLDER_GOOGLE_DRIVE+"/" + sync_session.email_sync
        gdrive_path_root = ''
        # Init dropbox folder
        for dir in full_path.split('/'):
            gdrive_path_root += '/' + dir
            logger.info("gdrive_path_root: " + gdrive_path_root)
            try:
                gdrive_folder = FileSystem.objects.get(fq_path_reference=hash_path(gdrive_path_root), container=container)
                if gdrive_folder.status != FileSystem.FILE_ACTIVE:
                    gdrive_folder.status = FileSystem.FILE_ACTIVE
                    gdrive_folder.save()
            except:
                gdrive_folder = FileSystem(
                    parent=target_path_filesystem,
                    container=container,
                    type=FileSystem.FOLDER_TYPE,
                    kind='folder',
                    icon='folder_1',
                    fq_path_reference=hash_path(gdrive_path_root),
                    fq_path=gdrive_path_root,
                    file_name=dir,
                    is_dir=True,
                    bytes=0,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_ACTIVE
                )
                gdrive_folder.save()
            target_path_filesystem = gdrive_folder
            logger.info(gdrive_folder.__dict__)
    except Exception as ex:
        logger.error(ex)
        return

    if gdrive_folder != None:
        try:
            token, expires_in, service = get_service_oauth(sync_session.refresh_token)
            if service != None:
                sync_session.token = token
                sync_session.expires_in = expires_in
                sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
            else:
                sync_session.status = SyncSession.TOKEN_INVALID
                sync_session.save()
                return

            folders, files = sync_gdrive_parse_files(service, container_id)

            sync_gdrive_folders(gdrive_folder, sync_session, container, folders)

            # Init swift storage connection
            # if settings.DEBUG:
            #     container_test_id = container_id.split('_test_')[0]
            #     container_test = SwiftContainer.objects.get(pk=container_test_id)
            #     storage = SwiftStorage(account, container_test)
            # else:
            storage = SwiftStorage(account, container)
            logger.info('Storage: '+str(storage.__dict__))

            # Create & download & upload to swift
            # Download and store files
            sync_gdrive_files(service, gdrive_folder, sync_session, container, storage, files, access_token, user_id)
        except Exception as ex:
            logger.error(ex)

def gdrive_check_quota_logic(sync_session_id, container_id, access_token, user_id):
    try:
        container = SwiftContainer.objects.get(pk=container_id)
        sync_session = SyncSession.objects.get(id=sync_session_id)
    except Exception as ex:
        logger.error(ex)
        return

    try:
        token, expires_in, service = get_service_oauth(sync_session.refresh_token)
        if service != None:
            sync_session.token = token
            sync_session.expires_in = expires_in
            sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
        else:
            sync_session.sync_value = 'Token invalid'
            sync_session.status = SyncSession.TOKEN_INVALID
            sync_session.save()
            return 'error'

        size_sum, total_files = sum_gdrive_files(service, container_id)

        total_size = (size_sum + container.quota_count)
        logger.info("total_files: " + str(total_files))
        logger.info("size_sum : " + str(size_sum))
        logger.info("max_quota: " + str(container.max_quota))
        logger.info("size_sum + quota_count: " + str(total_size))

        sync_value = {
            'total_files': total_files,
            'total_size': size_sum,
            'sync_success_files': 0,
            'sync_success_size': 0,
            'current_file_path': ''
        }
        sync_session.sync_value = json.dumps(sync_value)
        sync_session.total_size = size_sum
        if total_size <= container.max_quota:
            sync_session.status = SyncSession.UNLINKED
            SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + size_sum)
        else:
            sync_session.status = SyncSession.QUOTA_EXCEED
        sync_session.save()
        logger.info("SyncSession: " + str(sync_session.__dict__))
        push_notification(sync_session, ('Alert from Upbox', 'Check quota import your Google Drive successfully, keep touch inside...'), access_token, user_id)
        if sync_session.status == SyncSession.UNLINKED:
            return 'success'
        else:
            return 'error'
    except Exception as ex:
        logger.error(ex)
        return 'error'


# Google Drive
def sum_gphotos_files(service, container_id):
    files = sync_gphotos_parse_files(service, container_id)
    total_size = 0
    total_files = 0

    for item in files:
        total_size += item['size']
        total_files += 1

    return total_size, total_files

def sync_gphotos_parse_files(service, container_id):
    page_token = None
    files = []
    while True:
        try:
            param = {
                'spaces': 'photos',
                'pageSize': 20,
                'fields': 'nextPageToken, files(id, name, size, mimeType, ownedByMe, explicitlyTrashed, createdTime, modifiedTime)'
            }
            if page_token:
                param['pageToken'] = page_token
            result = service.files().list(**param).execute()
            for item in result.get('files', []):
                if item['ownedByMe'] and item['explicitlyTrashed'] == False:
                    createad_at = parse(item['createdTime'])
                    updated_at = parse(item['modifiedTime'])
                    files.append({
                        'id': item['id'],
                        'name': item['name'],
                        'path': '/'+item['name'],
                        'size': int(item['size']),
                        'mimeType': item['mimeType'],
                        'created_at': createad_at.strftime('%Y-%m-%d %H:%M:%S'),
                        'updated_at': updated_at.strftime('%Y-%m-%d %H:%M:%S')
                    })
            page_token = result.get('nextPageToken')
            if not page_token:
                break
        except errors.HttpError, error:
            print 'An error occurred: %s' % error
            break
    return files

def sync_gphotos_files(service, gphotos_folder, sync_session, container, storage, files, access_token, user_id):
    try:
        sync_value = json.loads(sync_session.sync_value)
    except Exception as ex:
        logger.info(ex.message)
    logger.info('Sync session: '+sync_session.id)
    # Init folder for storing google photos download files
    local_file_save_path = settings.SITE_ROOT + settings.MEDIA_URL + container.id + "/" + timezone.now().strftime("%Y-%m-%d")
    if not os.path.exists(local_file_save_path):
        os.makedirs(local_file_save_path)
        os.chmod(local_file_save_path, 0777)
    for data_file in files:
        gphotos_file_path = data_file['path']
        file_name = data_file['name']
        file_size = data_file['size']

        sync_session = SyncSession.objects.get(id=sync_session.id)
        try:
            sync_value = json.loads(sync_session.sync_value)
        except Exception as ex:
            logger.info(ex.message)
        logger.info("Sync status: " + str(sync_session.status))
        if sync_session.status == SyncSession.CANCEL:
            pending_size = sync_value['total_size'] - sync_value['sync_success_size']
            sync_session.total_size -= pending_size
            sync_value['total_size'] -= pending_size
            try:
                sync_session.sync_value = json.dumps(sync_value)
                sync_session.save()
                logger.info("SyncSession: " + str(sync_session.__dict__))
            except:
                pass
            SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - pending_size)
            logger.info("Sync is stop")
            push_notification(sync_session, ('Alert from Upbox', 'Import your Google Photos canceled, keep touch inside...'), access_token, user_id)
            return True
        else:
            logger.info("-------------------")
            time_expire = int(time.time())+600
            # current_time = int(time.time())+60
            logger.info("Check time refresh token: time_expire: "+str(time_expire)+" - sync_session.expires_in: "+str(sync_session.expires_in))
            if time_expire > sync_session.expires_in:
                logger.info("Update new token")
                token, expires_in, service = get_service_oauth(sync_session.refresh_token)
                if service != None:
                    sync_session.token = token
                    sync_session.expires_in = expires_in
                    sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                else:
                    sync_session.status = SyncSession.TOKEN_INVALID
                    sync_session.save()
                    return

            logger.info("Syncing file: " + str(data_file))

            # Update status sync session
            sync_value['current_file_path'] = gphotos_file_path
            try:
                sync_session.sync_value = json.dumps(sync_value)
                sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                logger.info("SyncSession: " + str(sync_session.__dict__))
            except:
                pass
            file_extension = file_name.split(".")
            if len(file_extension) > 1:
                file_extension = str(file_extension[-1]).lower()
            else:
                file_extension = None
            kind = get_file_kind(file_extension)

            file_path = gphotos_folder.fq_path + gphotos_file_path

            parent_folder = gphotos_folder

            try:
                file_to_download = FileSystem.objects.get(fq_path_reference=hash_path(file_path),
                                                          container=container)
                file_to_download.extension = file_extension
                file_to_download.kind = kind
                file_to_download.bytes = file_size
                file_to_download.status = FileSystem.FILE_SYNCING
                file_to_download.user_last_modified = parse(data_file['created_at'])
                file_to_download.save()
            except:
                file_to_download = FileSystem(
                    parent=parent_folder,
                    container=container,
                    extension=file_extension,
                    type=FileSystem.FILE_TYPE,
                    kind=kind,
                    icon='default',
                    fq_path_reference=hash_path(file_path),
                    fq_path=file_path,
                    file_name=file_name,
                    is_dir=False,
                    bytes=file_size,
                    user_last_modified=parse(data_file['created_at']),
                    status=FileSystem.FILE_SYNCING
                )
                file_to_download.save()

            logger.info("Link download: " + storage.generate_download_url(file_to_download.id))
            logger.info("File to download: " + str(file_to_download.__dict__))
            logger.info("Start download file: " + gphotos_file_path)

            # Start download file
            try:
                # check token
                # download file
                full_local_file_save_path = local_file_save_path + "/" + file_name
                logger.info("Save to: " + full_local_file_save_path)
                if service != None:
                    request = service.files().get_media(fileId=data_file['id'])
                    fh = io.BytesIO()
                    downloader = MediaIoBaseDownload(fh, request)
                    done = False
                    while done is False:
                        status, done = downloader.next_chunk()
                        print "Download %d%%." % int(status.progress() * 100)
                        if done is True:
                            with open(full_local_file_save_path, 'wb') as w:
                                w.write(fh.getvalue())
                                logger.info('Download: '+full_local_file_save_path+' success')
                                # Generate thumbnail
                                if file_to_download.kind == FileSystem.IMAGE_KIND:
                                    try:
                                        os.chmod(full_local_file_save_path, 0666)
                                        logger.info('Gen thumb: '+full_local_file_save_path)
                                        local_thumbnails_paths, track_exif = generate_thumbnail(file_name, full_local_file_save_path)
                                        logger.info(str(local_thumbnails_paths))
                                        if local_thumbnails_paths:
                                            small_thumb = local_thumbnails_paths[0]
                                            storage.upload_single_file(small_thumb[0], small_thumb[1])
                                            large_thumb = local_thumbnails_paths[1]
                                            storage.upload_single_file(large_thumb[0], large_thumb[1])
                                            album_large_thumb = local_thumbnails_paths[2]
                                            storage.upload_single_file(album_large_thumb[0], album_large_thumb[1])

                                            file_to_download.thumbnail_url_tmpl = small_thumb[1]
                                            file_to_download.large_thumbnail_url_tmpl = large_thumb[1]
                                            file_to_download.album_large_thumbnail_url_tmpl = album_large_thumb[1]
                                    except:
                                        pass

                                logger.info('Upload to S3: '+full_local_file_save_path)
                                is_uploaded = storage.upload_single_file(full_local_file_save_path, file_to_download.id)
                                logger.info('Is upload to S3: ' + str(is_uploaded))
                                logger.info('Update status file: ' + full_local_file_save_path)
                                if is_uploaded:
                                    file_to_download.status = FileSystem.FILE_ACTIVE
                                    sync_value['sync_success_size'] += file_size
                                    sync_value['sync_success_files'] += 1
                                    try:
                                        sync_session.sync_value = json.dumps(sync_value)
                                        sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
                                        logger.info("SyncSession: " + str(sync_session.__dict__))
                                    except:
                                        pass
                                else:
                                    file_to_download.status = FileSystem.FILE_SYNC_FAIL
                                logger.info('Status file: ' + full_local_file_save_path+": "+str(file_to_download.status ))
                                file_to_download.save()
                else:
                    logger.error('Token not valid')
                    file_to_download.delete()
                    SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - file_size)
            except Exception as ex:
                logger.error(ex)
                logger.info('Download file: '+file_name+' error')
                file_to_download.delete()
                SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') - file_size)

    # update sync status
    logger.info('Sync '+sync_session.id+' success')
    sync_session.status = SyncSession.SUCCESS
    sync_session.save()
    push_notification(sync_session, ('Alert from Upbox', 'Import your Google Photos successfully, keep touch inside...'), access_token, user_id)

def sync_gphotos_logic(sync_session_id, container_id, access_token, user_id):
    gphotos_folder = None
    try:
        account = SwiftAccount.objects.get(user_id=user_id)
        container = SwiftContainer.objects.get(pk=container_id)
        sync_session = SyncSession.objects.get(id=sync_session_id)
        logger.info("SyncSession: " + str(sync_session.__dict__))
        target_path_filesystem = None
        full_path = SyncSession.FOLDER_GOOGLE_PHOTOS+"/" + sync_session.email_sync
        gphotos_path_root = ''
        # Init dropbox folder
        for dir in full_path.split('/'):
            gphotos_path_root += '/' + dir
            logger.info("gphotos_path_root: " + gphotos_path_root)
            try:
                gphotos_folder = FileSystem.objects.get(container=container,
                                                        fq_path_reference=hash_path(gphotos_path_root))
                if gphotos_folder.status != FileSystem.FILE_ACTIVE:
                    gphotos_folder.status = FileSystem.FILE_ACTIVE
                    gphotos_folder.save()
            except:
                gphotos_folder = FileSystem(
                    parent=target_path_filesystem,
                    container=container,
                    type=FileSystem.FOLDER_TYPE,
                    kind='folder',
                    icon='folder_1',
                    fq_path_reference=hash_path(gphotos_path_root),
                    fq_path=gphotos_path_root,
                    file_name=dir,
                    is_dir=True,
                    bytes=0,
                    user_last_modified=timezone.now(),
                    status=FileSystem.FILE_ACTIVE
                )
                gphotos_folder.save()
            target_path_filesystem = gphotos_folder
            logger.info(gphotos_folder.__dict__)
    except Exception as ex:
        logger.error(ex)
        return

    if gphotos_folder != None:
        try:
            token, expires_in, service = get_service_oauth(sync_session.refresh_token)
            if service != None:
                sync_session.token = token
                sync_session.expires_in = expires_in
                sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
            else:
                sync_session.status = SyncSession.TOKEN_INVALID
                sync_session.save()
                return

            files = sync_gphotos_parse_files(service, container_id)

            # Init swift storage connection
            # if settings.DEBUG:
            #     container_test_id = container_id.split('_test_')[0]
            #     container_test = SwiftContainer.objects.get(pk=container_test_id)
            #     storage = SwiftStorage(account, container_test)
            # else:
            storage = SwiftStorage(account, container)
            logger.info('Storage: '+str(storage.__dict__))

            # Create & download & upload to swift
            # Download and store files
            sync_gphotos_files(service, gphotos_folder, sync_session, container, storage, files, access_token, user_id)
        except Exception as ex:
            logger.error(ex)

def gphotos_check_quota_logic(sync_session_id, container_id, access_token, user_id):
    try:
        container = SwiftContainer.objects.get(pk=container_id)
        sync_session = SyncSession.objects.get(id=sync_session_id)
    except Exception as ex:
        logger.error(ex)
        return

    try:
        token, expires_in, service = get_service_oauth(sync_session.refresh_token)
        if service != None:
            sync_session.token = token
            sync_session.expires_in = expires_in
            sync_session.save(update_fields=['sync_value', 'token', 'refresh_token', 'expires_in', 'total_size'])
        else:
            sync_session.sync_value = 'Token invalid'
            sync_session.status = SyncSession.TOKEN_INVALID
            sync_session.save()
            return 'error'

        size_sum, total_files = sum_gphotos_files(service, container_id)

        total_size = (size_sum + container.quota_count)
        logger.info("total_files: " + str(total_files))
        logger.info("size_sum : " + str(size_sum))
        logger.info("max_quota: " + str(container.max_quota))
        logger.info("size_sum + quota_count: " + str(total_size))

        sync_value = {
            'total_files': total_files,
            'total_size': size_sum,
            'sync_success_files': 0,
            'sync_success_size': 0,
            'current_file_path': '',
        }
        sync_session.sync_value = json.dumps(sync_value)
        sync_session.total_size = size_sum
        if total_size <= container.max_quota:
            sync_session.status = SyncSession.UNLINKED
            SwiftContainer.objects.filter(id=container.id).update(quota_count=F('quota_count') + size_sum)
        else:
            sync_session.status = SyncSession.QUOTA_EXCEED
        sync_session.save()
        logger.info("SyncSession: " + str(sync_session.__dict__))
        push_notification(sync_session, ('Alert from Upbox', 'Check quota import your Google Photos successfully, keep touch inside...'), access_token, user_id)
        if sync_session.status == SyncSession.UNLINKED:
            return 'success'
        else:
            return 'error'
    except Exception as ex:
        logger.error(ex)
        return 'error'

@shared_task()
def sync_dropbox(sync_session_id, container_id, access_token, user_id):
    sync_dropbox_logic(sync_session_id, container_id, access_token, user_id)

@shared_task()
def dropbox_check_quota(sync_session_id, container_id, access_token, user_id):
    dropbox_check_quota_logic(sync_session_id, container_id, access_token, user_id)

@shared_task()
def sync_gdrive(sync_session_id, container_id, access_token, user_id):
    sync_gdrive_logic(sync_session_id, container_id, access_token, user_id)

@shared_task()
def gdrive_check_quota(sync_session_id, container_id, access_token, user_id):
    gdrive_check_quota_logic(sync_session_id, container_id, access_token, user_id)

@shared_task()
def sync_gphotos(sync_session_id, container_id, access_token, user_id):
    sync_gphotos_logic(sync_session_id, container_id, access_token, user_id)

@shared_task()
def gphotos_check_quota(sync_session_id, container_id, access_token, user_id):
    gphotos_check_quota_logic(sync_session_id, container_id, access_token, user_id)
