FROM python:2.7-slim
MAINTAINER TungNT

RUN apt-get update && apt-get install -y supervisor && apt-get install -y default-jre && apt-get install -y libreoffice-common && apt-get install -y libreoffice-writer && apt-get install -y libmysqlclient-dev && apt-get install -y build-essential && apt-get install -y libssl-dev && apt-get install -y libffi-dev && apt-get install -y libjpeg62-turbo-dev && apt-get install -y libmemcached-dev && pip install uwsgi && useradd -m block

ADD . /home/block

RUN  mkdir -p /var/log/hipcloud_block_storage && touch /var/log/hipcloud_block_storage/prod_block_uwsgi.log && chmod 777 /var/log/hipcloud_block_storage/prod_block_uwsgi.log && cd /home/block && pip install -r requirements.txt && cp /home/block/supervisor.conf /etc/supervisor/conf.d && python manage.py collectstatic --noinput --settings=hipcloud_block_storage.settings.collectstatic

WORKDIR /home/block

CMD ["/usr/bin/supervisord"]