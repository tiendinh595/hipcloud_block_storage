

Local Linux
===========
## RabbitMQ
*  Open new terminal
```
rabbitmqctl add_vhost /upbox
rabbitmqctl add_user upbox_user 65q10LCouJg525Q8f0yY
rabbitmqctl set_permissions -p /upbox upbox_user ".*" ".*" ".*" rabbitmqctl add_user admin upboxADm1n
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*" rabbitmqctl set_permissions -p /upbox admin ".*" ".*" ".*"
rabbitmqctl set_user_tags admin administrator
```
## Elasticsearch
*  Open new terminal
Run elasticsearch
```
docker pull elasticsearch
docker rm -f elasticsearch
docker run -d -p 9200:9200 elasticsearch
```
Check elasticsearch running
```
http://localhost:9200/
```
## Redis
*  Open new terminal
```
docker pull redis
docker rm -f redis
docker run --name redis -p 6379:6379 -d redis
```
Check redis running
```
terminal% docker ps

Output :
05d55f4843e1        redis               "docker-entrypoint.sh"   3 hours ago         Up 3 hours          6379/tcp                                                   tender_mahavira
```
## Application

*  Open new terminal
1. Setup python 2.7.x , to check python available.
```
terminal% which python
/usr/local/bin/python
```
2. Install linux dependencies 
```
apt-get update 
apt-get install -y supervisor 
apt-get install -y libmysqlclient-dev 
apt-get install -y build-essential 
apt-get install -y libssl-dev 
apt-get install -y libffi-dev 
```
3. Install pip : to check pip available:
```
teminal% which pip
Output:
/usr/local/bin/pip
```
4. Install virtualenv :
```
pip install virtualenv
```
5. Check virtualenv available :
```
terminal% which virtualenv
Output:
/usr/local/bin/virtualenv
```
6. Create virtual environment name *block_venv* with virualenv:
```
cd folder_to_store_virtualenv
virtualenv block_venv
```
7. Create virtual environment name *block_venv* with virualenv:
```
cd folder_to_store_virtualenv
virtualenv block_venv
```
8. Activate virualenv and a (block_venv) will appear in the next line:
```
terminal% source $PATH/folder_to_store_virtualenv/block_venv/bin/activate
Output:
(block_venv)terminal%
```
9. Go to project directory:
```
(block_venv)terminal% cd $PROJECT_DIR
```
10. Call install dependencies, file requirements.txt container in project root:
```
(block_venv)terminal% pip install -r requirements.txt
```
11. Create db name block_storage in mysql with UTF8 , UTF8_general_ci
12. Update local.py to connect redis and mysql
```
(block_venv)terminal% vim $PATH/hipcloud_block_storage/settings/local.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'block_storage',
        'USER': '<local-username-mysql>',
        'PASSWORD': '<local-password-mysql>',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://<redis-host>:<redis-port>/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 200},
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
        }
    }
}
```
13. Makemigrations to create sync db structure with mysql
```
(block_venv)terminal% python $PROJECT_ROOT/manage.py makemigrations \
--settings=hipcloud_block_storage.settings.local
```
14. Migrate to confirm sync db structure to mysql
```
(block_venv)terminal% python $PROJECT_ROOT/manage.py migrate \
--settings=hipcloud_block_storage.settings.local
```
15. Collectstatic to prepapre static files for project
```
(block_venv)terminal% python $PROJECT_ROOT/manage.py collectstatic \
--settings=hipcloud_block_storage.settings.local
```
16. Run Server at  *port* ( 8000 )
```
(block_venv)terminal% python $PROJECT_ROOT/manage.py runserver 8000 \
--settings=hipcloud_block_storage.settings.local 

Output:

System check identified no issues (0 silenced).
February 21, 2017 - 03:52:48
Django version 1.8.17, using settings 'hipcloud_block_storage.settings.local'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

Docker
======

## Prod

~~~~
docker build -t block_storage:$BUILD_NUMBER -f Dockerfile .
docker stop block_storage
docker rm block_storage
docker run --name block_storage --add-host='radosgw:103.68.252.5' --add-host='swift:103.68.252.70' --add-host='keystone:10.10.101.32' --add-host='controller proxy:100.75.18.70' --add-host='store01:100.75.32.34' --add-host='store02:100.75.38.23' --add-host='store03:100.75.48.19' --link elasticsearch:elasticsearch --link rabbitmq:rabbitmq --link zookeeper:zookeeper -p 127.0.0.1:8800:8000 -v /etc/machine-id:/home/block/machine-id -v /data/block_storage-data/media:/home/block/media -v /var/www/block_storage-data/block/migrations:/home/block/block/migrations -v /etc/localtime:/etc/localtime -m 4096M --memory-swap -1 -d block_storage:$BUILD_NUMBER
docker stop block_storage_1
docker rm block_storage_1
docker run --name block_storage_1 --add-host='radosgw:103.68.252.5' --add-host='swift:103.68.252.70' --add-host='keystone:10.10.101.32' --add-host='controller proxy:100.75.18.70' --add-host='store01:100.75.32.34' --add-host='store02:100.75.38.23' --add-host='store03:100.75.48.19' --link elasticsearch:elasticsearch --link rabbitmq:rabbitmq --link zookeeper:zookeeper -p 127.0.0.1:8900:8000 -v /etc/machine-id:/home/block/machine-id -v /data/block_storage-data/media:/home/block/media -v /var/www/block_storage-data/block/migrations:/home/block/block/migrations -v /etc/localtime:/etc/localtime -m 4096M --memory-swap -1 -d block_storage:$BUILD_NUMBER
docker stop block_celery
docker rm block_celery
docker run --name block_celery --add-host='radosgw:103.68.252.5' --add-host='swift:103.68.252.70' --add-host='keystone:10.10.101.32' --add-host='controller proxy:100.75.18.70' --add-host='store01:100.75.32.34' --add-host='store02:100.75.38.23' --add-host='store03:100.75.48.19' --link rabbitmq:rabbitmq --link elasticsearch:elasticsearch -v /etc/machine-id:/home/block/machine-id -v /data/block_storage-data/media:/home/block/media -v /var/www/block_storage-data/block/migrations:/home/block/block/migrations -v /etc/localtime:/etc/localtime -m 2048M --memory-swap -1 -d block_storage:$BUILD_NUMBER /bin/sh -c '/usr/local/bin/python manage.py celery worker --loglevel=DEBUG --settings=hipcloud_block_storage.settings.prod'
~~~~

## Local
* Build Service
`
docker build -t block_storage -f Dockerfile_local .
`
* MySQL
`
docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -v /Users/thaing/Working/SourceCode/mysql_docker:/var/lib/mysql -e MYSQL_DATABASE=block_storage -p 3307:3306 -d mysql
`
* Redis
`
docker run --name redis -p 6380:6379 -d redis
`
* Elasticsearch
`
docker run --name elasticsearch -p 9201:9200 -p 9301:9300 -v /Users/thaing/Working/SourceCode/elasticsearch_docker/data:/usr/share/elasticsearch/data -d elasticsearch
 `
* Run Service
`
docker run --name block_storage --add-host='radosgw:103.68.252.5' --add-host='swift:103.68.252.70' --add-host='keystone:10.10.101.32' --add-host='controller proxy:100.75.18.70' --add-host='store01:100.75.32.34' --add-host='store02:100.75.38.23' --add-host='store03:100.75.48.19' -p 127.0.0.1:8900:8000 -v /Users/thaing/Working/SourceCode/hipcloud_block_storage/machine-id:/home/block/machine-id -v /Users/thaing/Working/SourceCode/hipcloud_block_storage/media:/home/block/media -v /Users/thaing/Working/SourceCode/hipcloud_block_storage/block/migrations:/home/block/block/migrations -m 4096M --link redis:redis --link elasticsearch:elasticsearch --link mysql:mysql --memory-swap -1 -d block_storage:latest
`