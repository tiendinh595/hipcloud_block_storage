# Config Thêm Field Trong Elasticsearch

Elasticsearch các keyword cần tìm hiểu:

Index (1): giống như database thôi

Document : là kiểu dữ liệu , hiểu nó như table trong db là được

Document Type : table phải có table tên gì , block này tạo ra document type là "filesystem"

Mapping : là định nghỉa ra cái Document nó có field gì, kiểu dử liệu từng field, ngoài ra còn một số các khái niệm advance ( coi thêm )

Index data : khi nói câu "index data cho elasticsearch đi", nghỉa là đổ data vào Index (1) của elasticsearch.

Giao thức giao típ với elasticsearch : elasticsearch phơi ra các API để tạo index, tạo document, index data vào elasticsearch, mapping document, update mapping ....

## Mapping trong block
Hiện tại trên block , lúc chạy lên , nó sẻ check elasticsearch đã có index chưa, nếu chưa có nó sẻ set cái mapping kiểu dử liệu để lưu trử vào ( xem chi tiết trong settings.py của từng môi trường).
Trong project block hiện tại mapping đang tạo là cho document type "filesystem".

## Update Mapping

Một khi mapping được tạo muốn thêm field phải gọi update mapping để thêm field mới vào mapping đã tạo

Coi trong :

https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-put-mapping.html

## Nếu muốn thêm field trong elasticsearch phải làm gì :

Gọi Update Mapping cho từng elasticsearch từng môi trường, để thêm field vào mapping

Sửa hàm index trong models nào có hàm index() ( chỉ có mổi model Filesystem ) để thêm vào field đã update lúc mapping để lúc gọi lại hàm index là nó sẻ thêm data mới

Hàm này nè:
Vi du thêm field taolaomialao , update cái json body
```python
def index(self):
    if self.status == self.FILE_CLEARED:
        return settings.ES.delete(index=settings.ELASTICSEARCH_INDEX, doc_type=settings.ELASTICSEARCH_DOCTYPE,id=self.id)
    else:
        return settings.ES.index(index = settings.ELASTICSEARCH_INDEX, doc_type = settings.ELASTICSEARCH_DOCTYPE,
                                 id = self.id,
                                 body = {
                                             "fid": self.id,
                                             "type": self.type,
                                             "container": self.container.id,
                                             "kind": self.kind,
                                             "extension": self.extension,
                                             "icon": self.icon,
                                             "fq_path": self.fq_path,
                                             "file_name": self.file_name,
                                             "is_dir": self.is_dir,
                                             "status": self.status,
                                             "taolaomialao":self.taolaomialao
                                        }
                                )
```

## Thêm bước xử lý kết quả search

Tình hình hiện tại , search sẻ do elastic lo hoàng toàn, phía API chỉ là replay lại trả về cho user thôi.

Có feature muốn là thêm các field thumbnail hình trả về, nó đòi phải có url hình trong kết quả search, mà trong elasticsearch trả về chỉ có mổi ID của tấm hình search thì sẻ phải làm sao.

Step 1 : ở API trả kết quả search về, thêm bước đọc data search data, thêm code để lọc ra từng document mà có self.kind là image
 
Step 2 : gọi hàm generate thumbnail , pass vào cái ID của thumbnail

Done
